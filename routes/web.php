<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home_page');

Route::get('/home2', function () {
    return view('home2');
});

Route::get('/enquire', function () {
    return "";
})->name('enquire');

Route::get('/coin/success', function () {
    return "";
})->name('coin.success');

Route::post('/get/inquiry', 'EmailController@Inquiry')->name('contact.inquiry');

Route::post('/verifyrecpatcha', 'RecaptchaController@verify')->name('verifyrecpatcha');

