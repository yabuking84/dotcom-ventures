<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

// use GuzzleHttp\Exception\GuzzleException;
// use GuzzleHttp\Client;

use ReCaptcha\ReCaptcha;

class RecaptchaController extends Controller
{	

	public function verify (Request $rqst) {
		

		$recaptcha_token = $rqst->recaptcha_token;

		// $client = new Client();

		// $response  = $client->post('https://www.google.com/recaptcha/api/siteverify', [
		// 	'form_params' => [
		// 		'secret'=>env('RECAPTCHA_SECRET'),
		// 		'response'=>$recaptcha_token,
		// 	]
		// ]);
		
		// // dd($response);
		// dd(response()->json($response));
		
		// // return response()->json($response);

		$retVal = "";

		$response = (new ReCaptcha(env('RECAPTCHA_SECRET')))
        ->setExpectedAction('homepage')
        ->verify($recaptcha_token, $rqst->ip());

		if (!$response->isSuccess()) {
			$retVal = 'fail';
		}
		if ($response->getScore() < 0.6) {
			$retVal = 'challenge';
		}
		if ($response->isSuccess()) {
			$retVal = 'success';
		}

		return response()->json($retVal);

		
	}
}
