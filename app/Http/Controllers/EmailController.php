<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use Mail;
use DateTime;
use Auth;

class EmailController extends Controller
{	

	public function Inquiry(Request $request) {

		$details = array();
		$details = [
					'name_to' => $request->name,
					'email_to' => $request->email,
					'content' => $request->message,
					// 'subject' => $request->subject,
					'subject' => 'Enquiry',
					// 'company' => $request->company
					'company' => '',
				   ];

		// dd($details);

		$arr = self::emailInquiry($details);

		$message = $arr['message'];
		$error = $arr['error'];

		return response()->json($arr);
		// return back()->with('error',$error)->with('message',$message)->with('success','true');
		// return redirect()
		// 		->route('home_page')
		// 			->with('error',$error)
		// 				->with('message',$message)
		// 					->with('action','enquire');
	
	}


    //
    private function emailInquiry($details) {

		$retVal = "";
		$error = false;		

		$message = 'Enquiry sent!';

		$name_from = 'Dotcom Ventures FZE';
		$email_from = 'info@dotcomv.com';
		

		// customer details
		$name_to = $details['name_to'];
		$email_to = $details['email_to'];
		$content = $details['content'];
		$subject_inquiry = $details['subject'];
		$company = $details['company'];

		// $bcc_email = 'r.delarama@almani.ae';

		// $name_to = $name_from;
		// $email_to = $email_from;
		
    	$data = array(
			'name_from' => $name_from,
			'email_from' => $email_from, 
			'name_to' => $name_to, 
			'email_to' => $email_to, 
			'subject' => 'Dotcom Ventures FZE Enquiry', 
			'content' => $content, 
			'subject_inquiry' => $subject_inquiry, 
			'company' => $company, 
    	);

		try {

			$mail_status = Mail::send( 'emails.enquire_email', $data, function( $mail ) use ($data) {
				
			    $mail->to( $data['email_from'], $data['name_from'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->bcc([ $data['email_to'] ])
			    		->subject(  $data['subject'] );
			});

		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;	
			$error = true;
			$message = 'Please try again later. There was an error when sending the enquiry.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending the enquiry.';
	    }

	    return [
	    	'message'=>$message,
	    	'error'=>$error,
	    ];

	    dd($message,$error);
    }
    
}
