<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Insert Favicon Here -->
    <link href="{{ asset('images/favicon/icon.png') }}" rel="icon">

    <!-- Page Title(Name)-->
    <title>Lose Weight or Get Paid</title>

    <!-- Bootstrap CSS File -->
    
    {{-- <link rel="stylesheet" href="{{asset('vendors/bootstrap/css/bootstrap.min.css')}}">  --}}
    {{-- <link rel="stylesheet" href="{{asset('vendors/bootstrap/css/bootstrap-grid.min.css')}}">  --}}
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">

    <!-- Font-Awesome CSS File -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    {{--
    <link rel="stylesheet" href="{{asset('css/font-awesome-5.3.1.css')}}"> --}}
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">

    <!-- Slider Revolution CSS File -->
    <link rel="stylesheet" href="{{asset('css/settings.css')}}">



    <!-- Animate CSS File -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">


    <!-- Owl Carousel CSS File -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">

    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">

    <!-- Style CSS File -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Font Style CSS File -->
    <link rel="stylesheet" href="{{asset('css/font-six.css')}}">

    <!-- Plyr -->
    <link rel="stylesheet" href="{{asset('vendors/plyr/css/plyr-3.4.4.min.css')}}">

    <!-- Theme CSS File -->
    {{--
    <link rel="stylesheet" href="{{asset('css/style-theme.css')}}"> --}}
    <link rel="stylesheet" href="{{asset('css/style-theme.css')}}">

    <!-- Color StyleSheet CSS File -->
    {{--
    <link href="{{asset('css/orange.css')}}" rel="stylesheet" type="text/css"> --}}
    {{--
    <link href="{{asset('css/white.css')}}" rel="stylesheet" type="text/css"> --}}

    <link href="{{asset('css/font-six.css')}}" rel="stylesheet" type="text/css">


    <?php $version = env('ASSET_VERSION','1.0'); ?>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/master-app.css?v='.$version)}}">


    <meta property="og:type" content="article">
    <meta property="og:url" content="https://loseweightorgetpaid.com">
    <meta property="og:image" content="{{ asset('images/logo-512x512.jpg') }}">
    <meta property="og:title" content="Lose Weight or Get Paid">
    <meta property="og:description" content="Commit to be fit! The 'NO SECRET DIET'">

    <meta name="description" content="Commit to be fit! The 'NO SECRET DIET'">    
    <meta name="author" content="Lose Weight or Get Paid">

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '759839427701603');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=759839427701603&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->




    <script>
        @if (isset($success))
          var success = 'true';
        @else
          var success = 'false';
        @endif
    </script>

</head>
        


{{--

<body data-spy="scroll" data-target=".navbar" data-offset="50" id="body" class="index-only-side-nav"> --}}

    <body data-spy="scroll" data-target=".navbar" data-offset="70" id="body" class="">

        {{-- fb pixel lead tracker --}}
        {{-- ///////////////////////////////////////////////////// --}}
        <script>
        fbq('track', 'Lead');
        </script>
        {{-- ///////////////////////////////////////////////////// --}}
        {{-- fb pixel lead tracker --}}

        {{-- fb button app --}}
        {{-- ////////////////////////////////////////////////////////////////////////////////////// --}}
        {{-- <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/ar_AR/sdk.js#xfbml=1&version=v3.2';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script> --}}
        {{-- ////////////////////////////////////////////////////////////////////////////////////// --}}
        {{-- fb button app --}}




        <!-- Loader -->
        <div class="loader">
            <div class="loader-inner">
                <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->



        <!-- Parent Section -->

        <section class="page_content_parent_section">



            <!-- Header Section -->
            <header>
                <!-- Navbar Section -->
                <nav class="navbar navbar-fixed-top green">


                    {{-- cookie warning --}}
                    {{-- //////////////////////////////////////////////////////////////////// --}}
                    <div class="cookie_warning">
                        <p>
                            To continue, you must agree to the <b>www.loseweightorgetpaid.com<span class="symbol">&copy;</span></b>
                            <a data-target="#tc_modal" data-toggle="modal" class="pp_tc_link">Terms and Conditions</a>
                            and <a data-target="#pp_modal" data-toggle="modal" class="pp_tc_link">Privacy Policy</a>.
                            This Website uses cookies in order to offer you the most relevant information.
                            Please read our Cookies Policy for more information in the Privacy Policy <a data-target="#pp_modal"
                                data-toggle="modal" data-anchor="#cookies" class="pp_tc_link scroll_toanchor">Here</a>.
                            If you want to proceed to the Website, please accept the Website policies by clicking on
                            "Yes, I accept" button below.
                            If you do not agree, then you may leave by closing this webpage.
                        </p>
                        <button class="btn btn-default i_accept">Yes, I accept</button>
                    </div>
                    {{-- //////////////////////////////////////////////////////////////////// --}}
                    {{-- cookie warning --}}



                    <div class="container-fluid">
                        <!--second nav button -->
                        <div id="menu_bars" class="right menu_bars">
                            <span class="t1 bg_theme"></span>
                            <span class="t2 bg_theme"></span>
                            <span class="t3 bg_theme"></span>
                        </div>
                        {{-- <div class="navbar-header clearfix">
                            <a class="navbar-brand lwgp_logo" href="#"><img src="images/logo.png" alt="logo"></a>
                        </div> --}}
                        <div class="container">
                            <div class="navbar-header">
                                {{-- <a class="navbar-brand lwgp_logo" href="{{ asset('') }}"><img src="images/logo.png"
                                        alt="logo"></a> --}}
                                <a class="navbar-brand lwgp_logo" href="{{ asset('') }}"><img src="{{asset('images/logo-65.png')}}"
                                        alt="logo"></a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-ex1-collapse  ">
                                <div class="navbar-right">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="#home" class="scroll">Home</a></li>
                                        <li><a href="#why_purchase" class="scroll">About the Platform</a></li>
                                        <li><a href="#guarantee" class="scroll">Get Paid Initiative</a></li>
                                        <li><a href="#pricing_section" class="scroll">Pricing</a></li>
                                        <li><a href="#testimonial_videos" class="scroll">Testimonials</a></li>
                                        <li><a href="#contact-form" class="scroll">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="sidebar_menu">
                            <nav class="pushmenu pushmenu-right">
                                {{-- <a class="push-logo" href="#"><img src="images/logo.png" alt="logo"></a> --}}
                                <ul class="push_nav centered">
                                    {{-- <li class="clearfix">
                                        <a href="#home" class="scroll"><span>01.</span> Home</a>
                                    </li> --}}

                                    <li class="clearfix"><a href="#home" class="scroll"><span>Home</span></a></li>
                                    <li class="clearfix"><a href="#why_purchase" class="scroll"><span>About the
                                                Platform</span></a></li>
                                    <li class="clearfix"><a href="#guarantee" class="scroll"><span>Get Paid Initiative</span></a></li>
                                    <li class="clearfix"><a href="#pricing_section" class="scroll"><span>Pricing</span></a></li>
                                    <li class="clearfix"><a href="#testimonial_videos" class="scroll"><span>Testimonials</span></a></li>
                                    <li class="clearfix"><a href="#contact-form" class="scroll"><span>Contact Us</span></a></li>
                                </ul>
                                <div class="clearfix"></div>
                                <ul class="social_icon black top25 bottom20 list-inline">
                                    <li><a target="_blank" href="http://facebook.com/nosecretdiet" class="navy_blue facebook"><i class="fa fa-fw fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://m.me/nosecretdiet" class="navy_blue facebook"><i class="fab fa-facebook-messenger"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/loseweightorgetpaid/" class="navy_blue instagram"><i class="fab fa-instagram"></i></a></li>
                                    <li><a target="_blank" href="https://www.pinterest.com/loseweightorgetpaid/" class="navy_blue pinterest"><i class="fa fa-fw fa fa-pinterest"></i></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </nav>
                <!-- /Navbar Section -->
                <!-- Main Slider Section -->
                <div class="page_content_main_slider" id="home">
                    {{-- <div class="swiper-container main_slider parallax-window" data-android-Fix="true" data-speed="-0.1"
                        data-parallax="scroll" data-image-src="images/main/slider_slide_3.jpg"> --}}

                        <?php
                $main_images = [
                    //'bg05.jpg',
                    //'bg30.jpg',

                    ['image'=>'bg29.jpg','css'=>'background-position-x: 80%;'],
                    ['image'=>'bg28.jpg','css'=>''],
                    ['image'=>'bg04.jpg','css'=>''],
                    ['image'=>'bg32.jpg','css'=>''],
                ];
                shuffle($main_images);
                $main_text = [
                    ['title'=>'Tired of the Cycle','text'=>'Are you tired of going through the cycle of losing-gaining-losing-gaining weight?'],
                    ['title'=>'No Results','text'=>'Have you tried all formsof dieting but experienced no result?'],
                    ['title'=>'Simply Lose Weight','text'=>'Do you know you can get a simple list of things to do to lose weight in no time and get your summer body in full effect?'],
                ];

            ?>
                        <div class="swiper-container main_slider">
                            <div class="swiper-wrapper">


                                @for($x=0; $x<3; $x++)
                                    {{-- <div class="swiper-slide swiper-slide_bg_bottom" style="background-image: url({{ asset('images/carousel/'.$main_images[0]) }});"> --}}
                                    <div class="swiper-slide " style="background-image: url({{ asset('images/carousel/'.$main_images[$x]['image']) }}); {{ $main_images[$x]['css'] }}">
                                        <div class="container">
                                            {{-- <h3 class="raleway text-right white_color swiper-slider-small-heading margin_default_bottom"> We Build Your</h3> --}}
                                            <h1 class="raleway white_color text-left swiper-slider-heading">
                                                <span class="font_400 pink_color">
                                                    {{ $main_text[$x]['title'] }}
                                                </span> 
                                            </h1>
                                            <div class="row">
                                                <div class="">
                                                    <p class="open_sans default_text white_color ma">
                                                        {{ $main_text[$x]['text'] }}
                                                    </p>
                                                </div>
                                            </div>
                                            {{-- <div class=" text-right">
                                                <a href="#." class="button button_default_style open_sans bg_pink bg_before_pink bg_before_white btn1">
                                                    Get A Quote <i class="fa fa-long-arrow-right "></i>
                                                </a>
                                            </div> --}}
                                        </div>
                                    </div>
                                @endfor


                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next swiper-arrows fa fa-angle-right hidden-sm hidden-xs"></div>
                                <div class="swiper-button-prev swiper-arrows fa fa-angle-left hidden-sm hidden-xs"></div>
                            </div>
                        </div>

                        <!-- /Main Slider Section -->
            </header>
            <!-- /Header Section -->

            <br>

            <!-- Half Section -->
            <section class="half_section_main half_section_right " id="where_in">
                <div class="container container_big">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="side_section_text big_padding">
                                <p class="default_small_heading raleway blue_color font_200">Where we come in?</p>
                                <h3 class="default_section_heading raleway navy_blue">
                                    LoseWeightOrGetPaid.com
                                    {{-- <span class="font_200"></span> --}}
                                </h3>
                                <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                                <p class="default_text_light default_text open_sans">
                                    LoseWeightOrGetPaid.com is a health and fitness platform dedicated to helping
                                    individuals achieve their weight loss goals. We are health and nutrition experts.
                                    We have studied a lot of popular and non-popular diets, have done a lot of research
                                    also studied official research papers. We concluded that most of the information
                                    available to the public and by most consumers accepted as "the truth" is just a
                                    myth and simply false!
                                    <br><br>
                                    An example is "Carbs are bad for you" or "fruits have too much sugar".
                                    <br><br>
                                    All these myths are false and following them does not guarantee you will lose
                                    weight.
                                </p>
                                <ul class="soc_media_icons">
                                    <li>
                                        <a class="anchor_style_default facebook wow zoomInDown" 
                                            href="http://facebook.com/nosecretdiet"
                                            target="_blank">
                                            <i class="text-center fa fa-facebook "></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="anchor_style_default messenger wow zoomInUp" 
                                            href="https://m.me/nosecretdiet"
                                            target="_blank">
                                            <i class="text-center fab fa-facebook-messenger"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="anchor_style_default instagram wow zoomInDown" 
                                            href="https://www.instagram.com/loseweightorgetpaid/"
                                            target="_blank">
                                            <i class="text-center fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="anchor_style_default pinterest wow zoomInDown" 
                                            href="https://www.pinterest.com/loseweightorgetpaid/" 
                                            target="_blank">
                                            <i class="text-center fab fa-pinterest"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 text-align-center">
                            {{-- hidden-sm hidden-xs --}}
                            <div class="half_section_picture wow slideInRight" style="background-image: url({{ asset('images/sections/lwgp-package.png') }});"></div>
                            <a href="#pricing_section" class="bg_pink button button_default_style open_sans bg_before_navy scroll slwn">
                                START LOSING WEIGHT NOW!!!
                            </a>
                        </div>

                    </div>
                </div>

            </section>
            <!-- half Section Ended -->



            <!-- Half Section -->
            <section class="half_section_main bg_grey" id="skill_section">
                <div class="half_section_picture hidden-sm hidden-xs" style="background-image: url({{ asset('images/sections/sc08.jpg') }});"></div>
                <div class="container container_big">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6">
                            <div class="side_section_text big_padding">
                                <p class="default_small_heading raleway blue_color font_200">Try it now</p>
                                <h3 class="default_section_heading raleway navy_blue text_left">You are exactly <span
                                        class="font_200">where you need to be </span></h3>
                                <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                                <p class="default_text_light default_text open_sans">
                                    There are about 108 million people in the United States that are either overweight,
                                    obese or suffering from morbid obesity.
                                    <br><br>
                                    These are the questions you must ask yourself when starting a diet plan. Let's face
                                    it, there are many ways to lose weight, but if you're looking for long-term
                                    success, then the fast, easy fad diets of pills, shakes, supplements, grapefruit,
                                    and pineapple are not what you're looking for.
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="icons_div_small">
                                            <p class="small_heading_navy raleway font_400">
                                                {{-- <i class="fa fa-comment-o pink_color" aria-hidden="true"></i> --}}
                                                <i class="fas fa-clock pink_color" aria-hidden="true"></i>
                                                Long Term
                                            </p>
                                            <p class="default_text default_text_light open_sans">
                                                We know you want to lose weight fast, but would you rather have
                                                long-term success or short-term success?
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="icons_div_small">
                                            <p class="small_heading_navy raleway font_400">
                                                {{-- <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i>
                                                --}}
                                                <i class="fas fa-thumbs-up pink_color" aria-hidden="true"></i>
                                                Reliable
                                            </p>
                                            <p class="default_text default_text_light open_sans">
                                                You need a reliable program that will provide results for life.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- half Section Ended -->



            <!-- Half Section -->

            <section class="food-company-section big_padding" id="why_purchase">
                <div class="container container_big">
                    <p class="default_small_heading raleway text-center blue_color font_200">
                        Why you should purchase this package?
                    </p>
                    <h2 class="default_section_heading text-center">
                        <span class="font_200">The</span> 'NO SECRET DIET' <span class="font_200">PACKAGE</span>
                        {{-- <span class="font_200">Why you should you</span> purchase <span class="font_200">this
                            package?</span> --}}
                    </h2>
                    <hr class="default_divider default_divider_blue default_divider_big">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="expertise_block text-center margin_default_bottom_big wow slideInLeft"
                                    style="visibility: visible; animation-name: slideInLeft;">
                                    <i class="fa fa-send pink_color"></i>
                                    <h3 class="small_heading_navy text-center raleway h3_pink font_600">Holistic
                                        Approach</h3>
                                    <p class="default_text text-center open_sans default_text_light">
                                        The "No Secret Diet" Package is a holistic approach to proper dieting and
                                        exercising.
                                        You should not forgo one for the other, we won't tell you otherwise.
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="expertise_block text-center margin_after_tab wow slideInLeft" style="visibility: visible; animation-name: slideInLeft;">
                                    <i class="fa fa-calendar pink_color"></i>
                                    <h3 class="small_heading_navy text-center raleway h3_pink font_600">Time Well Spent</h3>
                                    <p class="default_text text-center open_sans default_text_light">
                                        You do not need to invest hours and hours of valuable time before you can get
                                        started.
                                        It is not a lengthy drivel or ebook on what you have heard before.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 margin_after_tab">
                        <div class="food_about_bg" style="background-image: url({{ asset('images/sections/sc02.jpg') }});"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="expertise_block text-center margin_default_bottom_big wow slideInRight"
                                    style="visibility: visible; animation-name: slideInLeft;">
                                    <i class="fa fa-archive pink_color"></i>
                                    <h3 class="small_heading_navy text-center raleway h3_pink font_600">Simple</h3>
                                    <p class="default_text text-center open_sans default_text_light">
                                        You do not need to study long explanations or cooking recipes to make tasteless
                                        dishes and smoothies.
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="expertise_block text-center margin_after_tab wow slideInRight" style="visibility: visible; animation-name: slideInLeft;">
                                    <i class="fas fa-hand-holding pink_color"></i>
                                    <h3 class="small_heading_navy text-center raleway h3_pink font_600">Concise</h3>
                                    <p class="default_text text-center open_sans default_text_light">
                                        The 'No Secret Diet' package is a concise and simple guide to getting your
                                        health back.
                                        We have articulated this package in the best way possible.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- half Section Ended -->
































            <!-- Circle Js Skill Section -->
            <!-- Skill Heading Section -->
            <section class="combined_skills_section big_padding" id="what_waiting_for">

                <section class="skill_section">
                    <div class="container">
                        <div class="skill_section_inner">
                            <p class="default_text raleway text-center default_small_heading font_200">The 'NO SECRET
                                DIET' Package</p>
                            <h2 class="default_section_heading text-center">

                                What are you waiting for?
                                <span class="font_200">
                                </span>
                            </h2>
                            <hr class="default_divider default_divider_big">
                        </div>
                    </div>
                </section>
                <!-- /Skill Section -->
                <section class="circle_pie_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="expertise_section_inner">
                                    <div class="row">

                                        {{-- <div class="col-md-4"></div> --}}

                                        <div class="col-md-12">
                                            <div class="expertise_block text-center margin_after_tab wow fadeInUp">
                                                <p class="default_text text-center open_sans default_text_light">
                                                    <a href="#pricing_section" class="bg_pink button button_default_style open_sans bg_before_navy scroll slwn">
                                                        START LOSING WEIGHT NOW!!!
                                                    </a>
                                                </p>
                                            </div>
                                        </div>

                                        {{-- <div class="col-md-4"></div> --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
            <!-- /Circel Js Skill Section -->








































            <!-- Circle Js Skill Section -->
            <!-- Skill Heading Section -->
            <section class="combined_skills_section big_padding bg_grey" id="why_platform">

                <section class="skill_section">
                    <div class="container">
                        <div class="skill_section_inner">
                            <p class="default_text raleway text-center default_small_heading blue_color font_200">Why
                                this platform is for you?</p>
                            <h2 class="default_section_heading text-center">
                                LoseWeightOrGetPaid.com
                                <span class="font_200">

                                </span>
                            </h2>
                            <hr class="default_divider default_divider_blue default_divider_big">
                        </div>
                    </div>
                </section>

                <!-- /Skill Section -->
                <section class="circle_pie_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="expertise_section_inner">
                                    <div class="row">


                                        <div class="block_text_right">
                                            <div class="image_slashed" style="background-image: url({{ asset('images/sections/sc_why01.jpg') }});">
                                            </div>
                                            <div class="expertise_block text-center margin_after_tab wow slideInRight">
                                                <h3 class="small_heading_navy text-center raleway h3_pink font_400">
                                                    <span class="font_600 pink_color">Commit</span> to be fit
                                                </h3>
                                                <p class="default_text text-center open_sans default_text_light">
                                                    The ‘No Secret Diet’ will help you achieve your weight loss goals
                                                    in no time. To get to your desired body size, you need to follow a
                                                    guide that takes a holistic outlook on weight loss.
                                                </p>
                                            </div>
                                        </div>



                                        <div class="block_text_left">
                                            <div class="expertise_block text-center margin_after_tab wow slideInLeft">
                                                <h3 class="small_heading_navy text-center raleway h3_pink font_400">
                                                    <span class="font_600 pink_color">Beyond</span> losing weight
                                                </h3>
                                                <p class="default_text text-center open_sans default_text_light">
                                                    This program should go beyond just losing weight by incorporating a
                                                    natural and healthy eating lifestyle.
                                                </p>
                                            </div>
                                            <div class="image_slashed" style="background-image: url({{ asset('images/sections/sc_why02.jpg') }});"></div>
                                        </div>

                                        <div class="block_text_right">
                                            <div class="image_slashed" style="background-image: url({{ asset('images/sections/sc_why03.jpg') }});"></div>
                                            <div class="expertise_block text-center margin_after_tab wow slideInRight">
                                                <h3 class="small_heading_navy text-center raleway h3_pink font_400">
                                                    <span class="font_600 pink_color">Be</span> informed
                                                </h3>
                                                <p class="default_text text-center open_sans default_text_light">
                                                    The ‘No Secret Diet’ is not a radical piece of information or some
                                                    alternative form of medicine that tries to sell you hard on
                                                    something you don’t already know.
                                                </p>
                                            </div>
                                        </div>

                                        <div class="block_text_left">
                                            <div class="expertise_block text-center margin_after_tab wow slideInLeft">
                                                <h3 class="small_heading_navy text-center raleway h3_pink font_400">
                                                    <span class="font_600 pink_color">Old</span> but gold rules
                                                </h3>
                                                <p class="default_text text-center open_sans default_text_light">
                                                    The ‘No Secret Diet’ is a package that reaffirms and compliments
                                                    the old but gold rules of weight loss.
                                                </p>
                                            </div>
                                            <div class="image_slashed" style="background-image: url({{ asset('images/sections/sc_why04.jpg') }});"></div>
                                        </div>


                                        <div class="block_text_right">
                                            <div class="image_slashed" style="background-image: url({{ asset('images/sections/sc_why05.jpg') }});"></div>
                                            <div class="expertise_block text-center margin_after_tab wow slideInRight">
                                                <h3 class="small_heading_navy text-center raleway h3_pink font_400">
                                                    <span class="font_600 pink_color">Worked before</span>, will work
                                                    now
                                                </h3>
                                                <p class="default_text text-center open_sans default_text_light">
                                                    No fluffs, no cookie cutters, you get what has worked for some of
                                                    our team members and still works for thousands of our clients.
                                                </p>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
            <!-- /Circel Js Skill Section -->








            <!-- Circle Js Skill Section -->
            <!-- Skill Heading Section -->
            <section class="combined_skills_section big_padding" id="guarantee">

                <section class="skill_section">
                    <div class="container">
                        <div class="skill_section_inner">
                            <p class="default_text raleway text-center default_small_heading blue_color font_200">Guaranteed
                                by our</p>
                            <h2 class="default_section_heading text-center">
                                'Get Paid'
                                <span class="font_200">
                                    initiative
                                </span>
                            </h2>
                            <hr class="default_divider default_divider_blue default_divider_big">
                        </div>
                    </div>
                </section>
                <!-- /Skill Section -->
                <section class="circle_pie_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="expertise_section_inner">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="expertise_block text-center margin_after_tab wow slideInLeft">
                                                {{-- <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span
                                                        class="font_600 pink_color">01.</span> Web Design</h3> --}}
                                                <p class="default_text text-center open_sans default_text_light">
                                                    <span class="font_600 pink_color">We</span> believe in what we have
                                                    put together. This package is a product of years of research and
                                                    experimentation.
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="expertise_block text-center margin_after_tab wow fadeInUp">
                                                {{-- <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span
                                                        class="font_600 pink_color">01.</span> Web Design</h3> --}}
                                                <p class="default_text text-center open_sans default_text_light">
                                                    <span class="font_600 pink_color">In</span> view of this, we will
                                                    refund any customer that applies the tips in the ‘No Secret Diet’
                                                    with a good dose of faith and discipline and did not get the
                                                    desired results.
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="expertise_block text-center margin_after_tab wow slideInRight">
                                                {{-- <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span
                                                        class="font_600 pink_color">01.</span> Web Design</h3> --}}
                                                <p class="default_text text-center open_sans default_text_light">
                                                    <span class="font_600 pink_color">We</span> will refund you 10x
                                                    over if the "No secret diet" fails you after 8 weeks of applying
                                                    the methods in the package.
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="half_section_picture hidden-sm hidden-xs" style="background-image: url({{ asset('images/sections/bg10.jpg') }});"></div>
                            <div class="col-md-6 col-md-offset-6">
                                <div class="side_section_text small_padding">
                                    {{-- <p class="default_small_heading raleway blue_color font_200">Try it now</p>
                                    --}}
                                    <h3 class="default_section_heading raleway navy_blue text_left strongly_believe">
                                        <span class="font_200">We strongly believe you will find our </span>'No Secret
                                        Diet'<span class="font_200"> the best purchasing decision you have made in a
                                            long time.</span>
                                    </h3>
                                    <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                                    <p class="default_text_light default_text open_sans">
                                        How to prove and claim your $100:
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="icons_div_small claim">
                                                <p class="small_heading_navy raleway font_400">
                                                    {{-- <i class="fa fa-comment-o pink_color" aria-hidden="true"></i>
                                                    --}}
                                                    {{-- <i class="fas fa-step-backward pink_color"></i><i class="fas fa-step-forward pink_color"></i>
                                                    --}}
                                                    {{-- <i class="fas fa-fast-backward pink_color"></i><i class="fas fa-fast-forward pink_color"></i>
                                                    --}}
                                                    <i class="fas fa-backward pink_color"></i>Before
                                                    &nbsp;&nbsp;&&nbsp;&nbsp;
                                                    <i class="fas fa-forward pink_color"></i>After
                                                </p>
                                                <p class="default_text default_text_light open_sans">
                                                    If you can prove using ‘before and after’ pics.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="icons_div_small claim">
                                                <p class="small_heading_navy raleway font_400">
                                                    {{-- <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i>
                                                    --}}
                                                    {{-- <i class="fas fa-thumbs-up pink_color" aria-hidden="true"></i>
                                                    --}}
                                                    <i class="fas fa-calendar-alt pink_color"></i>Date
                                                    &nbsp;&nbsp;&&nbsp;&nbsp;
                                                    <i class="fas fa-clock pink_color"></i>Time
                                                </p>
                                                <p class="default_text default_text_light open_sans">
                                                    By using a machine with a date and time feature.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>
            </section>
            <!-- /Circel Js Skill Section -->










            <!-- Pricing Table -->
            <section class="pricing_section big_padding" id="pricing_section">
                <div class="container">

                    <div class="pricing_table_section">
                        <p class="default_text raleway text-center default_small_heading blue_color font_200">Start
                            Losing Weight Now!</p>
                        <h2 class="default_section_heading text-center">
                            <span class="font_200">Our</span>
                            Pricing
                            <span class="font_200"></span>
                        </h2>
                        <hr class="default_divider default_divider_blue default_divider_big">
                        <div class="row">
                            <div class="col-sm-offset-4 col-sm-4">
                                <div class="pricing_table_column wow fadeInUp">
                                    <h3 class="small_heading_navy raleway">The 'No Secret Diet' Package</h3>
                                    <div class="price">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <h2 class="open_sans font_400 pink_color">
                                                    <span class="dollar raleway">$</span>9.99
                                                    {{-- <br><span class="month raleway"></span> --}}
                                                </h2>
                                            </div>
                                            <div class="col-xs-8">
                                                <p>A thorough guide structured specifically to help you achieve your
                                                    weight loss goals.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="packages">
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Holistic Approach</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Verified</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Effective</li>
                                        <li><i class="fa fa-check" aria-hidden="true"></i>Concise</li>
                                    </ul>
                                    <div class="pricing_button">
                                        <a href="#." class="bg_pink button button_default_style open_sans bg_before_navy"
                                            data-toggle="modal" data-target="#lwgp_modal">Buy Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="payment_methods">
                            <div>                            
                                <i title="Visa" class="fab fa-cc-visa"></i>
                                <i title="Mastercard" class="fab fa-cc-mastercard"></i>
                                <i title="Cryptocurrency" class="fab fa-bitcoin"></i>
                            </div>
                            <div>                            
                                <i title="Paypal" class="fab fa-paypal"></i>
                                <i title="Google Pay" class="fab fa-google"></i>
                                <i title="Apple Pay" class="fab fa-apple"></i>
                                <img title="Samsung Pay" class="samsung-pay" src="{{ asset('images/payment-method-logos/samsung-pay.png') }}"/>              
                                {{-- <i title="Amazon Pay" class="fab fa-amazon-pay"></i> --}}
                            </div>                        
                        </div>                      
                    </div>

                </div>
            </section>




            <!-- Blog Section -->
            <section class="blog_section big_padding bg_grey" id="testimonial_videos">
                <div class="container">
                    <p class="default_text raleway text-center default_small_heading blue_color font_200">watch
                        testimonials</p>
                    <h2 class="default_section_heading text-center">
                        <span class="font_200"> Our </span>
                        Awesome
                        <span class="font_200"> Customers</span>
                    </h2>
                    <hr class="default_divider default_divider_blue default_divider_big">
                    <div class="testi-videos row">
                        <div class="col-md-4 testi-video">
                            <video poster="{{ asset('images/testimonials/posters/lwgp-testimonial-v1.jpg') }}" id="plyr_video1"
                                class="plyr_videos">
                                <source src="https://loseweightorgetpaid.com/videos/lwgp-testimonial-v1.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-md-4 testi-video">
                            <video poster="{{ asset('images/testimonials/posters/lwgp-testimonial-v2.jpg') }}" id="plyr_video2"
                                class="plyr_videos">
                                <source src="https://loseweightorgetpaid.com/videos/lwgp-testimonial-v2.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-md-4 testi-video">
                            <video poster="{{ asset('images/testimonials/posters/lwgp-testimonial-v3.jpg') }}" id="plyr_video3"
                                class="plyr_videos">
                                <source src="https://loseweightorgetpaid.com/videos/lwgp-testimonial-v3.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-md-4 testi-video">
                            <video poster="{{ asset('images/testimonials/posters/lwgp-testimonial-v4.jpg') }}" id="plyr_video4"
                                class="plyr_videos">
                                <source src="https://loseweightorgetpaid.com/videos/lwgp-testimonial-v4.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-md-4 testi-video">
                            <video poster="{{ asset('images/testimonials/posters/lwgp-testimonial-v5.jpg') }}" id="plyr_video5"
                                class="plyr_videos">
                                <source src="https://loseweightorgetpaid.com/videos/lwgp-testimonial-v5.mp4" type="video/mp4">
                            </video>
                        </div>
                        <div class="col-md-4 testi-video">
                            <video poster="{{ asset('images/testimonials/posters/lwgp-testimonial-v6.jpg') }}" id="plyr_video6"
                                class="plyr_videos">
                                <source src="https://loseweightorgetpaid.com/videos/lwgp-testimonial-v6.mp4" type="video/mp4">
                            </video>
                        </div>

                        <?php /*
                <?php $yt_settings = "?enablejsapi=1autoplay=0&fs=1&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&modestbranding=1"; ?>
                        <?php //$yt_settings = "?showinfo=0&controls=0"; ?>
                        <div class="col-md-4 testi-video">
                            <iframe src="https://www.youtube.com/embed/n3hh9H7Fdm0{{ $yt_settings }}" allowfullscreen="0"></iframe>
                        </div>
                        <div class="col-md-4 testi-video">
                            <iframe src="https://www.youtube.com/embed/I-vvuz2nQAg{{ $yt_settings }}" allowfullscreen="0"></iframe>
                        </div>
                        <div class="col-md-4 testi-video">
                            <iframe src="https://www.youtube.com/embed/9aZa6hH_M78{{ $yt_settings }}" allowfullscreen="0"></iframe>
                        </div>
                        <div class="col-md-4 testi-video">
                            <iframe src="https://www.youtube.com/embed/9QexJAwhHW0{{ $yt_settings }}" allowfullscreen="0"></iframe>
                        </div>
                        <div class="col-md-4 testi-video">
                            <iframe src="https://www.youtube.com/embed/YBgMAUZIFBM{{ $yt_settings }}" allowfullscreen="0"></iframe>
                        </div>
                        <div class="col-md-4 testi-video">
                            <iframe src="https://www.youtube.com/embed/MpCHbv9eVUE{{ $yt_settings }}" allowfullscreen="0"></iframe>
                        </div>
                        */ ?>
                    </div>
                </div>
            </section>
            <!-- /Blog Section -->






















            <!-- Stats Section -->
            @php

                    $date1 = new DateTime('2018-10-28');
                    $date2 = new DateTime(date('Y-m-d'));
                    $days_diff  = $date2->diff($date1)->format('%a');
                    
                    $hwl = number_format((int)5400*pow(1.03,$days_diff),0, '.', '');
                    $wl = number_format((int)27000*pow(1.03,$days_diff),0, '.', '');
                    $cb = number_format((int)207*pow(1.03,$days_diff),0, '.', '');
                    
            @endphp            
            <section class="stats_section big_padding" id="statistics">
                <div class="container">
                    <p class="default_text raleway text-center default_small_heading blue_color font_200">So far, so
                        good</p>
                    <h2 class="default_section_heading text-center">
                        <span class="font_200">Community </span>
                        Achievements
                    </h2>
                    <hr class="default_divider default_divider_blue default_divider_big">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 margin_after_tab text-center">
                            <div class="stats_section_inner">
                                <i class="far fa-smile-beam stats_section_icon text-center" aria-hidden="true"></i>
                                <h3 class="default_section_heading  raleway number-scroll"> {{ $hwl }} </h3>
                                <p class="small_heading_navy  open_sans font_600">Happy Weight Loser</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 margin_after_tab text-center">
                            <div class="stats_section_inner">
                                <i class="fas fa-balance-scale stats_section_icon text-center" aria-hidden="true"></i>
                                <h3 class="default_section_heading  raleway number-scroll"> {{ $wl }} </h3>
                                <p class="small_heading_navy  open_sans font_600">Weight Lost (kg)</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 margin_after_tab text-center">
                            <div class="stats_section_inner">
                                <i class="fas fa-dumbbell stats_section_icon text-center" aria-hidden="true"></i>
                                <h3 class="default_section_heading  raleway number-scroll"> {{ $cb }} </h3>
                                <p class="small_heading_navy  open_sans font_600">Calories Burned (millions)</p>
                            </div>
                        </div>
                        {{-- <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                            <div class="stats_section_inner">
                                <i class="fas fa-comment-alt  stats_section_icon text-center" aria-hidden="true"></i>
                                <h3 class="default_section_heading  raleway number-scroll"> 9 </h3>
                                <p class="small_heading_navy  open_sans font_200"></p>
                            </div>
                        </div> --}}

                        {{-- <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                            <div class="stats_section_inner">
                                <i class="fa fa-gift  stats_section_icon text-center" aria-hidden="true"></i>
                                <h3 class="default_section_heading  raleway number-scroll"> 9 </h3>
                                <p class="small_heading_navy  open_sans font_200"></p>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </section>
            <!-- /Stats Section -->






            <!-- Half Section -->
            <section class="half_section_main bg_grey" id="return_investment">
                <div class="half_section_picture hidden-sm hidden-xs" style="background-image: url({{ asset('images/sections/sc07.jpg') }});"></div>
                <div class="container container_big">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6">
                            <div class="side_section_text big_padding">
                                <p class="default_small_heading raleway blue_color font_200">and it gets better..</p>
                                <h3 class="default_section_heading raleway navy_blue text_left">
                                    <span class="font_200">Fancy a</span> return on your investment?
                                </h3>
                                <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                                <p class="default_text_light default_text open_sans">
                                    We will pay you $9.99 if you send a video testimonial telling us/the community
                                    about how the <span class="font_bold">'No Secret Diet'</span> has worked for you.
                                    <br><br>
                                    This is a no-brainer. You have absolutely nothing to lose.
                                </p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="icons_div_small">
                                            <p class="small_heading_navy raleway font_400">
                                                {{-- <i class="fa fa-comment-o pink_color" aria-hidden="true"></i> --}}
                                                {{-- <i class="fas fa-clock pink_color" aria-hidden="true"></i> --}}
                                                FOR JUST $9.99, YOU CAN GET THE <span class="font_600">'NO SECRET DIET'</span>
                                                PACKAGE
                                            </p>
                                            {{-- <p class="default_text default_text_light open_sans">
                                                We know you want to lose weight fast, but would you rather have
                                                long-term success or short-term success?
                                            </p> --}}
                                            <a href="#pricing_section" class="bg_pink button button_default_style open_sans bg_before_navy scroll slwn">
                                                START LOSING WEIGHT NOW!!!
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- half Section Ended -->



















            <!-- Customer Review Slider -->
            <section class="customer_feedback_section big_padding text-center bg_theme" id="customer_feedback">
                <div class="container">
                    <div class="feedaback_inner">
                        <img src="images/main/cutomer_quote.png" alt="cutomer_quote" class="quote_pic">
                        <h2 class="default_section_heading text-center white_color">
                            <span class="font_200">
                                What
                            </span>
                            People
                            <span class="font_200">
                                Say ?
                            </span>
                        </h2>
                        <hr class="default_divider default_divider_white default_divider_big">
                        <div class="swiper-container customer_feedback_slider white_pagination">
                            <div class="swiper-wrapper">


                                <?php 
                        $testimonials = [];
                        $delay = '5000';
                        
                        // $testimonials[] = ['name' => 'Justin Shanelle', 'text' => 'I knew I was missing out on life — I felt too scared to enter a gym and didn\'t want to risk ridicule. I even had nights that I awoke gasping for air due to my weight. I lost my mom when I was 14, and I always told myself I would give my kids better than I had. So, on New Year\'s Eve, I pulled the trigger and decided I wanted to change. I stumbled upon the \'No Secret Diet\' and got to work. I have seen tremendous changes in my fitness levels and lost over 140 pounds in 18 months.'];
                        // $testimonials[] = ['name' => 'Gabrielle Rowan', 'text' => 'When my husband and I decided to start bearing children after trying for seven years, I decided to see a fertility specialist. I was well over 450 pounds at that time and he told me there was nothing he could do for me until I lost weight. After hearing the doctor\'s news, I received an unexpected pregnancy announcement from a family member. That announcement broke me, but I also credit it with finally giving me the kick in the butt I needed. It made me realize if I ever wanted a family that I needed to do something a little more drastic. The \'No Secret Diet\' isn\'t just about eating right or exercising. It\'s about finding a balance between the two and setting attainable goals. It worked for me; I am pregnant and excited to the moon and back.'];
                        // $testimonials[] = ['name' => 'Jean Malone', 'text' => 'When I met my husband, I snapped back to reality. I found someone who loved me for who I was. I no longer felt I needed to prove anything to anyone regarding what I looked like, so I started eating and before I knew it, I looked in the mirror and didn\'t recognize myself. At 185 pounds, I got the \'No Secret Diet\' and decided to get my eating habits under control for good. 3 months later and I am 38 pounds lighter. I don\'t think I could be more fulfilled.'];
                        // $testimonials[] = ['name' => 'Demi Scott', 'text' => 'I\'ve always had a bad relationship with weight. I was a size 10 in the fifth grade. My friends growing up were really skinny. We ate the same things, but I was the biggest one. In college, I didn\'t cook for myself and landed at my highest weight of 215 pounds. Between working out, cooking for myself and not going to too many happy hours, I have been able to get to my desired weight level. I will forever be indebted to the tips in the \'No Secret Diet\' package.'];
                        $testimonials[] = ['name' => 'Joy Rudyard',          'delay'=>$delay, 'image'=>'jr.jpg',  'text' => 'Many things happened through those years and you\'d think would be a trigger to lose weight — from embarrassments because I couldn\'t fit on the ride to getting a seat belt extender on an airplane. But one day, I just thought, I want to live a long life and see my daughter grow up. I got the \'No secret diet\' page link from a friend and I took it upon myself to make it work. 60 pounds and still hitting those miles.'];
                        $testimonials[] = ['name' => 'Kayden Woody',         'delay'=>$delay, 'image'=>'kw.jpg',  'text' => 'When I got a dreaded report from my doctor I realized it was time to make a change. Heart disease ran in the family — and I didn\'t want to be susceptible to health problems down the road. That was the trigger, I thought, \'I\'ve got to do something about this,\' I got in on the "No Secret Diet\' Mania. My Blood pressure level is at an all time low. Can I say all praise to the \'No Secret Diet\' package?'];
                        $testimonials[] = ['name' => 'Diana Aidan',          'delay'=>$delay, 'image'=>'da.jpg',  'text' => 'Though I\'d struggled with disordered eating and weight gain for a long time, it wasn\'t until I met my now-second husband that I decided it was time to shape up for good. Plus, as a mother of two, I wanted to be a healthy role model for MY children. The \'No Secret Diet\' came off as an experiment and it grew on me after I began to see results. I am happy. It is worth every penny spent.'];
                        $testimonials[] = ['name' => 'Christopher Dashiell', 'delay'=>$delay, 'image'=>'cd.jpg',  'text' => 'Finding a diet that works in the long-term is hectic When I was overweight, used to skip breakfast and lunch, only to find myself snacking and overeating at night. Fad diets had failed me— so I decided to go all in with the \'No Secret Diet\' hoping it will not fail me. I started to focus on eating clean, whole foods…I eliminated anything processed and BOOM!!! I was getting leaner.'];
                        $testimonials[] = ['name' => 'Harry Caiden',         'delay'=>$delay, 'image'=>'hc.jpg',  'text' => 'I am an optimist at heart; I had to re-work my attitude to stick to a routine of measuring what I ate and exercising diligently. "While I\'m exercising, I\'m telling myself, \'You can do that, you\'re strong, you\'re fast, you\'re amazing,\' and then my body will follow," This "I can" attitude draws from the inspiration gotten from \'The No Secret Diet\'.'];
                        $testimonials[] = ['name' => 'Fran Louella',         'delay'=>$delay, 'image'=>'fl.jpg',  'text' => 'Just a few days shy of my 27th birthday; I suffered a major body breakdown, which left me bedridden. The health scare served as a total wake-up call, and when my doctor encouraged me to take experimental medication to deal with the symptoms, I opted to embark on my own, natural weight loss journey instead. The \'No Secret Diet\' is a life changer.'];
                        $testimonials[] = ['name' => 'Warner Calanthe',      'delay'=>$delay, 'image'=>'wc.jpg',  'text' => 'The secret is starting with confidence-boosting, beginner-level routines, and finding a workout you can squeeze into your life. As busy as my schedule, having a workout routine I can do in between appointments or any time throughout the day is the only way I will get anything done. The \'No Secret Diet\' afforded me the chance to get things done.'];
                        $testimonials[] = ['name' => 'Maggie Holmes',        'delay'=>$delay, 'image'=>'mh.jpg',  'text' => 'During the pregnancy of my third child, I was depressed and watched as the number on the scale went. In all, I gained over 100 pounds and things weren\'t looking good. I came across the \'No Secret Diet\' and in 3 months, I dropped 40 pounds after applying the pointers judiciously. I know for a fact that the \'No Secret Diet\' plan works.'];
                        $testimonials[] = ['name' => 'Pat Redd',             'delay'=>$delay, 'image'=>'pr.jpg',  'text' => 'I committed to doing at least 15 minutes of movement every single day, I started with walking and once I had more energy, gentle yoga and stretching. Fifteen minutes sounds way less intimidating than an hour-long cardio program, so the little steps really matter. That\'s what the \'No Secret Diet\' has taught me.'];
                        $testimonials[] = ['name' => 'Elias Tel',            'delay'=>$delay, 'image'=>'et.jpg',  'text' => 'I was a wrecking train. I couldn\'t go out, socialize or network. I lost a lot of friends down the line. I joined a group on Facebook and found the \'No Secret Diet\'. I was skeptical but after months of application, my energy levels and stamina skyrocketed. By the 8-month mark, I had lost over 100 pounds. '];
                        $testimonials[] = ['name' => 'Claude Kenyon',        'delay'=>$delay, 'image'=>'ck.jpg',  'text' => 'Carving 30 minutes a day for myself has made all of the difference. So many of us tell ourselves that we\'re selfish if we make time for ourselves, especially if we care for our bodies. But the 30 minutes a day I made for myself has made me edge closer to my health goals. Thanks to \'The No Diet\' plan.'];
                        $testimonials[] = ['name' => 'Letty Deniece',        'delay'=>$delay, 'image'=>'ld.jpg',  'text' => 'I was always a stress eater, and between being a lonely wife, I had plenty of difficult emotions to deal with. I realized I needed to make conscientious efforts towards reaching my health goals. I started doing a lot of running and cutting down on my carb intakes. '];
                        $testimonials[] = ['name' => 'Kylie Ebba',           'delay'=>$delay, 'image'=>'ke.jpg',  'text' => 'I began my weight-loss journey with a sky-high goal: to look like the women in fitness magazines. "I honestly had no idea how I could accomplish this feat, but I was determined," I already love working out, so I bought the \'No diet plan\'. I am closer today than I was yesterday to my magazine bod.'];
                        $testimonials[] = ['name' => 'James Wallace',        'delay'=>$delay, 'image'=>'jw.jpg',  'text' => 'The workout was a mix of things that involved high-intensity cardio. I struggled in the first month but grew in motivation. I also cleaned up my diet. From there, I cut out fast food and processed snacks and ate a balanced diet centered on lean protein, fruits, nuts, and whole grains. I am looking sharper than ever.'];
                        $testimonials[] = ['name' => 'Betty Reilly',         'delay'=>$delay, 'image'=>'br.jpg',  'text' => 'Learning how to eat better and cleaner along with exercising five to six days a week took me from 150 pounds in June to 120 pounds in October. I thought the $10 was too much for a guide, I mean we are not talking about a software. But it paid off; I know what works and what doesn\'t.'];
                        $testimonials[] = ['name' => 'Khloe Lee',            'delay'=>$delay, 'image'=>'kl.jpg',  'text' => 'I used to leave the dinner table feeling stuffed. Now I stick to a single serving of everything, and if I\'m still hungry, I\'ll eat a piece of fruit. Going from 2,500 calories a day to about 1,450 by ditching the drive through, avoiding processed foods, and replacing six-sodas-a-day with water.'];
                        $testimonials[] = ['name' => 'Penn Jean',            'delay'=>$delay, 'image'=>'pj2.jpg', 'text' => 'As any woman knows, losing weight isn\'t just about physical health, but emotional wellness as well. For me, it was time to feel better in both of those realms. I\'ve always been about an average weight but started to put on weight because of depression. I got hold of the \'No Secret Diet\' plan and I have seen results, slowly but surely.'];
                        $testimonials[] = ['name' => 'Page Justina',         'delay'=>$delay, 'image'=>'pj.jpg',  'text' => 'I make vision boards of where I want to be and what I want to look at. That was how I ensured the \'No Secret Diet\' became a thing of habit for me. Looking at these boards on a daily basis has trained my mind to ensure my health is my number 1 priority.'];
                        $testimonials[] = ['name' => 'Regana Ashlynn',       'delay'=>$delay, 'image'=>'ra.jpg',  'text' => 'To lose it all, I started with my diet as the \'No Secret Diet\' name implies: "I gave up almost all processed sugar and started to label read like it was a new hobby." With these simple changes (and no odd diet!), I shed 88 pounds in a year. '];
                        $testimonials[] = ['name' => 'Everette Finnegan',    'delay'=>$delay, 'image'=>'ef.jpg',  'text' => 'The \'No Secret Diet\' is the best gift, a friend has ever given me. I discussed my weight-loss struggles, such as finding the energy to work out after a long day, and triumphs on my Facebook page. The process is so therapeutic for me.'];
                        $testimonials[] = ['name' => 'Arn Barret',           'delay'=>$delay, 'image'=>'ab.jpg',  'text' => '"I tried not to set my goals too high," "I wanted to get to 150 pounds but I figured with doing only 15 minutes here and there, there was no way that could ever happen." But after cutting fast food from my diet, and focusing on portion control, just as instructed in the \'No Secret Diet\' plan, I started to lose weight.'];
                        $testimonials[] = ['name' => 'Clive Jerrold',        'delay'=>$delay, 'image'=>'cj.jpg',  'text' => 'I knew my 220 pounds frame was holding me back from being the dad I wanted to be. I was running outside trying to fly a kite and I realized I couldn\'t run around very well. Just realizing I had gotten to the point that I couldn\'t run with my daughter anymore was a wake-up call. The \'No Secret Diet\'.'];
                        $testimonials[] = ['name' => 'Mary Stacey',          'delay'=>$delay, 'image'=>'ms.jpg',  'text' => 'To be very honest, my diet hasn\'t changed too much, but what I think has changed the most is my mindset. Because I\'m not calling it \'a diet,\' because it\'s nothing odd or bizarre just as The No Secret Diet" landing page reads. Instead of doing a huge overhaul, I started making tiny tweaks to my meal plan and I became healthier.'];
                        $testimonials[] = ['name' => 'Kerry Shelton',        'delay'=>$delay, 'image'=>'ks.jpg',  'text' => 'I realized my true goal was to stay in my children\'s lives for as long as possible—and that the key to putting my children first was to prioritize eating healthy and losing weight to be the mom I wanted to be. I wasn\'t obese but slightly overweight. One month down the pipeline and I was back in my normal range.'];
                        $testimonials[] = ['name' => 'Shelly Camron',        'delay'=>$delay, 'image'=>'sc.jpg',  'text' => 'As a mom with two children, I no longer care about being thin as much as I value being healthy. It took me thirty years to realize that weight gain isn\'t a life sentence. When it comes to weight management, you never run out of opportunities to try again. I tried again with the \'No Secret Diet\' and there was no looking back.'];
                        $testimonials[] = ['name' => 'Shelly Wilbur',        'delay'=>$delay, 'image'=>'sw.jpg',  'text' => 'I noticed my blood pressure was rising. While I didn\'t have health issues, I was worried I could be at risk for heart disease. What\'s helped me lose weight is the \'No Secret Diet\' exercising three days a week, usually by walking. It seems simple but is super effective. Best money I ever spent.'];
                        
                        // shuffle($testimonials);

                        foreach($testimonials as $testimonial){ ?>
                                <div class="swiper-slide" data-swiper-autoplay="{{ $testimonial['delay'] }}">
                                    {{-- <div class="swiper-slide"> --}}
                                        <p class="customer_feedback_text white_color open_sans default_text font_200">
                                            <?=$testimonial['text']?>
                                        </p>
                                        <div class="img_text">
                                            <div class="customer_feedback_img" style="background-image: url({{ asset('images/testimonials/'.$testimonial['image']) }});"></div>
                                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">
                                                <?=$testimonial['name']?>
                                            </p>
                                        </div>
                                    </div>
                                    <?php }?>




                                </div>
                                <!-- Add Pagination -->
                                {{-- <div class="swiper-pagination"></div> --}}
                                <!-- Add Arrows -->
                                {{-- <div class="swiper-button-next fa fa-angle-right hidden-sm hidden-xs"></div> --}}
                                {{-- <div class="swiper-button-prev fa fa-angle-left hidden-sm hidden-xs"></div> --}}

                                <div class="swiper-button-next fa fa-angle-right "></div>
                                <div class="swiper-button-prev fa fa-angle-left "></div>
                            </div>
                        </div>
                    </div>
            </section>
            <!-- /Customer Review Slider -->











































































            <!-- Contact Form Section -->
            <section class="contact_form_section" id="contact-form">
                <div class="container">
                    <div class="row">
                        <div class="contact_form_inner big_padding clearfix">
                            <div class="col-md-6 wow slideInLeft">
                                <div class="contact_form_detail  center_on_mobile">
                                    <p class="default_small_heading raleway blue_color font_200">Contact Us</p>
                                    <h3 class="default_section_heading raleway navy_blue"> <span class="font_200">Let's
                                            Get In</span> Touch</h3>
                                    <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                                    <p class="default_text_light default_text open_sans">
                                        {{-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim,
                                        vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque
                                        penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit
                                        amet ligula consequat urna posuere convallis. --}}
                                    </p>
                                    <div class="row">
                                        <div class="contact_form_extra_inner clearfix center_on_mobile">
                                            <div class="col-md-1 col-sm-12 form_padding_left_0">
                                                <i class="fa fa-map-marker blue_color text-center form_icon"
                                                    aria-hidden="true"></i>
                                            </div>
                                            <div class="col-md-11 col-sm-12">
                                                <p class="default_text form_text open_sans default_text_light">
                                                    Dotcom Ventures FZE<span class="symbol">&copy;</span>
                                                </p>
                                                <p class="default_text form_text open_sans default_text_light">
                                                    Business Center
                                                </p>
                                                <p class="default_text form_text open_sans default_text_light">
                                                    Al Shmookh Building
                                                </p>
                                                <p class="default_text form_text open_sans default_text_light">
                                                    UAQ Free Trade Zone
                                                </p>
                                                <p class="default_text form_text open_sans default_text_light">
                                                    Umm Al Quwain, UAE
                                                </p>
                                            </div>
                                        </div>
                                        <div class="contact_form_extra_inner clearfix center_on_mobile">
                                            <div class="col-md-1 col-sm-12 form_padding_left_0">
                                                <i class="fa fa-phone blue_color text-center form_icon" aria-hidden="true"></i>
                                            </div>
                                            <div class="col-md-11 col-sm-12">
                                                <p class="default_text form_text open_sans default_text_light">
                                                    <a href="tel:+97143288751">+971 4 328 8751</a>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="contact_form_extra_inner clearfix center_on_mobile">
                                            <div class="col-md-1 col-sm-12 form_padding_left_0">
                                                <i class="fa fa-envelope blue_color text-center form_icon" aria-hidden="true"></i>
                                            </div>
                                            <div class="col-md-11 col-sm-12">
                                                {{-- <a href="#." class="anchor_style_default"> --}}
                                                <p class="default_text form_text open_sans default_text_light">
                                                    i<span></span>nfo<span></span>@loseweighto<span></span>rgetpaid<span></span>.com
                                                </p>
                                                {{-- </a> --}}
                                                {{-- <a href="#." class="anchor_style_default">
                                                    <p class="default_text form_text open_sans default_text_light">
                                                        www.loseweightorgetpaid.com
                                                    </p>
                                                </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 wow slideInRight">
                                {{-- <form onsubmit="return false" class="form_class"> --}}
                                    <form action="{{ route('enquire') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="mew_form clearfix">
                                                <div class="col-sm-12" id="result"></div>
                                                <div class="col-sm-6">
                                                    <input placeholder="Your Name" class="form_inputs" id="name" name="name"
                                                        required="required">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input placeholder="Email" class="form_inputs" id="email" name="email"
                                                        required="required">
                                                </div>
                                                <div class="col-sm-12">
                                                    <textarea placeholder="Your Message" class="form_inputs form_textarea"
                                                        id="message" name="message" required="required"></textarea>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="button_div  center_on_mobile">
                                                        <input id="submit_handle" type="submit" style="display: none" />
                                                        <a href="javascript:" id="submit_btn" class="bg_pink button button_default_style open_sans bg_before_navy">
                                                            <i class="fa fa-envelope" aria-hidden="true"></i> Send
                                                            Message</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Contact Form  Section -->



































            {{-- https://facebook.com/nosecretdiet --}}
            {{-- https://m.me/nosecretdiet --}}

            <!-- Footer Section -->
            <footer class="footer_section big_padding bg_navy">
                <div class="container">
                    <div class="footer_detail">
                        <ul class="footer_links text-center">

                            <li>
                                <a class="anchor_style_default facebook wow zoomInDown" href="http://facebook.com/nosecretdiet"
                                    target="_blank">
                                    <i class="text-center fa fa-facebook "></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default messenger wow zoomInUp" href="https://m.me/nosecretdiet"
                                    target="_blank">
                                    <i class="text-center fab fa-facebook-messenger"></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default instagram wow zoomInDown" href="https://www.instagram.com/loseweightorgetpaid/"
                                    target="_blank">
                                    <i class="text-center fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default pinterest wow zoomInDown" href="https://www.pinterest.com/loseweightorgetpaid/" target="_blank">
                                    <i class="text-center fab fa-pinterest"></i>
                                </a>
                            </li>


                            {{--
                            <li>
                                <a class="anchor_style_default twitter wow zoomInUp" href="#.">
                                    <i class="text-center fa fa-twitter "></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default g_plus wow zoomInDown" href="#.">
                                    <i class="text-center fa fa-google-plus "></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default linkedin wow zoomInUp" href="#.">
                                    <i class="text-center fa fa-linkedin "></i>
                                </a>
                            </li>
                            --}}
                        </ul>
                        <p class="text-center default_text open_sans white_color">LoseWeightOrGetPaid.com<span class="symbol">&copy;</span>
                            is a registered trademark of <a class="dotcomv" href="https://dotcomv.com" target="_blank">Dotcom Ventures FZE<span class="symbol">&copy;</span></a></p>

                <p class="text-center default_text open_sans white_color pp_tc">
                    <a href="#" data-target="#pp_modal" data-toggle="modal">Privacy Policy</a>
                    <span class="period"><i class="fas fa-circle"></i></span>
                    <a href="#" data-target="#tc_modal" data-anchor="#refundpolicy" data-toggle="modal" class="scroll_toanchor">Refund Policy</a>
                    <span class="period"><i class="fas fa-circle"></i></span>
                    <a href="#" data-target="#tc_modal" data-toggle="modal">Terms & Conditions</a>
                </p>

                <div class="payment_methods">
                    <div>                        
                        <i title="Visa" class="fab fa-cc-visa"></i>
                        <i title="Mastercard" class="fab fa-cc-mastercard"></i>
                    </div>

                    <div>                        
                        <i title="PayPal" class="fab fa-paypal"></i>
                        <i title="Google Pay" class="fab fa-google"></i>
                        <i title="Apple Pay" class="fab fa-apple"></i>
                        <img title="Samsung Pay" class="samsung-pay" src="{{ asset('images/payment-method-logos/samsung-pay.png') }}"/>
                        {{-- <i title="Amazon Pay" class="fab fa-amazon-pay"></i> --}}
                    </div>
                    
                    <div>
                        <div class="swiper-container cryptocurrencies white_pagination">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><i title="Bitcoin" class="fab fa-bitcoin"></i></div>
                                <div class="swiper-slide"><img title="Bitcoin Cash" class="" src="{{ asset('images/payment-method-logos/bitcoin-cash.png') }}"/></div>
                                <div class="swiper-slide"><img title="Dash" class="" src="{{ asset('images/payment-method-logos/dash.png') }}"/></div>
                                <div class="swiper-slide"><img title="Doge Coin" class="" src="{{ asset('images/payment-method-logos/doge.png') }}"/></div>
                                <div class="swiper-slide"><img title="Ethereum" class="" src="{{ asset('images/payment-method-logos/ethereum.png') }}"/></div>
                                <div class="swiper-slide"><img title="Ethereum Classic" class="" src="{{ asset('images/payment-method-logos/ethereum-classic.png') }}"/></div>
                                <div class="swiper-slide"><img title="Lite" class="" src="{{ asset('images/payment-method-logos/lite.png') }}"/></div>
                                <div class="swiper-slide"><img title="Monero" class="" src="{{ asset('images/payment-method-logos/monero.png') }}"/></div>
                                <div class="swiper-slide"><img title="Neo" class="" src="{{ asset('images/payment-method-logos/neo.png') }}"/></div>
                                <div class="swiper-slide"><img title="Ripple" class="" src="{{ asset('images/payment-method-logos/ripple.png') }}"/></div>
                                <div class="swiper-slide"><img title="Tether" class="" src="{{ asset('images/payment-method-logos/tether.png') }}"/></div>
                                <div class="swiper-slide"><img title="Zcash" class="" src="{{ asset('images/payment-method-logos/zcash.png') }}"/></div>
                            </div>
                        </div>
                        <div class="ccs_nav">                            
                            <div class="swiper-button-next fa fa-angle-right "></div>
                            <div class="swiper-button-prev fa fa-angle-left "></div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- /Footer Section -->






















            <div class="to_top sticky_buttons">
                <i class="fas fa-angle-up"></i>
            </div>
            

            <div class="share_facebook sticky_buttons">
                <a title="Share in Facebook" href="fb-messenger://share/?link= https%3A%2F%2Floseweightorgetpaid.com">
                    <i class="fab fa-facebook-messenger"></i>
                </a>
            </div>


            <div class="share_whatsapp sticky_buttons">
                <a title="Share via Whatsapp"
                    href="https://api.whatsapp.com/send?text= https%3A%2F%2Floseweightorgetpaid.com">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </div>


















            {{-- modal --}}
            {{-- ///////////////////////////////////////////////// --}}

            <div class="modal about-modal fade" id="lwgp_modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">
                                <img src="{{ asset('images/logo-65.png') }}">
                                Choose Payment Method
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-info">
                                <div class="payment_method_btns">
                                    <div>
                                        <button type="button" class="btn btn-primary btn-lg disabled">
                                            <i class="fab fa-cc-visa"></i>
                                        </button>
                                        <h4>Visa</h4>
                                    </div>

                                    <div>
                                        <button type="button" class="btn btn-primary btn-lg disabled">
                                            <i class="fab fa-cc-mastercard"></i>
                                        </button>
                                        <h4>Mastercard</h4>
                                    </div>


                                    <div>
                                        <a href="#" class="btn btn-primary btn-lg" target="_blank">
                                            <i class="fab fa-paypal"></i>
                                        </a> 

                                       <!--  <button class="btn btn-primary btn-lg disabled">
                                            <i class="fab fa-paypal"></i>
                                        </button> -->
                                        
                                        <h4>PayPal</h4>
                                    </div>


                                    <div>
                                        <button type="button" class="btn btn-primary btn-lg disabled">
                                            <i class="fab fa-google"></i>
                                        </button>
                                        <h4>Google Pay</h4>
                                    </div>

                                    <div>
                                        <button type="button" class="btn btn-primary btn-lg disabled">
                                            <i class="fab fa-apple"></i>
                                        </button>
                                        <h4>Apple Pay</h4>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-primary btn-lg has_img disabled">
                                            <img title="Samsung Pay" class="samsung-pay" src="{{ asset('images/payment-method-logos/samsung-pay.png') }}" />
                                        </button>
                                        <h4>Samsung Pay</h4>
                                    </div>

                                    <div>                                        
                                       {{--  <button type="submit" class="btn btn-primary btn-lg disabled">
                                            <i class="fab fa-bitcoin"></i>
                                        </button>
                                        <h4>Cryptocurrency</h4> --}}

                                        <form action="https://www.coinpayments.net/index.php" method="post" target="_blank">
                                            <input type="hidden" name="cmd" value="_pay">
                                            <input type="hidden" name="reset" value="1">
                                            <input type="hidden" name="merchant" value="338146889f6047365315e530a3075f65">
                                            <input type="hidden" name="item_name" value="The &#039;No Secret Diet&#039; Package">
                                            <input type="hidden" name="item_number" value="1">
                                            <input type="hidden" name="currency" value="USD">
                                            <input type="hidden" name="amountf" value="9.99000000">
                                            <input type="hidden" name="success_url" value="{{ route('coin.success') }}">
                                            <input type="hidden" name="quantity" value="1">
                                            <input type="hidden" name="allow_quantity" value="0">
                                            <input type="hidden" name="want_shipping" value="0">
                                            <input type="hidden" name="allow_extra" value="0">
                                            <button type="submit" class="btn btn-primary btn-lg">
                                                <i class="fab fa-bitcoin"></i>
                                            </button>
                                            <h4>Cryptocurrency</h4>
                                        </form>
                                    
                                    </div>

                                </div>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="payment_coming_soon">
                                <h3><span>Coming soon..</span></h3>
                            </div>
                            <button type="button" class="btn btn-secondary close_button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>




            <div class="modal about-modal fade" id="pp_modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">
                                <img src="{{ asset('images/logo-65.png') }}">
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-info">

                                <div class="document">

                                    <h2>Privacy Policy</h2>

                                    <div class="document_text_content">

                                        <p>By accessing or using www.loseweightorgetpaid.com (the "Website"), a website
                                            owned and maintained by Dotcom Ventures FZE ("Company", "we," "us" or
                                            "our"), you consent to the information collection, disclosure and use
                                            practices described in this Privacy Policy. This Privacy Policy applies to
                                            all services provided by us and sets out how we may collect, use and
                                            disclose information in relation to users of the Website.</p>
                                        <p>The Website is a health / weight lose consultancy website, which offers "No
                                            Secret Diet" package for weight loss and other health related tips to its
                                            clients ("Services"). The ‘No Secret Diet’ will help you achieve your
                                            weight loss goals as you will be required to follow a guide that takes a
                                            holistic outlook on weight loss.</p>
                                        <p>Use of Website, including all materials presented herein and all Services
                                            provided by us, is subject to separate Terms and Conditions and the
                                            following Privacy Policy.</p>

                                        <h3>Information We Collect</h3>
                                        <p>Your privacy is important to us and we have taken steps to ensure that we do
                                            not collect more information from you than is necessary for us to provide
                                            you with our services and to protect your account.</p>
                                        <p>We may process the following categories of personal data about you:</p>
                                        <ul>
                                            <li><strong>Personal Data</strong> may include name, email, address, date
                                                of birth, gender, age, phone number, weight, Height, BMI, Debit/Credit
                                                Card details and such other data as we may deem necessary for provision
                                                of Services. When you contact us through any of the communication modes
                                                as mentioned below, your email address may be added to our mailing list
                                                from which you can unsubscribe at any time using the unsubscribe link
                                                in each email or by contacting us at info@loseweightorgetpaid.com.</li>
                                            <li><strong>Communication Data</strong> may include any communication that
                                                you send to us whether that be through the contact form on our Website,
                                                through email, text, social media messaging, social media posting or
                                                any other communication that you send us. We process this data for the
                                                purposes of communicating with you, for record keeping and for the
                                                establishment, pursuance or defence of legal claims. Our lawful ground
                                                for this processing is our legitimate interests which in this case are
                                                to reply to communications sent to us, to keep records and to
                                                establish, pursue or defend legal claims.</li>
                                            <li><strong>Technical Data</strong> may include data about your use of our
                                                Website and online services such as your IP address, your login data,
                                                details about your browser, length of visit to pages on our Website,
                                                page views and navigation paths, details about the number of times you
                                                use our Website, time zone settings and other technology on the devices
                                                you use to access our Website. The source of this data is from our
                                                analytics tracking system. We process this data to analyse your use of
                                                our Website and other online services, to administer and protect our
                                                business and Website, to deliver relevant website content and
                                                advertisements to you and to understand the effectiveness of our
                                                advertising. Our lawful ground for this processing is our legitimate
                                                interests which in this case are to enable us to properly administer
                                                our Website and our business and to grow our business and to decide our
                                                marketing strategy.</li>
                                            <li><strong>Marketing Data</strong> may include data about your preferences
                                                in receiving marketing from us and our third parties and your
                                                communication preferences. We process this data to enable you to
                                                partake in our promotions such as competitions, prize draws and free
                                                give-aways, to deliver relevant website content and advertisements to
                                                you and measure or understand the effectiveness of this advertising.
                                                Our lawful ground for this processing is our legitimate interests which
                                                in this case are to study how customers use our products/services, to
                                                develop them, to grow our business and to decide our marketing
                                                strategy.</li>
                                        </ul>
                                        <p>We may use Personal Data, User Data, Technical Data and Marketing Data (the
                                            "Data") to provide you with Services and to deliver relevant Website
                                            content and advertisements to you and to measure or understand the
                                            effectiveness of the advertising we serve you. We may also use such data to
                                            send other marketing communications to you.</p>


                                        <h3>Consent and its Withdrawal</h3>
                                        <p>When you visit our Website you provide us consent to use your Data as per
                                            this Privacy Policy. In order to provide you with the Services, it is
                                            necessary for us to collect all relevant and necessary Data about you from
                                            you.</p>
                                        <p>If you change your mind, you may withdraw your consent for us to contact
                                            you, for the continued collection, use or disclosure of your information,
                                            at any time, by contacting us at info@loseweightorgetpaid.com.</p>



                                        <h3>Promotional Email</h3>
                                        <p>You agree that the Website and/or third parties may from time to time send
                                            e-mail messages to you which offer products and services, contain
                                            advertisements, promotions, subscriptions or registration-based services or
                                            other material. If you wish to discontinue receiving such email, you may
                                            opt-out by writing us through email at info@loseweightorgetpaid.com. Your
                                            preferences will then be updated.</p>




                                        <h3>How We Use and Process the Data</h3>
                                        <p>The Data collected by us from you may be used to provide you with Services
                                            and better understand your needs related services and programs, to
                                            correspond with you and reply to your questions with about our services.</p>
                                        <p>We will not rent or sell your Data to others. We may store the Data in
                                            locations outside the direct control of the Company (for instance, on
                                            servers or databases co-located with hosting providers).</p>
                                        <p>If you provide any Data to us, you are deemed to have authorized us to
                                            collect, retain and use that data for the following purposes:</p>
                                        <ul>
                                            <li>verifying your identity;</li>
                                            <li>providing you with customer service and responding to your queries,
                                                feedback, or disputes;</li>
                                            <li>making such disclosures as may be required for any of the above
                                                purposes or as required by law, regulations and guidelines or in
                                                respect of any investigations, claims or potential claims brought on or
                                                against us;</li>
                                            <li>provide and maintain the Services;</li>
                                            <li>notify you about changes to our Services;</li>
                                        </ul>

                                        <p>We shall ensure that:</p>
                                        <ul>
                                            <li>The Data collected and processed for and on our behalf by any party is
                                                collected and processed fairly and lawfully;</li>
                                            <li>You are always made fully aware of the reasons for the collection of
                                                Data and are given details of the purpose(s) for which the data will be
                                                used;</li>
                                            <li>The Data is only collected to the extent that is necessary to fulfil
                                                the purpose(s) for which it is required;</li>
                                            <li>No Data is held for any longer than necessary in light of the
                                                purpose(s) for which it is required.</li>
                                            <li>Whenever cookies or similar technologies are used online by us, they
                                                shall be used strictly in accordance with the law;</li>
                                            <li>You are informed if any data submitted by you online cannot be fully
                                                deleted at your request under normal circumstances and how to request
                                                that the we delete any other copies of that data, where it is within
                                                your right to do so;</li>
                                            <li>All Data is held in a safe and secure manner taking all appropriate
                                                technical and organisational measures to protect the data;</li>
                                            <li>All data is transferred securely, whether it is transmitted
                                                electronically or in hard copy.</li>
                                            <li>You can fully exercise your rights with ease and without hindrance.</li>
                                        </ul>

                                        <h3>Disclosure of Data</h3>
                                        <p>We shall not be able to keep your Data private in response to legal process
                                            i.e., a court order or a subpoena, a law enforcement agency’s request. If,
                                            in our view, it is deemed appropriate to investigate, prevent, or take
                                            action regarding illegal activities, suspected fraud, situations involving
                                            potential threats to the physical safety of any person, violations of our
                                            terms of use, or as otherwise required by law, we may be compelled to
                                            disclose the Data and Personal Data. Moreover, in case of takeover, merger
                                            or acquisition, we reserve a right to transfer your data to new platform.</p>
                                        <p>We may disclose the Data in the good faith belief that such action is
                                            necessary to:</p>
                                        <ul>
                                            <li>comply with a legal obligation</li>
                                            <li>protect and defend our rights or property </li>
                                            <li>prevent or investigate possible wrongdoing </li>
                                            <li>protect the personal safety of users of the Service or the public</li>
                                            <li>protect against legal liability</li>
                                        </ul>
                                        <p>When necessary, we may also disclose and transfer your Data to our
                                            professional advisers, law enforcement agencies, insurers, government and
                                            regulatory and other organizations.</p>


                                        <h3>Data Storage</h3>
                                        <p>Your Data may be stored and processed at the servers in the United States,
                                            Europe, or any other country in which the Website or its subsidiaries,
                                            affiliates or service providers maintain facilities.</p>
                                        <p>The Website may transfer Data to affiliated entities, or to other third
                                            parties across borders and from your country or jurisdiction to other
                                            countries or jurisdictions around the world. Your consent to this Privacy
                                            Policy followed by your submission of such information represents your
                                            agreement to that transfer.</p>
                                        <p>We will take all steps reasonably necessary to ensure that your data is
                                            treated securely and in accordance with this Privacy Policy and no transfer
                                            of your Data will take place to an organization or a country unless there
                                            are adequate controls in place including the security of your data and
                                            other personal information.</p>
                                        <p>We will only retain your Data preferably for as long as necessary to fulfil
                                            the purposes we collected it for, including for the purposes of satisfying
                                            any legal, see section regarding insurance requirement and reporting
                                            requirements. When deciding what the correct time is to keep the Data for
                                            we look at its amount, nature and sensitivity, potential risk of harm from
                                            unauthorised use or disclosure, the processing purposes, if these can be
                                            achieved by other means and legal requirements.</p>


                                        <h3>How We Protect Your Information</h3>
                                        <p>We store all the Data submitted by you through Website at a secure database.</p>
                                        <p>We are concerned with protecting your privacy and data, but we cannot ensure
                                            or warrant the security of any data you transmit to or guarantee that your
                                            Data may not be accessed, disclosed, altered or destroyed by breach of any
                                            of our industry standard physical, technical or managerial safeguards.</p>
                                        <p>No method of transmission over the Internet or method of electronic
                                            Therefore, we cannot guarantee its absolute security. If you have any
                                            questions about security of our Website, you can contact us at
                                            info@loseweightorgetpaid.com.</p>
                                        <p>Any Data supplied by you will be retained by us and will be accessible by
                                            our employees, any service providers engaged by us and third parties.</p>


                                        <h3>The Rights of Users </h3>
                                        <p>You may exercise certain rights regarding your Data processed by us. In
                                            particular, you have the right to do the following:</p>
                                        <ul>
                                            <li><strong>Withdraw consent at any time.</strong> You have the right to
                                                withdraw consent where you have previously given your consent to the
                                                processing of your Data.</li>
                                            <li><strong>Object to processing of Data.</strong> You have the right to
                                                object to the processing of your Data if the processing is carried out
                                                on a legal basis other than consent. </li>
                                            <li><strong>Access to the Data.</strong> You have the right to learn if
                                                Data is being processed by us, obtain disclosure regarding certain
                                                aspects of the processing and obtain a copy of the Data undergoing
                                                processing.</li>
                                            <li><strong>Verify and seek rectification.</strong> You have the right to
                                                verify the accuracy of your Personal and other Data and ask for it to
                                                be updated or corrected.</li>
                                            <li><strong>Restrict the processing of your Data.</strong> You have the
                                                right, under certain circumstances, to restrict the processing of Data.
                                                In this case, we will not process your Data for any purpose other than
                                                storing it.</li>
                                            <li><strong>Have the Data deleted or otherwise removed.</strong> You have
                                                the right, under certain circumstances, to obtain the erasure of the
                                                Data from us.</li>
                                        </ul>
                                        <p>You may initiate request with us at info@loseweightorgetpaid.com to exercise
                                            any of the above mentioned rights. We shall review your request and, in our
                                            own discretion, honor your request, if deemed necessary by us, within
                                            reasonable time.</p>


                                        <h3>Compliance with the GDPR</h3>
                                        <p>For users based in the European Union (EU), the Website shall make all
                                            reasonable efforts to ensure that it complies with The <strong>General Data
                                                Protection Regulation (GDPR) (EU) 2016/679</strong> as set forth by the
                                            European Union regarding the collection, use, and retention of Data from
                                            European Union member countries. Website shall make all reasonable efforts
                                            to adhere to the requirements of notice, choice, onward transfer, security,
                                            data integrity, access and enforcement.</p>


                                        <h3 id="cookies">Cookies</h3>
                                        <p>We use technologies, such as cookies, to make better user experience,
                                            customise content and advertising, to provide social media features and to
                                            analyse traffic to the Website. Where applicable the Website uses a cookie
                                            control system allowing the user on their first visit to the Website to
                                            allow or disallow the use of cookies on their computer / device.</p>
                                        <p>We also share information about your use of our site with our trusted social
                                            media, advertising and analytics partners. Third parties, including
                                            Facebook and Google, may use cookies, web beacons, and other storage
                                            technologies to collect or receive your information from the Websites and
                                            use that information to provide measurement services and target ads.</p>
                                        <p>Cookies are small files saved to the user's computers’ or mobile devices’
                                            hard drive or memory that track, save and store information about the
                                            user's interactions and usage of the Website. This allows the Website,
                                            through its server to provide the users with a tailored experience within
                                            this Website.</p>
                                        <p>Users are advised that if they wish to deny the use and saving of cookies
                                            from this Website on to their computers hard drive they should take
                                            necessary steps within their web browsers security settings to block all
                                            cookies from this Website and its external serving vendors.</p>
                                        <p>We may gather certain information automatically and store it in log files.
                                            This information includes Internet protocol (IP) addresses, browser type,
                                            Internet service provider (ISP), referring/exit pages, operating system,
                                            date/time stamp, and click stream data. We may use this information, which
                                            does not identify individual users, to analyze trends, to administer the
                                            Website, to track users’ movements around the Website and to gather
                                            demographic information about our user base as a whole.</p>
                                        <p>We may track the referring URL (the web page you left before coming to the
                                            Website) and the pages, links, and graphics of the Website you visited. We
                                            do so because it allows us to evaluate the reputation and responsiveness of
                                            specific web pages and any promotional programs we may be running.</p>
                                        <p>Managing Cookies: Many web browsers allow you to manage your preferences.
                                            You ca set your browser to refuse cookies or delete certain cookies. You
                                            may be able to manage other technologies in the same way that you manage
                                            cookies using your browser’s preferences.</p>
                                        <p>Please note that if you choose to block cookies, doing so may impair the
                                            Website Services or prevent certain elements of it from functioning.</p>


                                        <h3>Third Party Links</h3>
                                        <p>The Website may contain links to third-party websites, plug-ins and
                                            applications. Except as otherwise discussed in this Privacy Policy, this
                                            document only addresses the use and disclosure of information we collect
                                            from you on our Website. Other websites accessible through our site via
                                            links or otherwise have their own policies in regard to privacy. We are not
                                            responsible for the privacy policies or practices of third parties. When
                                            you leave our Website, we encourage you to read the privacy notice of every
                                            website you visit.</p>


                                        <h3>Changes to this Privacy Statement</h3>
                                        <p>We may modify these this Privacy Policy from time to time, and any such
                                            change shall be reflected on the Website with the updated version of the
                                            Privacy Policy and you agree to be bound to any changes to the updated
                                            version of Privacy Policy when you use the Website or its services.</p>
                                        <p>You acknowledge and agree that it is your responsibility to review this
                                            Website and this Policy periodically and to be aware of any modifications.
                                            Updates to this Policy will be posted on this page.</p>
                                        <p>Also, occasionally there may be information on the Website that contains
                                            typographical errors, inaccuracies or omissions that may relate to service
                                            descriptions, pricing, availability, and various other information, and the
                                            Website reserves the right to correct any errors, inaccuracies or omissions
                                            and to change or update the information at any time, without prior notice.</p>


                                        <h3>Contact US</h3>
                                        <p>If you have questions about our Privacy Policy, please contact us via email:
                                            info@loseweightorgetpaid.com.</p>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal about-modal fade hasScrollTo" id="tc_modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">
                                <img src="{{ asset('images/logo-65.png') }}">

                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-info">


                                <div class="document">

                                    <h2>Terms & Conditions</h2>

                                    <div class="document_text_content">


                                        <h3>Introduction</h3>
                                        <p>Please carefully read the Terms and Conditions ("Terms") for the website
                                            located at www.loseweightorgetpaid.com (the "Website") including its
                                            sub-domains and mobile optimized version, as set out hereinafter. The
                                            Website is operated by Dotcom Ventures FZE (hereinafter referred to also as
                                            "Company") Any ancillary terms, guidelines, the privacy policy (the
                                            "Policy") and other documents made available by the Website from time to
                                            time and as incorporated herein by reference, shall be deemed as an
                                            integral part of the Terms. These terms set forth the legally binding
                                            agreement between you as the user(s) of the Website (hereinafter referred
                                            to as "you", "your" or "User") and the Company. If you are using the
                                            Website or its services on behalf of an entity, organization, or company
                                            (collectively "Subscribing Organization"), you declare that you are an
                                            authorized representative of that Subscribing Organization with the
                                            authority to bind such organization to these Terms; and agree to be bound
                                            by these Terms on behalf of such Subscribing Organization. In such a case,
                                            "you" in these Terms refers to your Subscribing Organization, and any
                                            individual authorized to use the Service on behalf of the Subscribing
                                            Organization, including you.</p>
                                        <p>By using company provided services, accessing or using the Website in any
                                            manner as laid down herein, including, but not limited to, visiting or
                                            browsing it, or contributing content or other materials to it, you agree to
                                            be bound by these Terms. </p>
                                        <p>These Terms, and any rights and licenses granted hereunder, may not be
                                            transferred or assigned by you, but may be assigned by the Website without
                                            restriction. Any attempted transfer or assignment in violation hereof shall
                                            be null and void. </p>


                                        <h3>Acceptance of the Terms</h3>
                                        <p>Each time by viewing, using, accessing, browsing, or submitting any content
                                            or material on the Website, including the webpages contained or hyperlinked
                                            therein and owned or controlled by the Website and its services, whether
                                            through the Website itself or through such other media or media channels,
                                            devices, software, or technologies as the Website may choose from time to
                                            time, you are agreeing to abide by these Terms, as amended from time to
                                            time with or without your notice. </p>
                                        <p>The Website reserves the right to modify or discontinue, temporarily or
                                            permanently, and at any time, the Website and/or the Website Services (or
                                            any part thereof) with or without notice. You agree that the Website shall
                                            not be liable to you or to any third party for any modification, suspension
                                            or discontinuance of the Website Services.</p>
                                        <p>Website or the Website management may modify these Terms from time to time,
                                            and any change to these Terms will be reflected on the Website with the
                                            updated version of the Terms and you agree to be bound to any changes to
                                            these Terms when you use the Website or the Website Services. The Website
                                            may also, in its sole and absolute discretion, choose to alert via email
                                            all users with whom it maintains email information of such modifications.</p>
                                        <p>Also, occasionally there may be information on the Website or within the
                                            Website Services that contains typographical errors, inaccuracies or
                                            omissions that may relate to service descriptions, pricing, availability,
                                            and various other information, and the Website management reserves the
                                            right to correct any errors, inaccuracies or omissions and to change or
                                            update the information at any time, without prior notice. </p>
                                        <p>When you register an account on the Website and/or upload, submit, enter any
                                            information or material to the Website or use any of the Website Services,
                                            you shall be deemed to have agreed to and understand the Terms. <br> IF YOU
                                            DO NOT AGREE TO THESE TERMS OF USE, THEN YOU MAY NOT USE THE WEBSITE</p>


                                        <h3>Terms of Service</h3>
                                        <p>The Website is a health / weight lose consultancy website, which offers "No
                                            Secret Diet" package for weight loss and other health related tips to its
                                            clients ("Services"). The ‘No Secret Diet’ will help you achieve your
                                            weight loss goals as you will be required to follow a guide that takes a
                                            holistic outlook on weight loss.</p>
                                        <p>The "No Secrete Diet" does not involve any medication or other treatment.
                                            Rather, it is based on the old but gold rules of weight loss.</p>
                                        <p>The one time price for "No Secrete Diet" is US$9.99. You will get an e-book
                                            for such consideration.</p>
                                        <p>All payments shall be made through secure gateway. The acceptable payment
                                            modes shall be Debit/Credit Cards, selected cryptocurrencies, Amazon Pay,
                                            Samsung Pay, Apply Pay, and PayPal. It shall be in the sole discretion of
                                            the Company to allow or disallow a particular payment mode/channel. The
                                            Website shall not be liable if a certain payment method does not work at a
                                            given point of time.</p>
                                        <p>You will receive $9.99 from us if you send us a video testimonial telling
                                            us/the community about how the "No Secret Diet" has worked for you.</p>


                                        <h3 id="refundpolicy">Refunds</h3>
                                        <p>The Website will refund you your paid amount and pay you a total amount of
                                            $100 if no visible change occurs in your body after applying the methods as
                                            contained the "No Secret Diet" package 8 weeks.</p>
                                        <p>You may send us proof of non-effectiveness of the "No Secret Diet" by
                                            sending us ‘before and after’ photos of yourself with a weight machine
                                            having a date and time feature on it.</p>
                                        <p>It shall be in the sole discretion of the Website to accept your proof and
                                            to refund. The decision of the Website shall be final. You will be informed
                                            of the decision within a reasonable time.</p>


                                        <h3>Service Availability</h3>
                                        <p>The Website shall use commercially reasonable efforts to keep it up and
                                            running 24 hours a day, seven days a week; provided, however, that it may
                                            carry out scheduled and unscheduled maintenance work as necessary from time
                                            to time and such maintenance work may impact the availability of the
                                            Website.</p>


                                        <h3>Account Registration and Membership</h3>
                                        <p>In order to use some or all of the functionalities and Services provided
                                            through the Website, you may be required to register an account with the
                                            Website. At the time of registration of account, you will be asked to
                                            complete a registration form which shall require you to provide personal
                                            information such as name, address, phone number, email address, username
                                            and other personal information.</p>
                                        <p>Upon verification of details, Website may accept account registration
                                            application.</p>
                                        <p>You represent, warrant and covenant that: (i) you have full power and
                                            authority to accept these Terms, to grant any license and authorization and
                                            to perform any of your obligations hereunder; (ii) you will undertake the
                                            use the Website and Services for personal purposes only; and (iii) the
                                            address you provide when registering is your personal address.</p>
                                        <ul>
                                            <li>You must not allow any other person to use your account to access the
                                                Website.</li>
                                            <li>You must notify us in writing immediately if you become aware of any
                                                unauthorised use of your account.</li>
                                            <li>You must not use any other person's account to access the Website,
                                                unless you have that person's express written permission to do so.</li>
                                        </ul>


                                        <h3>User IDs and passwords</h3>
                                        <p>If you register for an account with the Website, you will be asked to choose
                                            a user ID and password. Your user ID must not be misleading and must comply
                                            with the content rules set out in this document; you must not use your
                                            account or user ID for or in connection with the impersonation of any
                                            person.</p>
                                        <p>You shall be responsible to maintain the confidentiality of your password
                                            and shall be responsible for all uses via your registration and/or login,
                                            whether authorized or unauthorized by you. You agree to immediately notify
                                            us of any unauthorized use or your registration, user account or password.</p>
                                        <p>You must notify the Website by emailing us info@loseweightorgetpaid.com if
                                            you have reason to believe that your account is no longer secure for any
                                            reason (for example, in the event of a loss, theft or unauthorized
                                            disclosure or use of your password).</p>
                                        <p>You are responsible for any activity and content on the account arising out
                                            of any failure to keep your password confidential, and may be held liable
                                            for any losses arising out of such a failure.</p>
                                        <p>Registration data and other personally identifiable information that we may
                                            collect is subject to the terms of our Privacy Policy.</p>


                                        <h3>Termination of Account and Discontinuation of Use</h3>
                                        <p>If you engage in Prohibited Conduct or otherwise violate any of the Terms,
                                            your permission to use the Website will be terminated.</p>
                                        <p>You also agree that we may, at any time and without notice to you, suspend
                                            or revoke your access to and use of the Website, and any accounts you may
                                            have in connection with the Wervice including: (i) where we determine in
                                            our sole discretion that such action is reasonable in order to comply with
                                            legal requirements or to protect the rights or interests of Company or any
                                            third party; or (ii) in connection with any general discontinuation of the
                                            Website services.</p>
                                        <p>We will have no liability whatsoever on account of any change to the Service
                                            or any suspension or revocation of your access to or use of the Website.
                                            You may terminate your account at any time by sending us an email to
                                            info@loseweightorgetpaid.com. </p>


                                        <h3>Electronic Signature Consent</h3>
                                        <p>You agree that your "Electronic Signature" is the legal equivalent of your
                                            manual signature for this Agreement, thereby indicating your consent to do
                                            business electronically.</p>
                                        <p>By clicking on the applicable button in the Website, you will be deemed to
                                            have executed these Terms electronically via your Electronic Signature with
                                            Company; effective on the date you first click to accept these Terms. </p>


                                        <h3>Electronic Delivery of Communications</h3>
                                        <p>You agree to receive communications from Website in electronic form. Such
                                            electronic communications may include, but will not be limited to, any and
                                            all current and future notices and/or disclosures that various laws or
                                            regulations require that we provide to you, as well as such other
                                            documents, statements, data, records and any other communications regarding
                                            your relationship with the Website.</p>
                                        <p>You accept that the electronic documents, files and associated records
                                            provided via your account with Website are reasonable and proper notice,
                                            for the purpose of any and all laws, rules, and regulations, and you
                                            acknowledge and agree that such electronic form fully satisfies any
                                            requirement that such communications be provided to you in writing or in a
                                            form that you may keep. Website reserves the right to require ink
                                            signatures on hard copy documents from the related parties, at any time.</p>


                                        <h3>User Responsibility</h3>
                                        <p>Users are solely responsible for all of the transactions conducted on,
                                            through or as a result of use of the Website or Services.</p>
                                        <p>You agree that the use of the Website and/or the Website Services on the
                                            Website is subject to all applicable local, state and federal laws and
                                            regulations. You also agree:</p>
                                        <ul>
                                            <li>not to access the Website or services using a third-party's
                                                account/registration without the express consent of the account holder;</li>
                                            <li>not to use the Website for illegal purposes;</li>
                                            <li>not to commit any acts of infringement on the Website or with respect
                                                to content on the Website;</li>
                                            <li>not to copy any content for republication in print or online;</li>
                                            <li>not to create reviews or blog entries for or with any purpose or intent
                                                that does not in good faith comport with the purpose or spirit of the
                                                Website;</li>
                                            <li>not to attempt to gain unauthorized access to other computer systems
                                                from or through the Website;</li>
                                            <li>not to interfere with another person's use and enjoyment of the Website
                                                or another entity's use and enjoyment of the Website;</li>
                                            <li>not to upload or transmit viruses or other harmful, disruptive or
                                                destructive files; and/or</li>
                                            <li>not to disrupt, interfere with, or otherwise harm or violate the
                                                security of the Website, or any services, system restores, accounts,
                                                passwords, servers or networks connected to or accessible through the
                                                Website or affiliated or linked website.</li>
                                            <li>not to use the Website in any way or take any action that causes, or
                                                may cause, damage to the Website or impairment of the performance,
                                                availability or accessibility of the Website;</li>
                                            <li>not to use the Website in any way that is unlawful, illegal, fraudulent
                                                or harmful, or in connection with any unlawful, illegal, fraudulent or
                                                harmful purpose or activity;</li>
                                            <li>not to use se the Website to copy, store, host, transmit, send, use,
                                                publish or distribute any material which consists of (or is linked to)
                                                any spyware, computer virus, Trojan horse, worm, keystroke logger,
                                                rootkit or other malicious computer software;</li>
                                            <li>not to conduct any systematic or automated data collection activities
                                                (including without limitation scraping, data mining, data extraction
                                                and data harvesting) on or in relation to the Website without the
                                                express written consent of the Website owner;</li>
                                            <li>not to access or otherwise interact with the Website using any robot,
                                                spider or other automated means;</li>
                                            <li>not to violate the directives set out in the robots.txt file for the
                                                website;</li>
                                            <li>not to use data collected from the website for any direct marketing
                                                activity (including without limitation email marketing, SMS marketing,
                                                telemarketing and direct mailing);</li>
                                            <li>not to infringe these Terms or allow, encourage or facilitate others to
                                                do the same;</li>
                                            <li>not to plagiarize and/or infringe the intellectual property rights or
                                                privacy rights of any third party;</li>
                                            <li>not to disturb the normal flow of Services provided within the Website;</li>
                                            <li>not to create a link from the Website to another website or document
                                                without Company’s prior written consent;</li>
                                            <li>not to obscure or edit any copyright, trademark or other proprietary
                                                rights notice or mark appearing on the Website;</li>
                                            <li>not to create copies or derivate works of the Website or any part
                                                thereof;</li>
                                            <li>not to reverse engineer, decompile or extract the Website’s source
                                                code;</li>
                                            <li>not to remit or otherwise make or cause to deliver unsolicited
                                                advertising, email spam or other chain letters;</li>
                                            <li>not to collect, receive, transfer or disseminate any personally
                                                identifiable information of any person without consent from title
                                                holder; and/or</li>
                                            <li>not to pretend to be or misrepresent any affiliation with any legal
                                                entity or third party.</li>
                                        </ul>
                                        <p>In addition to the above clause, unless specifically endorsed or approved by
                                            the Website, the following uses and activities of and with respect to the
                                            Website and the Website Services are prohibited:</p>
                                        <ul>
                                            <li>criminal or tortuous activity, including child pornography, fraud,
                                                trafficking in obscene material, drug dealing, gambling, harassment,
                                                stalking, spamming, copyright infringement, patent infringement, or
                                                theft of trade secrets;</li>
                                            <li>transmitting chain letters or junk email;</li>
                                            <li>engaging in any automated use of the Website or the Website Services.</li>
                                            <li>interfering with, disrupting, or creating an undue burden on the
                                                Website or the Website Services or the networks or services connected
                                                or linked thereto;</li>
                                            <li>attempting to impersonate another user or person;</li>
                                            <li>using the username of another user;</li>
                                            <li>selling or otherwise transferring your profile;</li>
                                            <li>using any information obtained from the Website or the Website Services
                                                in order to harass, abuse, or harm another person;</li>
                                            <li>deciphering, decompiling, disassembling or reverse engineering any of
                                                the software comprising or in any way making up a part of the Website
                                                or the Website Services;</li>
                                            <li>attempting to bypass any measures of the Website or the Website
                                                Services designed to prevent or restrict access to the Website or the
                                                Website Services, or any portion of the Website or the Website
                                                Services;</li>
                                            <li>harassing, annoying, intimidating or threatening any the Website
                                                employees or agents engaged in providing any portion of the Website
                                                Services;</li>
                                            <li>using the Website and/or the Website Services in any manner
                                                inconsistent with any and all applicable laws and regulations.</li>
                                            <li>Using data collected from the website to contact individuals, companies
                                                or other persons or entities.</li>
                                            <li>Supplying false, untrue, expired, incomplete or misleading information
                                                through the Website.</li>
                                        </ul>
                                        <p>You also acknowledge and accept that any violation of the aforementioned
                                            provisions may result in the immediate termination of your access to the
                                            Website and use of our Services, without refund, reimbursement, or any
                                            other credit on our part. Access to the Website may be terminated or
                                            suspended without prior notice or liability of Company. You represent and
                                            warrant to us that you have all right, title, and interest to any and all
                                            content you may post, upload or otherwise disseminate through the Website.
                                            You hereby agree to provide Company with all necessary information,
                                            materials and approval, and render all reasonable assistance and
                                            cooperation necessary for our Services.</p>


                                        <h3>Third party websites</h3>
                                        <p>The Website includes hyperlinks to other websites owned and operated by
                                            third parties; such hyperlinks are not recommendations. Goods and services
                                            of third parties may be advertised and/or made available on or through this
                                            web site. Representations made regarding products and services provided by
                                            third parties are governed by the policies and representations made by
                                            these third parties. The Website shall not be liable for or responsible in
                                            any manner for any of your dealings or interaction with third parties.</p>
                                        <p>The management of the Website has no control over third party websites and
                                            their contents, and subject to the Terms it accepts no responsibility for
                                            them or for any loss or damage that may arise from your use of them.</p>
                                        <p>The Website may contain links from third party websites. External hyperlinks
                                            to or from the site do not constitute the Website’s endorsement of,
                                            affiliation with, or recommendation of any third party or its website,
                                            products, resources or other information. The Website is not responsible
                                            for any software, data or other information available from any third party
                                            website. You are solely responsible for complying with the terms and
                                            conditions for the third party sites. You acknowledge that Company shall
                                            have no liability for any damage or loss arising from your access to any
                                            third party website, software, data or other information.</p>
                                        <p>We do not always review the information, pricing, availability or fitness
                                            for use of such products and services and they will not necessarily be
                                            available or error free or serve your purposes, and any use thereof is at
                                            your sole risk. We do not make any endorsements or warranties, whether
                                            express or implied, regarding any third party websites (or their products
                                            and services). Any linked websites are ruled by their privacy policies,
                                            terms and conditions and legal disclaimers. Please read those documents,
                                            which will rule any interaction thereof.</p>
                                        <p>The Website may provide tools through the Service that enable you to export
                                            information to third party services, including through use of an API or by
                                            linking your account on the Website with an account on the third party
                                            service, such as Twitter or Facebook. By using these tools, you agree that
                                            we may transfer such User Content and information to the applicable third
                                            party service. Such third party services are not under our control, and we
                                            are not responsible for the contents of the third party service or the use
                                            of your User Content or information by the third party service. The
                                            Service, including our websites, may also contain links to third-party
                                            websites. The linked sites are not under our control, and we are not
                                            responsible for the contents of any linked site. We provide these links as
                                            a convenience only, and a link does not imply our endorsement of,
                                            sponsorship of, or affiliation with the linked site. You should make
                                            whatever investigation you feel necessary or appropriate before proceeding
                                            with any transaction with any of these third parties services or websites.</p>


                                        <h3>Third party rights</h3>
                                        <p>A contract under the Terms is for our benefit and your benefit, and is not
                                            intended to benefit or be enforceable by any third party.</p>
                                        <p>The exercise of the parties' rights under a contract under the Terms is not
                                            subject to the consent of any third party.</p>
                                        <p>You agree not to; modify, copy, distribute, transmit, display, perform,
                                            reproduce, publish, license, create derivative works from, transfer,
                                            scrape, gather, market, rent, lease, re-license, reverse engineer, or sell
                                            any information published by other users without the original publishers
                                            written consent.</p>


                                        <h3>Ownership</h3>
                                        <p>The trademarks, copyright, service marks, trade names and other intellectual
                                            and proprietary notices displayed on the Website are the property of – or
                                            otherwise are licensed to – Company or its licensors or affiliates, whether
                                            acknowledged (or not), and which are protected under intellectual and
                                            proprietary rights throughout the world. Respective title holders may or
                                            may not be affiliated with us or our affiliates, partners and advertisers.</p>
                                        <p>No section hereof shall be construed as intent to grant to you any interest
                                            in the Website or our Services, in whole or in part. All content and
                                            materials included as part of the Services, such as images, photographs,
                                            graphics, texts, forms, lists, charts, guidelines, data, logos, code,
                                            icons, videos, audio and other content are the property of, are licensed to
                                            or are otherwise duly available to Company, its affiliates, its licensors
                                            or to the appertaining third party copyrights holder.</p>
                                        <p>You acknowledge and agree that any and all infringing use or exploitation of
                                            copyrighted content in the Website and our Services may cause us, our
                                            affiliates, licensors or content providers irreparable injury, which may
                                            not be remedied solely at law, and therefore our affiliates, licensors or
                                            content providers may seek remedy for breach of these Terms, either in
                                            equity or through injunctive or other equitable relief.</p>


                                        <h3>Term and Termination</h3>
                                        <p>The term hereof shall begin on the date that comes first among: (i) first
                                            access to the Website; (ii) your first access or execution of our Services;
                                            or (iii) Company begins providing its Services to you.</p>
                                        <p>The term hereof will automatically end on the earlier date of either your:
                                            (i) account deactivation, suspension, freezing or deletion; (ii) access
                                            termination or access revocation for our Services or the Website; (iii)
                                            Company’s termination of these Terms or its Services, at its sole and final
                                            discretion; (iv) the termination date indicated by Company to you from time
                                            to time; or (v) Company’ decision to make the Website or Services no longer
                                            available for use, at its sole and final discretion.</p>
                                        <p>Upon expiration of these Terms or termination of your subscription to our
                                            Services, you shall thereafter immediately cease any and all use of our
                                            Services, along with any and all information and data collected therefrom.</p>


                                        <h3>Amendments</h3>
                                        <p>Company hereby reserves the right to update, modify, change, amend,
                                            terminate or discontinue the Website, the Terms and/or the Policy, at any
                                            time and at its sole and final discretion. Company may change the Website’s
                                            functionalities and (any) applicable fees at any time. Any changes to these
                                            Terms will be displayed in the Website, and we may notify you through the
                                            Website or by email. Please, refer to the date shown above for the date
                                            where effective changes were last undertook by us. Your use of our Services
                                            after the effective date of any update– either by an account registration
                                            or simple use – thereby indicates your acceptance thereof.</p>


                                        <h3>No Warranty</h3>
                                        <p>Your use of our Website is at your own risk, and therefore you hereby
                                            acknowledge and agree that our Website and Services are provided "as is",
                                            "with all faults", and "as available", including all content, guides,
                                            checklists, reference guides, sample filing forms, software, materials,
                                            services, functions and/or information made available thereby. It shall be
                                            your own responsibility to ensure that the Services or information
                                            available through this Website meet your specific requirements.</p>
                                        <p>Neither Company, nor its affiliates, subsidiaries, officers, employees and
                                            agents warrantee that the Website will be error-free, uninterrupted,
                                            secure, or produce any particular results; or that any listing, purchase,
                                            order, amount, information, guide, sheet, checklist and/or content will be
                                            current, measured useful and/or valid, or that it will produce any
                                            particular results or that the information obtained therefrom will be
                                            reliable or accurate. No advice or information given by Company or its
                                            employees, affiliates, contractors and/or agents shall create a guarantee.
                                            No warranty or representation is made with regard to such services or
                                            products of third parties contacted on or through the Website. In no event
                                            shall Company or our affiliates be held liable for any such services.</p>
                                        <p>Neither Company, nor its affiliates, licensors, owners, subsidiaries, brands
                                            or advertisers are a professional advisor in any industry. The results
                                            described in the Website are not typical and will vary based on a variety
                                            of factors outside the control of Company. Your use of any information
                                            and/or materials on this Website is entirely at your own risk, for which we
                                            shall not be held liable.</p>


                                        <h3>Disclaimer of Damages</h3>
                                        <p>In no event shall the Company be liable to you or to any third party for any
                                            direct, indirect, incidental, consequential, special, exemplary or punitive
                                            damages, including but not limited to: (i) damages for business
                                            interruption, loss of profits, loss of data, computer or software failure
                                            or inaccessibility or any other type of personal damages or losses arising
                                            out of or related to your use of or inability to use the Website, including
                                            negligence; (ii) infringement of third party intellectual property rights;
                                            and (iii) claims by any party that they are entitled to defense or
                                            indemnification in relation to assertions of rights, demands or claims by
                                            third party rights claimants.</p>
                                        <p>The aforementioned limitation of damage liability, shall be in force
                                            regardless of however caused or however awarded, regardless of the theory
                                            of liability applied (including contract, warranty or tort), whether
                                            active, passive or imputed, including negligence, strict liability, product
                                            liability or other legal theory, regardless of the product or service
                                            offered by action or inaction by merchant; and even if you have been
                                            advised of such possibility.</p>
                                        <p>To the fullest extent allowable under applicable law, Company hereby
                                            expressly disclaims any and all representations and warranties of any kind
                                            with respect to the Website, including any and all liability arising out of
                                            or related to any purported facts or information and description of any
                                            information, products and/or Services displayed on our Website, including
                                            all warranties of any kind, whether express or implied; including, without
                                            limitation, warranties of title, merchantability, accuracy, completeness,
                                            condition, quality, durability, performance, accuracy, reliability,
                                            suitability, fitness for a particular purpose or non-infringement.</p>


                                        <h3>Indemnification</h3>
                                        <p>You agree to indemnify, defend and hold Company and its independent
                                            contractors, affiliates, subsidiaries, officers, employees and agents, and
                                            their respective employees, agents and representatives, harmless from and
                                            against any and all actual or threatened proceedings (at law or in equity),
                                            suits, actions, damages, claims, deficiencies, payments, settlements,
                                            fines, judgments, costs, liabilities, losses and expenses (including, but
                                            not limited to, reasonable expert and attorney fees and disbursements)
                                            arising out of, caused or resulting from: (i) your conduct and any user
                                            content; (ii) your violation of these Terms or the Policy; and (iii) your
                                            violation of the rights of any third-party. </p>
                                        <p>You indemnify the Website and its management for any time that the Website
                                            may be unavailable due to routine maintenance, updates or any other
                                            technical or non-technical reasons. You agree to indemnify the Website and
                                            its management for any error, omission, interruption, deletion, defect,
                                            delay in operation or transmission, communication line failure, theft or
                                            destruction or unauthorized access to your published content, damages from
                                            lost profits, lost data or business interruption.</p>
                                        <p>You hereby indemnify the Website and its management and will not hold them
                                            responsible for copyright theft, reverse engineering and use of your
                                            content by other users on the website.</p>


                                        <h3>Generals</h3>
                                        <p>Advertisements and Promotions. From time to time, we may place ads and
                                            promotions from third party sources in the Website. Accordingly, your
                                            participation or undertakings in promotions of third parties other than
                                            Company, and any terms, conditions, warranties or representations
                                            associated with such undertakings, are solely between you and such third
                                            party. Company is not responsible or liable for any loss or damage of any
                                            sort incurred as the result of any such dealings or as the result of the
                                            presence of third party advertisers on the Website.</p>
                                        <p>No Assignment. You may not assign or transfer these Terms by operation of
                                            law or otherwise without our prior written consent. Notwithstanding the
                                            foregoing, we may assign any rights or obligations hereunder to any current
                                            or future affiliated company and to any successor in interest. Any rights
                                            not expressly granted herein are thereby reserved. These terms will inure
                                            to the benefit of any successors of the parties. We reserve the right, at
                                            any time, to transfer some or all of Company’s assets in connection with a
                                            merger, acquisition, reorganization or sale of assets or in the event of
                                            bankruptcy.</p>
                                        <p>Content Moderation. Company hereby reserves the right, at its sole and final
                                            discretion, to review any and all content delivered into the Website, and
                                            use moderators and/or any monitoring technology to flag and remove any user
                                            generated content or other content deemed inappropriate.</p>
                                        <p>Force Majeure. Company is no liable for any failure of performance on its
                                            obligations as set forth herein, where such failure arises from any cause
                                            beyond Company's reasonable control, including but not limiting to,
                                            electronic, power, mechanic or Internet failure, from acts of nature,
                                            forces or causes beyond our control, including without limitation, Internet
                                            failures, computer, telecommunications or any other equipment failures,
                                            electrical power failures, strikes, labor disputes, riots, insurrections,
                                            civil disturbances, shortages of labor or materials, fires, flood, storms,
                                            explosions, acts of God, war, governmental actions, orders of domestic or
                                            foreign courts or tribunals or non-performance of third parties. </p>
                                        <p>Headings. The titles of paragraphs in these Terms are shown only for ease of
                                            reference and will not affect any interpretation therefrom.</p>
                                        <p>No Waiver. Failure by Company to enforce any rights hereunder shall not be
                                            construed as a waiver of any rights with respect to the subject matter
                                            hereof. </p>
                                        <p>No Relationship. You and Company are independent contractors, and no agency,
                                            partnership, joint venture, employee-employer, or franchiser-franchisee
                                            relationship is intended or created by these Terms.</p>
                                        <p>Notices. All legal notices or demands to or upon Company shall be made in
                                            writing and sent to Company personally, by courier, certified mail, or
                                            facsimile, and shall be delivered to any address the parties may provide.
                                            For communications by e-mail, the date of receipt will be the one in which
                                            confirmation receipt notice is obtained. You agree that all agreements,
                                            notices, demands, disclosures and other communications that Company sends
                                            to you electronically satisfy the legal requirement that such communication
                                            should be in writing.</p>
                                        <p>Severability. If any provision of these Terms is held unenforceable, then
                                            such provision will be modified to reflect the parties' intention. All
                                            remaining provisions of these Terms will remain in full force and effect.
                                            The failure of either party to exercise in any respect any right provided
                                            for herein will not be deemed a waiver of any further rights hereunder.</p>


                                        <h3>Contact</h3>
                                        <p>For any inquires or complaints regarding the Service or Website, please
                                            contact by email at info@loseweightorgetpaid.com.</p>                                        

                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>



         






        {{-- success modal  --}}
                <div class="modal about-modal fade" id="sccs_modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">
                                        <img src="{{ asset('images/logo-65.png') }}">
                                    </h4>
                            </div>
                                <div class="modal-body">
                                    <div class="modal-info">
                                            <div class=" success-modal">
                                                    <div id="success-icon">
                                                        <span class="fas-check">
                                                            <i class="fas fa-check"></i>
                                                        </span>
                                                    </div>
                                                    <h1><strong>Thank You!</strong></h1>
                                                    <p class="points">Purchase Completed</p>
                                                    <hr>
                                                    <?php if (isset(($message))): ?>
                                                        <p class="message">{{ $message }}</p>    
                                                    <?php endif ?>
                                                </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                

         
         

            
                {{-- ///////////////////////////////////////////////// --}}
        
                {{-- //modal --}}



       
        </section>
        <!-- /Parent Section Ended -->

        <!-- jQuery 2.2.0-->
        <script src="{{asset('js/jquery.js')}}"></script>
        {{-- <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script> --}}


        <!-- Bootstrap Core JavaScript -->
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        {{-- <script src="{{asset('vendors/bootstrap/js/bootstrap.min.js')}}"></script> --}}

        <!-- Fancy Box Javacript -->
        <script src="{{asset('js/jquery.fancybox.js')}}"></script>

        <!-- Wow Js -->
        <script src="{{asset('js/wow.min.js')}}"></script>

        <!-- Appear Js-->
        <script src="{{asset('js/jquery.appear.js')}}"></script>

        <!-- Swiper Slider Core JavaScript -->
        <script src="{{asset('js/swiper.min.js')}}"></script>

        <!-- Plyr -->
        {{-- <script src="{{asset('vendors/plyr/js/plyr.polyfilled-3.4.4.min.js')}}"></script> --}}



        <!-- Theme Core Javascript -->
        <script src="{{asset('js/script-theme.js')}}"></script>


        {{-- <script src="{{asset('js/script.js')}}"></script> --}}


        <!-- Custom JavaScript -->
        <script src="{{asset('js/master-app.js?v='.$version)}}"></script>

    </body>

</html>







































































<?php /*



    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}

    <section class="half_section_main" style="background-color: red;">
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </section>








    <!-- Half Section -->
    <section class="half_section_main bg_grey"  id="skill_section">
        <div class="half_section_picture hidden-sm hidden-xs" style="background-image: url({{ asset('images/sections/sc01.jpg') }});"></div>
        <div class="container container_big">
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="side_section_text big_padding">
                        <p class="default_small_heading raleway blue_color font_200">Try it now</p>
                        <h3 class="default_section_heading raleway navy_blue text_left">You are exactly <span class="font_200">where you need to be </span></h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            There are about 108 million people in the United States that are either overweight, obese or suffering from morbid obesity.
                            <br><br>
                            These are the questions you must ask yourself when starting a diet plan. Let's face it, there are many ways to lose weight, but if you're looking for long-term success, then the fast, easy fad diets of pills, shakes, supplements, grapefruit, and pineapple are not what you're looking for.
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        {{-- <i class="fa fa-comment-o pink_color" aria-hidden="true"></i>  --}}
                                        <i class="fas fa-clock pink_color" aria-hidden="true"></i>
                                        Long Term
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        We know you want to lose weight fast, but would you rather have long-term success or short-term success?
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        {{-- <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i>  --}}
                                        <i class="fas fa-thumbs-up pink_color" aria-hidden="true"></i>
                                        Reliable
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        You need a reliable program that will provide results for life.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- half Section Ended -->


    <!-- Circle Js Skill Section -->
    <!-- Skill Heading Section -->
    <section class="combined_skills_section big_padding" >

        <section class="skill_section" >
            <div class="container">
                <div class="skill_section_inner">
                    <p class="default_text raleway text-center default_small_heading blue_color font_200">Where we come in?</p>
                    <h2 class="default_section_heading text-center">
                        LoseWeightOrGetPaid.com
                        <span class="font_200">
                            Platform
                        </span>
                    </h2>
                    <hr class="default_divider default_divider_blue default_divider_big">
                </div>
            </div>
        </section>
        <!-- /Skill Section -->
        <section class="circle_pie_section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="expertise_section_inner">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="expertise_block text-center margin_after_tab wow slideInLeft">
                                        <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span class="font_600 pink_color">01.</span> Web Design</h3>
                                        <p class="default_text text-center open_sans default_text_light">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="expertise_block text-center margin_after_tab wow fadeInUp">
                                        <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span class="font_600 pink_color">02.</span> Photography</h3>
                                        <p class="default_text text-center open_sans default_text_light">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dosit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="expertise_block text-center wow slideInRight">
                                        <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span class="font_600 pink_color">03.</span> Audio Creation</h3>
                                        <p class="default_text text-center open_sans default_text_light">
                                            Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!-- /Circel Js Skill Section -->




    <!-- Stats Section -->
    <section class="stats_section big_padding bg_grey">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-check white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 2000 </h3>
                        <p class="small_heading_navy white_color open_sans font_200">Completed Projects</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-flag white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 125 </h3>
                        <p class="small_heading_navy white_color open_sans font_200">Total Countries</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-globe white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 5000</h3>
                        <p class="small_heading_navy white_color open_sans font_200">Users Worldwide</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-gift white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 9 </h3>
                        <p class="small_heading_navy white_color open_sans font_200">Award Winners</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Stats Section -->


    <!-- Portfolio Section -->
    <section class="portfolio_section big_padding" style="padding-bottom: 0;" id="work">
        <div class="container">
            <p class="default_small_heading raleway text-center blue_color font_200">Creative Portfolio</p>
            <h2 class="default_section_heading text-center">
                <span class="font_200"> Our </span>
                Awesome
                <span class="font_200">Work</span>
            </h2>
            <hr class="default_divider default_divider_blue default_divider_big">
        </div>
        <div  class="cube_fullwidth_style_portfolio">
            <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter open_sans">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item open_sans button bg_before_pink">
                    All
                    <div class="cbp-filter-counter"></div>
                </div>
                <div data-filter=".branding" class="cbp-filter-item open_sans button bg_before_pink">
                    Branding
                    <div class="cbp-filter-counter"></div>
                </div>
                <div data-filter=".web-design" class="cbp-filter-item open_sans button bg_before_pink">
                    Web Design
                    <div class="cbp-filter-counter"></div>
                </div>
                <div data-filter=".photography" class="cbp-filter-item open_sans button bg_before_pink">
                    Photography
                    <div class="cbp-filter-counter"></div>
                </div>
            </div>
            <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">
                <div class="cbp-item web-design photography">
                    <a href="images/main/projects-1.jpg" class="cbp-caption cbp-lightbox" data-title="Bolt UI<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/main/projects-1.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">Bolt UI</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item branding photography">
                    <a href="images/main/projects-2.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock<br>by Paul Flavius Nechita">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/main/projects-2.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">World Clock</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item branding photography">
                    <a href="images/main/projects-3.jpg" class="cbp-caption cbp-lightbox" data-title="WhereTO App<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/main/projects-3.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">WhereTO App</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item branding graphic">
                    <a href="images/main/projects-4.jpg" class="cbp-caption cbp-lightbox" data-title="Digital Menu<br>by Cosmin Capitanu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/main/projects-4.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">Digital Menu</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item web-design branding">
                    <a href="images/main/projects-6.jpg" class="cbp-caption cbp-lightbox" data-title="iDevices<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/main/projects-6.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">iDevices</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="cbp-item photography">
                    <a href="images/main/projects-5.jpg" class="cbp-caption cbp-lightbox" data-title="Seemple* Music<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/main/projects-5.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">Seemple* Music</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- /Portfolio Section -->


    <section class="team_section big_padding text-center " id="team">
        <div class="container">
            <p class="raleway default_small_heading blue_color font_200">Let's Meet</p>
            <div class="col-xs-12">
                <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                    Creative
                    <span class="font_200">
                                Team
                            </span>
                </h2>
                <hr class="default_divider default_divider_blue default_divider_big">
                <div class="swiper-container team_slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="team_detail wow slideInLeft">
                                <div class="team_detail_inner">
                                    <img src="images/main/team-1.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Chris Martin</h3>
                                <p class="default_text text-center open_sans">CEO - Company</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_detail wow fadeInUpBig">
                                <div class="team_detail_inner">
                                    <img src="images/main/team-2.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Ellyse Perry</h3>
                                <p class="default_text text-center open_sans">Java Developer</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_detail wow fadeInDownBig">
                                <div class="team_detail_inner">
                                    <img src="images/main/team-3.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Danny M</h3>
                                <p class="default_text text-center open_sans">Web Designer</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_detail wow slideInRight">
                                <div class="team_detail_inner">
                                    <img src="images/main/team-4.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Sarah Taylor</h3>
                                <p class="default_text text-center open_sans">Graphic Designer</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- Half Section -->
    <section class="half_section_main half_section_right bg_grey">
        <div class="half_section_picture hidden-sm hidden-xs"></div>
        <div class="container container_big">
            <div class="row">
                <div class="col-md-6">
                    <div class="side_section_text big_padding">
                        <p class="default_small_heading raleway blue_color font_200">Professional Expertise</p>
                        <h3 class="default_section_heading raleway navy_blue"><span class="font_200"> Our </span> Awesome <span class="font_200"> Skills </span></h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis.
                        </p>
                        <div class="progress_bar_outer_div">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy raleway default_small_heading navy_blue h3_pink font_400">Writing</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 84%;" role="progressbar" aria-valuenow="84" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">84%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy default_small_heading navy_blue raleway font_400">Development</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 98%;" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">98%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy default_small_heading navy_blue raleway font_400">Design</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 63%;" role="progressbar" aria-valuenow="63" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">63%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy default_small_heading navy_blue raleway font_400">Video Editing</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 78%;" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">78%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- half Section Ended -->




    <!-- Pricing Table -->
    <section class="pricing_section big_padding bg_grey" id="pricing_table">
        <div class="container">
            <div class="pricing_table_section">
                <p class="default_text raleway text-center default_small_heading blue_color font_200">New Plans</p>
                <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                    Pricing
                    <span class="font_200">
                                Table
                            </span>
                </h2>
                <hr class="default_divider default_divider_blue default_divider_big">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="pricing_table_column wow slideInLeft">
                            <h3 class="small_heading_navy raleway">Basic</h3>
                            <div class="price">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2 class="open_sans font_400 pink_color"><span class="dollar raleway">$</span>19<br><span class="month raleway">Month</span></h2>
                                    </div>
                                    <div class="col-xs-8">
                                        <p>It has survived not only five centuries, but also the leap into electronic.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="packages">
                                <li><i class="fa fa-check" aria-hidden="true"></i>Full access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Bandwidth</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Powerful Admin Panel</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Email Accounts</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>8 Free Forks Every Months</li>
                            </ul>
                            <div class="pricing_button">
                                <a href="#." class="bg_pink button button_default_style open_sans bg_before_navy">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pricing_table_column blue_price wow fadeInUpBig">
                            <h3 class="small_heading_navy raleway white_color">Advance</h3>
                            <div class="price">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2 class="open_sans font_400 white_color"><span class="dollar raleway">$</span>49<br><span class="month raleway">Month</span></h2>
                                    </div>
                                    <div class="col-xs-8">
                                        <p>It has survived not only five centuries, but also the leap into electronic.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="packages">
                                <li><i class="fa fa-check" aria-hidden="true"></i>Full access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Bandwidth</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Powerful Admin Panel</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Email Accounts</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>8 Free Forks Every Months</li>
                            </ul>
                            <div class="pricing_button">
                                <a href="#." class="bg_white button button_default_style open_sans bg_before_pink navy_blue">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pricing_table_column wow slideInRight">
                            <h3 class="small_heading_navy raleway">Premium</h3>
                            <div class="price">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2 class="open_sans font_400 pink_color"><span class="dollar raleway">$</span>99<br><span class="month raleway">Month</span></h2>
                                    </div>
                                    <div class="col-xs-8">
                                        <p>It has survived not only five centuries, but also the leap into electronic.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="packages">
                                <li><i class="fa fa-check" aria-hidden="true"></i>Full access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Bandwidth</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Powerful Admin Panel</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Email Accounts</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>8 Free Forks Every Months</li>
                            </ul>
                            <div class="pricing_button">
                                <a href="#." class="bg_pink button button_default_style open_sans bg_before_navy">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Pricing Table -->



    <!-- Customer Review Slider -->
    <section class="customer_feedback_section big_padding text-center bg_theme">
        <div class="container">
            <div class="feedaback_inner">
                <img src="images/main/cutomer_quote.png" alt="cutomer_quote" class="quote_pic">
                <h2 class="default_section_heading text-center white_color">
                            <span class="font_200">
                                What
                            </span>
                    People
                    <span class="font_200">
                                Say ?
                            </span>
                </h2>
                <hr class="default_divider default_divider_white default_divider_big">
                <div class="swiper-container customer_feedback_slider white_pagination">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <p class="customer_feedback_text white_color open_sans default_text font_200">

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere
                                convallis.
                            </p>
                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">John Doe - Google CEO</p>
                        </div>
                        <div class="swiper-slide">
                            <p class="customer_feedback_text white_color open_sans default_text font_200">

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere
                                convallis.
                            </p>
                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">John Doe - Google CEO</p>
                        </div>
                        <div class="swiper-slide">
                            <p class="customer_feedback_text white_color open_sans default_text font_200">

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere
                                convallis.
                            </p>
                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">John Doe - Google CEO</p>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next fa fa-angle-right hidden-sm hidden-xs"></div>
                    <div class="swiper-button-prev fa fa-angle-left hidden-sm hidden-xs"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Customer Review Slider -->





    <!-- Blog Section -->
    <section class="blog_section big_padding" id="blog">
        <div class="container">
            <p class="default_text raleway text-center default_small_heading blue_color font_200">Read the News</p>
            <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                Latest
                <span class="font_200">
                                Blogs
                            </span>
            </h2>
            <hr class="default_divider default_divider_blue default_divider_big">
            <div class="blog_slider_main swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="blog_slide wow slideInLeft">
                            <a href="blog.html" class="anchor_style_default">
                                <h3 class="small_heading_navy raleway center_on_mobile font_400">Pellentesque euismod</h3>
                            </a>
                            <p class="default_text default_text_light open_sans center_on_mobile">June 18, 2017</p>
                            <div class="blog_img">
                                <img src="images/main/blog_post_1.jpg" alt="blog_img">
                            </div>
                            <p class="default_text open_sans center_on_mobile">
                                Aenean faucibus nulla ex, in rutrum dui blandit at. Pellentesque bibendum odio vitae gravida consequat aliquam sem morbi dor sit imet sen
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="blog_slide wow fadeInDownBig">
                            <a href="blog.html" class="anchor_style_default">
                                <h3 class="small_heading_navy raleway center_on_mobile font_400">Proin condimentum</h3>
                            </a>
                            <p class="default_text default_text_light open_sans center_on_mobile">June 18, 2017</p>
                            <div class="blog_img">
                                <img src="images/main/blog_post_2.jpg" alt="blog_img">
                            </div>
                            <p class="default_text open_sans center_on_mobile">
                                Cras eget ligula dolor aenean ac velit ultricies, i pretium tincidunt bibendum turpis maximus posuere get
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="blog_slide wow slideInRight">
                            <a href="blog.html" class="anchor_style_default">
                                <h3 class="small_heading_navy raleway center_on_mobile font_400">Donec dapibus</h3>
                            </a>
                            <p class="default_text default_text_light open_sans center_on_mobile">June 18, 2017</p>
                            <div class="blog_img">
                                <img src="images/main/blog_post_3.jpg" alt="blog_img">
                            </div>
                            <p class="default_text open_sans center_on_mobile">
                                Cras eget ligula dolor aenean ac velit ultricies, accumsan nisl cursus, aliquam sem morbi pretium tincidunt bibendum turpis maximus posuere get
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
    <!-- /Blog Section -->



    <!-- Google Map Section -->
    {{-- <section id="map"></section> --}}
    <!-- /Google Map Section -->





    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
    {{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}






*/ ?>