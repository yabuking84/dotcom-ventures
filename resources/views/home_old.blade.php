<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Insert Favicon Here -->
    <link rel="icon" type="image/png" href="images/logos/logo-icon-sm.png?v=1.0"/>
    <!-- Page Title(Name)-->
    <title>Dotcom Ventures FZE</title>
    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Font-Awesome CSS File -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- Slider Revolution CSS File -->
    <link rel="stylesheet" href="css/settings.css">
    <!--  Fancy Box CSS File -->
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <!-- Circleful CSS File -->
    <link rel="stylesheet" href="css/jquery.circliful.css">
    <!-- Animate CSS File -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Cube Portfolio CSS File -->
    <link rel="stylesheet" href="css/cubeportfolio.min.css">
    <!-- Owl Carousel CSS File -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- Swiper CSS File -->
    <link rel="stylesheet" href="css/swiper.min.css">
    <!-- Custom Style CSS File -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Color StyleSheet CSS File -->
    <link href="css/blue.css" rel="stylesheet" id="color" type="text/css">

    
    <link rel="stylesheet" href="css/master-loader.css?v={{ env('APP_ASSET_VER','1.1') }}">
    <link rel="stylesheet" href="css/master-app.css?v={{ env('APP_ASSET_VER','1.1') }}">


    <meta property="og:type" content="article">
    <meta property="og:url" content="https://dotcomv.com">
    <meta property="og:image" content="{{ asset('images/logos/logo-icon.png') }}">
    <meta property="og:title" content="Dotcom Ventures FZE">
    <meta property="og:description" content="WE DREAM IT. WE BUILD IT.">

    <meta name="description" content="WE DREAM IT. WE BUILD IT.">    
    <meta name="author" content="Dotcom Ventures FZE">    

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <style type="text/css">
        
        .grecaptcha-badge {
            margin: auto;
            margin-top: 15px;
        }

    </style>

</head>

<script type="text/javascript">
    @if (session('error') !== null)
       var success = 'true';
    @else
        var success = 'false';
    @endif
</script>

    
<body data-spy="scroll" data-target=".navbar" data-offset="83" id="body" class="particles_special_id">

<!-- Loader -->
<div class="loader">
    <div class="loader-inner">
        {{-- <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div> --}}


        <div class="wrapper">
          <div class="knight-loader">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>             
    </div>
</div>
<!-- Parent Section -->
<section class="page_content_parent_section">

    <!-- Header Section -->
    <header>
        <!-- Navbar Section -->
        <nav class="navbar navbar-fixed-top ">



                    {{-- cookie warning --}}
                    {{-- //////////////////////////////////////////////////////////////////// --}}
                    <div class="cookie_warning">
                        <p>
                            To continue, you must agree to the <b>www.dotcomv.com<span class="symbol">&copy;</span></b>
                            <a data-target="#tc_modal" data-toggle="modal" class="pp_tc_link">Terms and Conditions</a>
                            and <a data-target="#pp_modal" data-toggle="modal" class="pp_tc_link">Privacy Policy</a>.
                            This Website uses cookies in order to offer you the most relevant information.
                            Please read our Cookies Policy for more information in the Privacy Policy <a data-target="#pp_modal"
                                data-toggle="modal" data-anchor="#cookies" class="pp_tc_link scroll_toanchor">Here</a>.
                            If you want to proceed to the Website, please accept the Website policies by clicking on
                            "Yes, I accept" button below.
                            If you do not agree, then you may leave by closing this webpage.
                        </p>
                        <button class="btn btn-default i_accept">Yes, I accept</button>
                    </div>
                    {{-- //////////////////////////////////////////////////////////////////// --}}
                    {{-- cookie warning --}}



            <div class="container-fluid">
                <!--second nav button -->
                <div id="menu_bars" class="right menu_bars">
                    <span class="t1"></span>
                    <span class="t2"></span>
                    <span class="t3"></span>
                </div>
                <!-- Brand and toggle get grouped for better mobile display -->

                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{{ asset('') }}"><img src="{{ asset('images/logos/logo-sm.png') }}" alt="logo"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse  ">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#home" class="scroll">Home</a></li>
                            <li><a href="#about_us" class="scroll">About Us</a></li>
                            <li><a href="#mission_vision" class="scroll">Mission Vision</a></li>
                            <li><a href="#our_team" class="scroll">Our Team</a></li>
                            <li><a href="#products_services" class="scroll">Products & Services</a></li>
                            <li><a href="#present_products" class="scroll">Projects</a></li>
                            
                            <li><a href="#contact-form" class="scroll">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <div class="sidebar_menu">
                    <nav class="pushmenu pushmenu-right">
                        <a class="push-logo" href="#"><img src="images/logos/logo-sm.png" alt="logo"></a>
                        <ul class="push_nav centered">
                            <li class="clearfix"><a href="#home" class="scroll"><span>01.</span>Home</a></li>
                            <li class="clearfix"><a href="#about_us" class="scroll"> <span>02.</span>About Us</a></li>
                            <li class="clearfix"><a href="#mission_vision" class="scroll"> <span>03.</span>Mission Vision</a></li>
                            <li class="clearfix"><a href="#our_team" class="scroll"> <span>04.</span>Our Team</a></li>
                            <li class="clearfix"><a href="#products_services" class="scroll"> <span>05.</span>Products & Services</a></li>
                            <li class="clearfix"><a href="#present_products" class="scroll"><span>06.</span>Projects</a></li>

                            <li class="clearfix"><a href="#contact-form" class="scroll"> <span>07.</span>Contact Us</a></li>
                        </ul>
                        <div class="clearfix"></div>
                        {{-- <ul class="social_icon black top25 bottom20 list-inline">
                            <li><a href="#" class="navy_blue facebook"><i class="fa fa-fw fa-facebook"></i></a></li>
                            <li><a href="#" class="navy_blue twitter"><i class="fa fa-fw fa-twitter"></i></a></li>
                            <li><a href="#" class="navy_blue pinterest"><i class="fa fa-fw fa fa-pinterest"></i></a></li>
                            <li><a href="#" class="navy_blue linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul> --}}
                    </nav>
                </div>
            </div>
        </nav>
        <!-- /Navbar Section -->








        <!-- Main Slider Section -->
        <div class="page_content_main_slider" id="home">
            <div class="swiper-container main_slider parallax-window"  
                data-speed="-0.1" 
                data-parallax="scroll" 
                data-image-src="images/wallpapers/bg01.jpg">

                <div class="gradient-overlay-slider"></div>

                <canvas id="home_particles_bg" class="particles_bg" style="position: absolute"></canvas>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="container">
                            <h1 class="raleway white_color swiper-slider-heading main_title">Dotcom <span class="font_400">Ventures</span></h1>
                            <br>
                            <h3 class="raleway white_color swiper-slider-small-heading main_slogan">WE DREAM IT. WE BUILD IT.</h3>
                            {{-- <div class="button_div">
                                <a href="#." class="button button_default_style bordered_button open_sans   bg_before_white btn2">Learn More</a>
                            </div> --}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- /Main Slider Section -->




    </header>
    <!-- /Header Section -->





























































    {{-- <section class="half_section_main about_us "  id="about_us" style=""> --}}
    {{-- <section class="half_section_main about_us "  id="about_us" style="background-image: url({{ asset('images/home/img33.jpg') }});"> --}}
    <section class="half_section_main about_us "  id="about_us" style="background-image: url({{ asset('images/home/graphics/grphcs10.png') }});">
        <div class="half_section_picture wow slideInRight" style="background-image: url({{ asset('images/home/graphics/grphcs07.png') }});"></div>
        <div class="container container_big">
            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="side_section_text big_padding" style="padding-bottom: 0;">
                        <h3 class="default_section_heading raleway navy_blue text-center">About <span class="font_200"> Us </span></h3>
                        <hr class="default_divider default_divider_blue " style="">
                    </div>
                </div>
            </div> --}}

            <div class="row display_flex">
                
                {{-- <div class="col-md-6 valign_center">
                    <div class="image" style="background-image: url({{ asset('images/home/img01.jpg') }});"></div>
                </div> --}}

                <div class="col-md-6">
                    <div class="side_section_text xs_padding">
                        <p class="default_small_heading raleway blue_color font_200">Dotcom Ventures FZE</p>
                        <h3 class="default_section_heading raleway navy_blue">In Touch <span class="font_200">With Tomorrow</span></h3>
                        <hr class="default_divider default_divider_medium default_divider_blue " style="margin-left: 0;">

                        <p class="default_text_light default_text open_sans">
                            The advent and the proliferation of the digital economy have opened a new frontier in the technology industry. This concept has led to the popularity of virtual products and services that has a high potential for viral growth. Typical examples are the startups (Google, Amazon, Facebook, Netflix a.o.) that became global companies in just a couple of years. This sector is still in development and has a massive potential for creating more success stories.                             
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        {{-- <i class="fa fa-comment-o pink_color" aria-hidden="true"></i>  --}}
                                        {{-- <i class="fas fa-exclamation "></i> --}}
                                        <i class="fas fa-exclamation-triangle"></i>
                                        Critical Role
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        The digital economy plays a critical role locally and on a global scale; it fosters innovation, supports industries and creates employment.                                        
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        {{-- <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i>  --}}
                                        <i class="fas fa-lightbulb"></i>
                                        Innovation
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        However, due to the increase in competition in this sector, there is a need for extensive market research and elements of innovation to have an edge over competition. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>














































    <section class="half_section_main about_us parallax_bg"  id="about_us_1" style="background-image: url({{ asset('images/home/img11.jpg') }});">
        <div class="backdrop dark_bg_backdrop"></div>
        <div class="container container_big">
            <div class="row">
                <div class="col-md-12">
                    <div class="side_section_text big_padding">
                        <p class="default_small_heading raleway blue_color font_300"><strong>About</strong> Us</p>
                        {{-- <h3 class="default_section_heading raleway navy_blue"><span class="font_200">Dotcom</span> Venture FZE</h3> --}}
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            We are a technology company that focuses on developing online platforms, digital products and services across various sectors to provide value for individuals and businesses. Incorporated in the United Arab Emirates in 2018, DV leverages more then a decade experience of the company’s stakeholders in the development of digital platforms and products to form a consortium of online businesses.
                            <br><br>
                            DV believes that the success in online entrepreneurship is driven by the discovery of challenges in various niches and proffering hard to find solutions to the pain points. However, the procedure of discovering the viable market places a demand on data-driven analytics relating the Internet big data which reveals critical patterns and trends that indicate the viability of an online business prospect. This approach helps to channel efforts in the development of products and services for a hungry market rather than depending on instincts to develop products for a non-existing market.
                            <br><br>
                            Besides data-driven market research, DV has the competence, commitment and technical capacity to transform a business idea into reality by harnessing well-established and emerging technologies to deploy solutions. The team of expert programmers and frontend designers of Dotcom Ventures have extensive experience in developing Information Products, Mobile Apps, and Web Applications using various platforms. The goal of the company is to identify hungry markets and create in-demand services that provide immense value. This approach engenders loyalty and goodwill from the users which eventually leads to natural growth. 
                            <br><br>
                            DV believes that progression is driven by curiosity and is unafraid to venture into new sectors to embrace emerging technologies. Blockchain technology and the entire cryptocurrency ecosphere is proving to be a viable technology that proffers solutions which transcends the financial industry. DV looks forward to developing blockchain-derived products among other digital products and services.                            
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>




    









    <section class="big_padding dark_bg" id="value_proposition" style="background-image: url({{ asset('images/home/img32.jpg') }});">
        {{-- <div class="particles_bg" style="position: absolute"></div> --}}

        <div class="container container_big">

            <div class="row" style="display: flex; flex-wrap: wrap;">
                <div class="col-md-5" style="display: flex; align-items: center; flex-wrap: wrap;">
                    <div class="side_section_text ">
                        <h2 class="default_section_heading navy_blue">
                            Value <span class="font_200">Proposition</span>
                        </h2>
                        <hr class="default_divider default_divider_blue" style="margin-left: 0;">
                        
                        <p class="default_text_light default_text open_sans text_align_justify" style="">
                            Dotcom Ventures has established value propositions that serve as the corporate philosophy of the company, fostering the internal operations, product and service development, as well as present and future corporate alliances.
                        </p>
                    </div>
                    <div class="row">
                        <?php
                            $vps = [
                                   ['title'=>'Research & Development',       'icon'=>'<i class="fas fa-search"></i>',        'class'=>'', ],
                                   ['title'=>'Partnerships',                 'icon'=>'<i class="fas fa-handshake"></i>',     'class'=>'', ],
                                   ['title'=>'Technology',                   'icon'=>'<i class="fas fa-atom"></i>',          'class'=>'', ],
                                   ['title'=>'Integrity & Professionalism',  'icon'=>'<i class="fas fa-university"></i>',    'class'=>'', ],
                                   ['title'=>'Quality Service Delivery',     'icon'=>'<i class="fas fa-hand-holding"></i>',  'class'=>'', ],
                                   ['title'=>'Creativity & Innovation',      'icon'=>'<i class="fas fa-lightbulb"></i>',     'class'=>'', ],
                            ];
                        ?>

                        @foreach($vps as $vp)
                        <div class="col-sm-12">
                            <div class="expertise_block text-center  wow slideInLeft"
                                style="visibility: visible; animation-name: slideInLeft;">
                                <?= $vp['icon'] ?>
                                <h3 class="small_heading_navy text-center raleway  font_600">
                                    {{ $vp['title'] }}                            
                                </h3>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="col-md-7">
                    <img class="vp_img wow slideInRight" src="{{ asset('images/home/graphics/grphcs11.png') }}" alt="">
                </div>
            </div>


        </div>
    </section>














    {{-- <section class="combined_skills_section big_padding has_bg_image parallax_bg parallax-window"  
    data-speed="-0.2" 
    data-parallax="scroll" 
    data-image-src="images/home/img22.jpg" 
    id="mission_vision" > --}}



    <section class="combined_skills_section big_padding has_bg_image parallax_bg parallax-window" id="mission_vision" >
        {{-- <div class="backdrop dark_bg_backdrop"></div> --}}
        <section class="skill_section" >
            <div class="container">
                <div class="skill_section_inner">
                    {{-- <p class="default_text raleway text-center default_small_heading blue_color font_200">What we do</p> --}}
                    <h2 class="default_section_heading text-center">
                        Mission 
                        <span class="font_200">
                        Vision
                        </span>
                    </h2>
                    <hr class="default_divider default_divider_big">
                </div>
            </div>
        </section>

        <!-- /Skill Section -->
        <section class="circle_pie_section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="expertise_section_inner">
                            <div class="row flex_space_evenly">
                                <div class="col-md-5 mission">
                                    <div class="expertise_block text-center margin_after_tab wow slideInLeft">
                                        <h3 class="small_heading_navy text-center raleway font_400">
                                            <span class="font_600 ">Our</span>
                                            Mission
                                        </h3>
                                        <p class="default_text text_align_justify open_sans default_text_light">
                                            To research, develop, and maintain highly efficient digital products and services that solve critical challenges across industry verticals.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-5 vision">
                                    <div class="expertise_block text-center margin_after_tab wow slideInRight">
                                        <h3 class="small_heading_navy text-center raleway font_400">
                                            <span class="font_600 ">Our</span>
                                            Vision
                                        </h3>
                                        <p class="default_text text_align_justify open_sans default_text_light">
                                            To be a key player in the evolution of next-generation technology and internet services that enhances personal lives and businesses around the world.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>















{{-- data-image-src="{{ asset('images/home/img21.jpg') }}"   --}}

{{-- <section class="half_section_main parallax-window"  
data-speed="-0.2"   
data-parallax="scroll" 
data-image-src="{{ asset('images/home/img22.jpg') }}"  
id="unique_selling_points"> --}}

    <section 
    class="half_section_main parallax-window" 
    id="unique_selling_points">

        {{-- <div class="backdrop dark_bg_backdrop"></div> --}}
        
        <div class="curved_bg" style="background-image: url({{ asset('images/home/curved-white-bg.png') }});"></div>

        <div class="container container_big">
            <div class="row">
                <div class="col-md-12">
                    <div class="side_section_text big_padding">
                        {{-- <p class="default_small_heading raleway blue_color font_200">About Us</p> --}}
                        <h3 class="default_section_heading raleway">Unique <span class="font_200">Selling Points</span></h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans main_text">
                            The Unique selling points of DV are factors that give the company an edge in the competitive business landscape of the knowledge-driven economy.                             
                        </p>
                        <div class="row usps">

                            <?php
                                $usps = [
                                    ['title'=>'Experience',       'animate'=>'slideInLeft',  'content'=>'The stakeholders of DV have extensive experience in creating online businesses and brings valuable expertise and existing products that allows the company to compete favourably. '],
                                    ['title'=>'Versatility',      'animate'=>'slideInDown',  'content'=>'DV has a team of developers and front-end designers who are versed in various technologies that give the company an array of options to deploy the products and services.'],
                                    ['title'=>'Quality',          'animate'=>'slideInRight',  'content'=>'The core of the company’s objectives is to develop in-demand quality products and services that provide value to the users thereby engendering natural growth.'],
                                    ['title'=>'Innovation',       'animate'=>'slideInLeft',  'content'=>'Dotcom Venture believes in searching for new opportunities, and shall explore new frontiers with the goal of establishing premier services in untapped niches.'],
                                    ['title'=>'Cost-Efficiency',  'animate'=>'slideInUp',  'content'=>'Equipped with extensive experience derived from establishing parallel online projects, DV can handle new projects and save costs without compromising on quality delivery.'],
                                    ['title'=>'Healthy Capital',  'animate'=>'slideInRight',  'content'=>'DV shall seek fresh capital from investors to attract and retain topnotch human capital, and maintain premium server infrastructure which is critical to online services.'],
                                ];
                            ?>

                            @foreach($usps as $usp)
                            <div class="col-md-4">
                            <div class="usp wow {{ $usp['animate'] }}">
                                <div class="icons_div_small">                                    
                                    
                                    <div class="title">                                        
                                        <div class="hexagon">                                        
                                            <i class="fas fa-check"></i>                                    
                                        </div>
                                        <p class="small_heading_navy raleway font_400"> 
                                            {{ $usp['title'] }}
                                        </p>
                                    </div>

                                    <p class="default_text default_text_light open_sans">
                                        {{ $usp['content'] }}
                                    </p>

                                </div>
                            </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
































    <section class="half_section_main "  id="ceo" style="background-image: url({{ asset('images/home/img31.jpg') }});">
        <div class="container container_big">
            <div class="row">
                <div class="col-md-6">
                    <div class="ceo_img" style="background-image: url({{ asset('images/home/johannes-eidens.png') }});"></div>
                </div>
                <div class="col-md-6">
                    <div class="side_section_text big_padding">
                        {{-- <p class="default_small_heading raleway blue_color font_200">Dotcom Ventures FZE</p> --}}
                        <h3 class="default_section_heading raleway navy_blue">
                            Johannes Nepomuk Eidens <br>
                            <span class="font_200" style="font-size: 70%;">Chief Executive Officer</span>
                        </h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            Johannes Nepomuk Eidens has academic experience in International Business and Management and has since worked at various supervisory and management position with several international organisations. Johannes’s innate entrepreneurship talent led him to start many successful online and offline companies. He is an astute business manager with extensive expertise in identifying business opportunities, securing business finance, implementation of successful business models and marketing strategies and risks evaluation.
                            <br><br>                            
                            Johannes pioneered innovative digital services such as eReef an on-demand multimedia service that existed before Netflix, CamelLED, DubaiConsult, ZoomDNA and a host of other successful online businesses. 
                            Almani.ae is a project developed by Johannes which synergises online-offline marketing strategies within the LED Lighting industry. Dotcom Ventures is his current project in which he seeks to leverage over a decade of experience in creating online ventures to build successful online brands.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>




















    <section class="half_section_main dark_bg" id="our_team"  style="background-image: url({{ asset('images/home/img34.jpg') }});">
        <div class="container container_big">
            <div class="row">
                <div class="col-md-8">
                    <div class=" big_padding text_content">
                        <p class="default_small_heading raleway blue_color font_200">Dotcom Ventures FZE</p>
                        <h3 class="default_section_heading raleway navy_blue">
                            Our Team
                        </h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            The functions of Dotcom Ventures is driven by a dedicated team of young, vibrant and committed specialists who are experts in the areas of their core competence. The personnel are professionals and highly trained in different aspects of Information Technology, product research and development, internet marketing and customer service. The teams of the company work together to create a synergy that identifies online opportunities and develops novel solutions that cater to the needs of various niches and attract highly-targeted leads to the businesses. 
                            <br><br>
                            Dotcom Ventures has active management led by Johannes Nepomuk Eidens, a serial entrepreneur and business manager with extensive experience in managing online ventures and offline businesses. 
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>





















    <style>

    #products_services {
        
    }
    
    </style>


    {{-- <section class="half_section_main parallax-window"  
    data-speed="0.2"   
    data-parallax="scroll" 
    data-image-src="{{ asset('images/home/img26.png') }}"  
    style="" 
    id="products_services"> --}}

    <section 
    class="half_section_main parallax-window" 
    id="products_services" 
    style="">

        <div class="container container_big">
            <div class="row">
                <div class="col-md-12">
                    <div class="side_section_text big_padding">
                        <h3 class="default_section_heading raleway"><span class="font_200">Products</span> and Services</h3>
                        <hr class="default_divider default_divider_blue big_padding" style="margin-left: 0;">
                        
                        <div class="usps">

                            <?php
                                $usps = [

                                    ['title'=>'Big Data', 								'img-class'=>'move_left_reverse',	'animate'=>'slideInRight',   'image'=>'grphcs12.png',   'order'=>'reverse',        'content'=>'Post-millennial businesses are inundated with a lot of data that encumbers businesses which is commonly referred to as Big Data. Many corporations see these data as a burden which they are compelled to manage to provide their clients service. However, the discerning corporations knows that harnessing the power of the Big Data can provide insight into the business processes that can give a corporation an edge over completions by harnessing raw data to support better business decisions. <br><br> The Big Data solution is an integral aspect of the Dotcom Venture product development and service delivery strategy.  DV has the expertise, skills and technical competence to develop platforms that provide end-to-end services for the discovery, computing, deploying and administering Big Data solution for the benefit of businesses and corporate clients. As more business become aware of the potential benefit the Big Data provides, this emerging service industry shall evolve into an in-demand service for businesses and corporations. <br><br>This system places a demand on the technology that integrates IT infrastructure for accessing the Big Data, The management of the data (data security and processing), and advanced data science, engineering, analytics and presentation. The end result is business intelligent information valuable for enhanced organizational efficiency and profitability.',  ],

                                    ['title'=>'Artificial Intelligence', 				'img-class'=>'',					'animate'=>'slideInLeft',   'image'=>'grphcs05.png',   'order'=>'',        'content'=>'As a modern business, Artificial Intelligence (AI) applications forms a vital aspect of our product development strategy. These are self-absorbing programs that actively interacts with data transactions in its environment and learns from its peculiar patterns.  This type of system presents endless opportunities allowing the user to leverage data-driven information to make informed business and technical decisions. Besides the conversion of raw data to logical information, the systems are configurable to make intelligent choices and can trigger actions based on pre-defined logical sequence. This feature opens a new frontier for the development of automation application for various industries. <br><br>The relevance of AI technology is applicable across all walks of life; both at home and business. It forms the core of emerging technologies such as the Internet of Things (IoT). Basis the broad application of this technology and the endless opportunities it presents, the Dotcom Ventures AI application shall be beneficial in delivering critical solutions to various industries and derive lucrative returns for the stakeholders of Dotcom Ventures.',  ],

                                    ['title'=>'Cryptocurrency & Blockchain',          	'img-class'=>'move_left',			'animate'=>'slideInRight',  'image'=>'grphcs01.png',   'order'=>'reverse', 'content'=>'When Bitcoin emerged with the blockchain open, distributed ledger system which forms the basis of the cryptocurrency in 2010, the future seems precarious. However, in less than a decade, the adoption increased exponentially, and its potential in solving challenges across industry verticals became more apparent by the day. This technology aligns with Dotcom Venture’s vision to leverage technology to provide value. Therefore, Dotcom Ventures shall develop blockchain-derived products as a part of the company’s core product/service development strategy. The radical adoption of this technology is an indication of the future trend. As of the time of writing, the market capitalisation of the cryptocurrency industry stands at $211billion. This value is impressive considering that the industry is less than a decade old.',  ],
                                    
                                    ['title'=>'Mobile Apps',             				'img-class'=>'move_right',			'animate'=>'slideInLeft',  'image'=>'grphcs03.png',   'order'=>'', 'content'=>'The mobile apps industry has taken the world of computing by storm. Mobile devices have evolved from pure voice and data communicating tools to powerful computing devices. Data from internet sources shows that the number of Mobile devices has outnumbered desktops by the ratio 1:4. Thanks to the powerful processors of smartphones and the modern operating systems that power these devices, smartphone users can leverage mobile apps to perform most computing tasks that used to be exclusive to PC users. The sheer number of smartphone users and the various means of monetising Mobile Apps make it a lucrative niche for Dotcom Ventures. Mobile Apps development that leverages on the emerging sharing economy and other profitable niches shall be the focus of the Mobile App development business of the company.',  ],
                                    
                                    ['title'=>'E-Learning ',             				'img-class'=>'move_left',			'animate'=>'slideInRight',   'image'=>'grphcs04.png',   'order'=>'reverse',        'content'=>'Digital learning products is a hot niche that became popular at the turn of the century and does not show a sign of slowing down any time soon. Present trends show that e-learning is the future. In 2014, A Market research firm named Global Industry Analysts projected that the "E-Learning" would reach $107 Billion benchmarks in 2015,  and it did. Now, experts are forecasting that the industry shall triple within the next decade (2025). Reports from Forbes shows that online courses grossed a total of $46 Billion in 2017. These data provide empirical evidence that the e-learning niche is a viable sector which is growing exponentially. Dotcom Ventures shall provide e-learning in the form of electronic books and multimedia training delivery system to capture the population of visual learners.',  ],
                                    
                                    ['title'=>'Online Services',         				'img-class'=>'move_right',			'animate'=>'slideInLeft',   'image'=>'grphcs02.png',   'order'=>'',        'content'=>'Over the last decade, the deployment of applications has changed from desktop-based to cloud-based application delivery system. The rise to prominence of cloud-based programs is driven by the many advantages it provides to the developers and the end users as well. Delivering applications on the cloud using the Software as a Service (SaaS) model offers many advantages for Dotcom Ventures. It increases the number of potential users because of the lower cost of ownership, cross-platform availability, lower operating hardware requirements, and circumventing the bottleneck of distribution networks. Consequently, this model enhances sustainability and increase in overall revenue because the services are available on a monthly subscription basis.  Dotcom Venture shall provide in-demand online services for carefully selected niches after comprehensive market research.',  ],
                                ];
                            ?>

                            @foreach($usps as $x => $usp)                            
                            <div class="row {{ $usp['order'] }}">
                                    
                                <div class="img_container">
                                    <img class="{{ $usp['img-class'] }}" src="{{ asset('images/home/graphics/'.$usp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}">
                                </div>

                                <div class="col-md-8 col-offset-md-4 content_text">
                                <div class="usp wow {{ $usp['animate'] }}">
                                    <div class="">

                                        <div class="title">
                                            <p class="small_heading_navy raleway font_400"> 
                                                <span>{{ $usp['title'] }}</span>
                                            </p>
                                        </div>

                                        <p class="default_text default_text_light open_sans">
                                            <?= $usp['content'] ?>
                                        </p>

                                    </div>
                                </div>
                                </div>

                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



















    <section class="big_padding" id="present_products">
        <div class="container">
            <p class="default_text raleway text-center default_small_heading blue_color font_200">Tangible value in the form of digital products</p>
            <h2 class="default_section_heading text-center navy_blue">
                <span class="font_200">
                    Hot
                </span>
                Developments
            </h2>
            <hr class="default_divider default_divider_blue ">

            <p class="default_text_light default_text open_sans main_text xs_padding">
                The CEO of Dotcom Ventures brings tangible value into the company in the form of digital products and platforms proven to be successful in their own right.                 
            </p>
            
            <?php

                $socialBal = [
                    [ 'type'=>'instagram', 'icon'=>'instagram', 'url'=>'https://www.instagram.com/buyanylight/'],
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/BuyAnyLight/'],
                    [ 'type'=>'linkedin', 'icon'=>'linkedin', 'url'=>'https://www.linkedin.com/company/buyanylight/'],
                    [ 'type'=>'twitter', 'icon'=>'twitter', 'url'=>'https://twitter.com/BuyAnyLight'],
                    [ 'type'=>'youtube', 'icon'=>'youtube', 'url'=>'https://www.youtube.com/c/BuyAnyLight'],
                    [ 'type'=>'telegram', 'icon'=>'telegram', 'url'=>'https://t.me/buyanylight'],
                ];

                $socialBillboardly = [
                    [ 'type'=>'instagram', 'icon'=>'instagram', 'url'=>'https://www.instagram.com/billboard.ly/'],
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/billboard.ly/'],
                    [ 'type'=>'linkedin', 'icon'=>'linkedin', 'url'=>'https://www.linkedin.com/company/billboard-ly/'],
                    [ 'type'=>'twitter', 'icon'=>'twitter', 'url'=>'https://twitter.com/billboard_ly'],
                ];
    

                $dps = [
                    ['title'=>'Buy Any Light',            'img_class'=>'buyanylight',  'url'=>'www.buyanylight.com',         'image'=>'images/hot-developments/bal.png', 'style'=>"", 'social'=>$socialBal],
                    ['title'=>'Billboardly',              'img_class'=>'billboardly',  'url'=>'www.billboard.ly',            'image'=>'images/hot-developments/billboardly.png', 'style'=>"", 'social'=>$socialBillboardly],
                ];
            ?>


            <style>
                
            </style>

	 		<div class="row hot_devs">
                @foreach($dps as $dp)
                <div class="col-sm-6 ">
                    <div class="hot_dev wow slideInLeft">
                        <a href="http://{{ $dp['url'] }}" class="anchor_style_default"  target="_blank">
                            <h3 class="small_heading_navy raleway center_on_mobile font_400">{{ $dp['title'] }}</h3>
                        </a>
                        <p class="default_text default_text_light open_sans center_on_mobile">
                            <a href="http://{{ $dp['url'] }}" target="_blank">{{ $dp['url'] }}</a>
                        </p>
                        <a href="http://{{ $dp['url'] }}" target="_blank" class="img_a">
                            <div class="blog_img">
                                <img style="{{ $dp['style'] }}" src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                            </div>
                        </a>
                    </div>
                    <div class="social">
                        <ul class="footer_links text-center">
                            @foreach ($dp['social'] as $soc)
                            <li>
                                <a class="anchor_style_default {{ $soc['type'] }} wow zoomInDown" href="{{ $soc['url'] }}" target="_blank">
                                    <i style=""; class="text-center fab fa-{{ $soc['icon'] }}"></i>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>


			
            {{-- <div class="swiper_main_container">
                <div class="present_products_slider_main swiper-container">
                    <div class="swiper-wrapper">

                        
                        @foreach($dps as $dp)
                        <div class="swiper-slide">
                            <div class="blog_slide wow slideInLeft">
                                <a href="http://{{ $dp['url'] }}" class="anchor_style_default"  target="_blank">
                                    <h3 class="small_heading_navy raleway center_on_mobile font_400">{{ $dp['title'] }}</h3>
                                </a>
                                <p class="default_text default_text_light open_sans center_on_mobile">
                                    <a href="http://{{ $dp['url'] }}" target="_blank">{{ $dp['url'] }}</a>
                                </p>
                                <a href="http://{{ $dp['url'] }}" target="_blank" class="img_a">
                                    <div class="blog_img">
                                            <img class="{{ $dp['img_class'] }}" style="{{ $dp['style'] }}" src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                                    </div>
                                </a>
                            </div>
                        </div>    
                        @endforeach

                    </div>                    
                </div>
                <div class="swiper-navs">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div> --}}


            <p class="default_text_light default_text open_sans main_text " style="padding-top: 20px;">
                Additional products and services shall be produced to form a consortium of digital products and services that shall be sub-brands of the Dotcom Ventures.
            </p>


            
 


        </div>
    </section>



    <section class="big_padding" id="successful_spinoffs">
        <div class="container">
                
                <div class="side_section_text xs_padding default_content">
                    {{-- <p class="default_small_heading raleway blue_color font_200">Dotcom Ventures FZE</p> --}}
                    <h3 class="default_section_heading raleway navy_blue">Successful <span class="font_200">Spin-Offs</span></h3>
                    <hr class="default_divider default_divider_blue " style="margin-left: 0;">

                    <p class="default_text_light default_text open_sans">                        
                        Take a look at what we have developed with passion and lots of joy in the past. Today, amazing teams continue to work on our ventures adding further value to the market resulting in outstanding products and better lives for all!
                    </p>
                


                    <div class="swiper_main_container">

                        <div class="successful_spinoffs_slider_main swiper-container">                    
                            <div class="swiper-wrapper">
                                <?php

                                    $socialAlmani = [
                                        [ 'type'=>'instagram', 'icon'=>'instagram', 'url'=>'https://www.instagram.com/almani_lighting'],
                                        [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/almani.lighting'],
                                        [ 'type'=>'linkedin', 'icon'=>'linkedin', 'url'=>'https://www.linkedin.com/company/almani-lighting'],
                                        [ 'type'=>'twitter', 'icon'=>'twitter', 'url'=>'https://twitter.com/Almani_Lighting'],
                                    ];

                                    $socialZoomDNA = [
                                        [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/zoomdna'],
                                    ];

                                    $socialDubaiConsult = [
                                        [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/DubaiConsult'],
                                    ];

                                    $dps = [
                                        ['title'=>'Almani Lighting',  'img_class'=>'',  'url'=>'www.almani.ae',         'image'=>'images/hot-developments/almani.png',    'social'=> $socialAlmani],
                                        ['title'=>'ZoomDNA',          'img_class'=>'',  'url'=>'www.zoomdna.com',       'image'=>'images/more-projects/zoomdna.png',      'social'=> $socialZoomDNA],
                                        ['title'=>'DubaiConsult',     'img_class'=>'',  'url'=>'www.dubaiconsult.com',  'image'=>'images/more-projects/dubaiconsult.png', 'social'=> $socialDubaiConsult],
                                    
                                    ];
                                ?>

                                @foreach($dps as $dp)
                                <div class="swiper-slide">
                                     <div class="blog_slide wow slideInLeft">
                                        <a href="http://{{ $dp['url'] }}" class="anchor_style_default"  target="_blank">
                                            <h3 class="small_heading_navy raleway center_on_mobile font_400">{{ $dp['title'] }}</h3>
                                        </a>
                                        <p class="default_text default_text_light open_sans center_on_mobile">
                                            <a href="http://{{ $dp['url'] }}" target="_blank">{{ $dp['url'] }}</a>
                                        </p>
                                        <a href="http://{{ $dp['url'] }}" target="_blank" class="img_a">
                                            <div class="blog_img">
                                                    {{-- <img src="images/sample-bgs/blog_post_1.jpg" alt="blog_img"> --}}
                                                    <img src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                                            </div>
                                        </a>
                                        {{-- <p class="default_text open_sans center_on_mobile">
                                            Aenean faucibus nulla ex, in rutrum dui blandit at. Pellentesque bibendum odio vitae gravida consequat aliquam sem morbi dor sit imet sen
                                        </p> --}}
                                    </div>
                                    <div class="social">
                                        <ul class="footer_links text-center">
                                            @foreach ($dp['social'] as $soc)
                                            <li>
                                                <a class="anchor_style_default {{ $soc['type'] }} wow zoomInDown" href="{{ $soc['url'] }}" target="_blank">
                                                    <i style=""; class="text-center fab fa-{{ $soc['icon'] }}"></i>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>                                    
                                </div>
                                @endforeach

                            </div>
                            {{-- <div class="swiper-pagination-custom"></div> --}}
                        </div>

                        <div class="swiper-navs">
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>

                    </div>





                </div>

        </div>
    </section>




    <section style="display: none;" class="big_padding d-none" id="more_projects">
        <div class="container">
                
                <div class="side_section_text xs_padding default_content">
                    {{-- <p class="default_small_heading raleway blue_color font_200">Dotcom Ventures FZE</p> --}}
                    <h3 class="default_section_heading raleway navy_blue">More <span class="font_200">Ventures</span></h3>
                    <hr class="default_divider default_divider_blue " style="margin-left: 0;">

                    <p class="default_text_light default_text open_sans">                        
                        We pride ourselves on each project undertaken in the past and grateful for the invaluable experiences gained through success and failure. These experiences have shaped and set high standards for our products today. Through this we continue to persue our entrepreneurial dream and deliver cutting edge innovations to create better lives for all.
                    </p>
                


                    <div class="swiper_main_container">

                        <div class="more_projects_slider_main swiper-container">                    
                            <div class="swiper-wrapper">
                                <?php
                                    $dpss = [

                                        ['title'=>'eREEF',         'img_class'=>'',  'url'=>'',                 'image'=>'images/more-projects/ereef.png',         ],
                                        ['title'=>'MyFoil',        'img_class'=>'',  'url'=>'',                 'image'=>'images/more-projects/myfoil.png',        ],
                                        ['title'=>'ClubRank',        'img_class'=>'',  'url'=>'',                 'image'=>'images/more-projects/clubrank.png',    ],
                                    ];
                                ?>

                                @foreach($dpss as $dp)
                                <div class="swiper-slide">
                                    <div class="blog_slide wow slideInLeft">
                                        {{-- <a href="http://{{ $dp['url'] }}" class="anchor_style_default"  target="_blank">
                                            <h3 class="small_heading_navy raleway center_on_mobile font_400">{{ $dp['title'] }}</h3>
                                        </a> --}}

                                        @if($dp['url'])                                   
                                                                                        
                                            <a href="http://{{ $dp['url'] }}" target="_blank" class="img_a">
                                                <div class="blog_img">
                                                        {{-- <img src="images/sample-bgs/blog_post_1.jpg" alt="blog_img"> --}}
                                                        <img src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                                                </div>
                                            </a>

                                        @else 

                                            <div href="" class="img_a">
                                                <div class="blog_img">
                                                        {{-- <img src="images/sample-bgs/blog_post_1.jpg" alt="blog_img"> --}}
                                                        <img src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                                                </div>
                                            </div>                                        
                                        
                                        @endif



                                        {{-- <p class="default_text open_sans center_on_mobile">
                                            Aenean faucibus nulla ex, in rutrum dui blandit at. Pellentesque bibendum odio vitae gravida consequat aliquam sem morbi dor sit imet sen
                                        </p> --}}
                                    </div>
                                </div>
                                @endforeach

                            </div>
                            {{-- <div class="swiper-pagination-custom"></div> --}}
                        </div>

                        <div class="swiper-navs">
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>

                    </div>





                </div>

        </div>
    </section>

 





















    <section class="half_section_main  "  id="future_prospects" style="background-image: url({{ asset('images/home/img32.jpg') }});">
        <div class="half_section_picture" style="background-image: url({{ asset('images/home/graphics/grphcs06.png') }});"></div>
        <div class="container container_big">
            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="side_section_text big_padding" style="padding-bottom: 0;">
                        <h3 class="default_section_heading raleway navy_blue text-center">
                            Future <span class="font_200">Prospects</span>
                        </h3>
                        <hr class="default_divider default_divider_blue " style="">
                    </div>
                </div>
            </div> --}}

            <div class="row display_flex">
                
                {{-- <div class="col-md-6 valign_center">
                    <div class="image" style="background-image: url({{ asset('images/home/graphics/grphcs06.png') }});"></div>
                </div> --}}

                <div class="col-md-6 col-md-offset-6">
                    <div class="side_section_text xs_padding">
                        <p class="default_small_heading raleway blue_color font_200">Dotcom Ventures FZE</p>
                        <h3 class="default_section_heading raleway navy_blue">Future <span class="font_200">Prospects</span></h3>
                        <hr class="default_divider default_divider_blue default_divider_medium" style="margin-left: 0;">

                        <p class="default_text_light default_text open_sans">
                            Bearing in mind that the digital economy emerged at the turn of the century and the fact that new niches are still surfacing, the future potential is enormous. 
                        </p>
                        <br>
                        <div class="row item_sections">
                            <div class="col-md-12">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        {{-- <i class="fa fa-comment-o pink_color" aria-hidden="true"></i>  --}}
                                        {{-- <i class="fas fa-exclamation "></i> --}}
                                        <i class="fas fa-exclamation-triangle"></i>
                                        Potent force
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        Dotcom Ventures seeks to position the company as a potent force in this critical period of digital evolution. 
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        {{-- <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i>  --}}
                                        <i class="fas fa-lightbulb"></i>
                                        Name Brands
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        Dotcom Ventures hope to produce brands that shall become household names in the near future.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </section>






































    <!-- Contact Form Section -->
    <section class="contact_form_section" id="contact-form">
        <div class="container">
            <div class="row">
                <div class="contact_form_inner big_padding clearfix">
                    <div class="col-md-6 wow slideInLeft">
                        <div class="contact_form_detail  center_on_mobile">
                            <p class="default_small_heading raleway blue_color font_200">Contact Us</p>
                            <h3 class="default_section_heading raleway navy_blue"> <span class="font_200">Let's Get In</span> Touch</h3>
                            <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                            {{-- <p class="default_text_light default_text open_sans">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis.
                            </p> --}}
                            <div class="row">
                                <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-map-marker blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <p class="default_text form_text open_sans default_text_light">
                                            Dotcom Ventures FZE
                                        </p>
                                        <p class="default_text form_text open_sans default_text_light">
                                            Business Center
                                        </p>
                                        <p class="default_text form_text open_sans default_text_light">
                                            Al Shmookh Building
                                        </p>
                                        <p class="default_text form_text open_sans default_text_light">
                                            UAQ Free Trade Zone
                                        </p>
                                        <p class="default_text form_text open_sans default_text_light">
                                            Umm Al Quwain, UAE
                                        </p>
                                    </div>
                                </div>
                                <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-phone blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <p class="default_text form_text open_sans default_text_light">
                                            <a href="tel:+97148873265">+971 4 887 3265</a>
                                        </p>
                                    </div>
                                </div>


                                <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-globe blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <p class="default_text form_text open_sans default_text_light">
                                            i<span></span>nfo<span></span>@dot<span>com</span>v<span></span>.com
                                        </p>                                        
                                        <a href="https://dotcomv.com" class="anchor_style_default" target="_blank">
                                            <p class="default_text form_text open_sans default_text_light">
                                                https://dotcomv.com
                                            </p>
                                        </a>
                                    </div>
                                </div>

                                {{-- <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-facebook  blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <a href="https://www.facebook.com/Dotcom-Ventures-271688870188257/" class="anchor_style_default" target="_blank">
                                            <p class="default_text form_text open_sans default_text_light">
                                                Dotcom Ventures
                                            </p>
                                        </a>
                                    </div>
                                </div> --}}







                            </div>
                        </div>
                    </div>
                        <div class="col-md-6 wow slideInRight"> 
                            {{-- // --}}
                        <form method="post" action="{{ route('contact.inquiry') }}" name="inquiry" id="inquiry" class="form_class">
                               {{ csrf_field() }}
                            <div class="row">
                                <div class="mew_form clearfix">
                                    <div class="col-sm-12" id="result"></div>
                                    <div class="col-sm-6">
                                        <input placeholder="Your Name" class="form_inputs" id="name" name="name" required="required">
                                    </div>
                                    <div class="col-sm-6">
                                        <input placeholder="Email" class="form_inputs" id="email" name="email" required="required">
                                    </div>

                                     <div class="col-sm-6">
                                        <input placeholder="Company" class="form_inputs" id="company" name="company" required="required">
                                    </div>
                                    <div class="col-sm-6">
                                        <input placeholder="Your Subject" class="form_inputs" id="subject" name="subject" required="required">
                                    </div>

                                    <div class="col-sm-12">
                                        <textarea placeholder="Your Message" class="form_inputs form_textarea" id="message" name="message" required="required"></textarea>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="privacy-field">
                                           <input type="checkbox" name="checkbox" class="" id="check_invoice" value="0" required="required">
                                         <span class="default_small_heading raleway blue_color font_200t" for="check_invoice">I agree the processing of my personal data. Click <a href="#" data-target="#pp_modal" data-toggle="modal">Privacy Policy</a> for more info.</span>
                                        </div>
                                    </div> 

                                     <div class="col-xs-12">
                                        <div class="button_div center_on_mobile">
                                            <a href="javascript:;" id="submit_btn"   class=" submit_btn bg_pink button button_default_style open_sans bg_before_navy">
                                                <i class="fa fa-envelope" aria-hidden="true"></i> Send
                                                Message</a>
                                        </div>
                                        <input id="submit_handle" type="submit" data-badge="inline" 
                                        class="g-recaptcha" data-callback="onFormSubmit"  
                                        data-sitekey="6Lf2zIIUAAAAAG9_hsOsroY7wHkEmvBMGLRUILWC" style="display: none"/>
                                    </div>

                                        <script type="text/javascript">

                                                      function onFormSubmit(token) {

                                                        return new Promise(function(resolve, reject) {

                                                            var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                                                            if($('#name').val() === '') {

                                                                alert('Please Enter your Name');
                                                                reset();

                                                            } else if($('#email').val() === '') {

                                                                alert('Please Enter your Email');
                                                                reset();

                                                            } else if(!emailFilter.test($('#email').val())) {

                                                                alert('Please Enter a Valid Email');
                                                                reset();

                                                            } else if($('#subject').val() === '') {

                                                                alert('Please Enter your Subject');
                                                                reset();

                                                            } else if($('#message').val() === '') {

                                                                alert('Please Enter your Message');
                                                                reset();

                                                            } else if($('#company').val() === '') {
                                                                alert('Please Enter your Company');
                                                            } else {
                                                                send();
                                                            }

                                                            resolve();

                                                        });

                                                       }

                                                       function reset() {
                                                            grecaptcha.reset();
                                                       }

                                                       function send() {
                                                             $('#submit_btn').html('Please Wait ..').attr('disabled','disabled');
                                                              document.getElementById("inquiry").submit();
                                                       }

                                                </script>   

                                                <style type="text/css">

                                                    /*// Extra small (<480px)*/
                                                    @media(max-width: $screen-xs-max){
                                                      .g-recaptcha iframe {
                                                        max-width: 100%;
                                                        transform:scale(0.77);
                                                        -webkit-transform:scale(0.77);
                                                        transform-origin: center center;
                                                        -webkit-transform-origin: center center;
                                                      }
                                                      #rc-imageselect {
                                                        transform:scale(0.77);
                                                        -webkit-transform:scale(0.77);
                                                        transform-origin:0 0;
                                                        -webkit-transform-origin:0 0;
                                                      }
                                                    }

                                                    /*// Medium small (>=480px)*/
                                                    @media(min-width: $screen-ms-min){
                                                      #rc-imageselect {
                                                        transform: none;
                                                        -webkit-transform: none;
                                                      }

                                                      .g-recaptcha iframe {
                                                        max-width: none;
                                                        transform: none;
                                                        -webkit-transform: none;
                                                      }
                                                    }

                                                    /*// Horizontally center the recaptcha - applied to all widths*/
                                                    .g-recaptcha > div > div{
                                                      margin: 4px auto !important;
                                                      text-align: center;
                                                      width: auto !important;
                                                      height: auto !important;
                                                    }

                                                </style>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- /Contact Form  Section -->





    <!-- Footer Section -->
    <footer class="footer_section big_padding">
        <div class="container">
            <div class="footer_detail">
                {{-- <ul class="footer_links text-center">
                    <li>
                        <a class="anchor_style_default facebook wow zoomInDown" href="#.">
                            <i class="text-center fa fa-facebook "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default twitter wow zoomInUp" href="#.">
                            <i class="text-center fa fa-twitter "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default g_plus wow zoomInDown" href="#.">
                            <i class="text-center fa fa-google-plus "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default linkedin wow zoomInUp" href="#.">
                            <i class="text-center fa fa-linkedin "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default  pinterest wow zoomInDown" href="#.">
                            <i class="text-center fa fa-pinterest"></i>
                        </a>
                    </li>
                </ul> --}}
                    <ul class="footer_links text-center">

                         {{--    
                            <li>
                                <a class="anchor_style_default messenger wow zoomInUp" href="https://m.me/nosecretdiet"
                                target="_blank">
                                <i class="text-center fab fa-facebook-messenger"></i>
                            </a>
                        </li>
                        <li>
                            <a class="anchor_style_default pinterest wow zoomInDown" href="https://www.pinterest.com/loseweightorgetpaid/" target="_blank">
                                <i class="text-center fab fa-pinterest"></i>
                            </a>
                        </li>
                            <li>
                                <a class="anchor_style_default g_plus wow zoomInDown" href="#.">
                                    <i class="text-center fa fa-google-plus "></i>
                                </a>
                            </li>                        
                        
                        --}}
                            <li>
                                <a class="anchor_style_default facebook wow zoomInDown" href="https://www.facebook.com/DotcomV/" target="_blank">
                                    <i style="color: #fff"; class="text-center fa fa-facebook "></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default linkedin wow zoomInUp" href="https://www.instagram.com/dotcomv_/" target="_blank">
                                    <i style="color: #fff"; class="text-center fab fa-instagram"></i>
                                </a>
                            </li>
                            
                            <li>
                                <a class="anchor_style_default linkedin wow zoomInUp" href="https://www.linkedin.com/company/dotcomv" target="_blank">
                                    <i style="color: #fff"; class="text-center fa fa-linkedin "></i>
                                </a>
                            </li>
                            <li>
                                <a class="anchor_style_default twitter wow zoomInUp" href="https://twitter.com/DotcomV_" target="_blank">
                                    <i style="color: #fff"; class="text-center fa fa-twitter "></i>
                                </a>
                            </li>
                            


                        </ul>

                <p class="text-center default_text open_sans white_color">Dotcom Ventures FZE<span class="symbol">&copy;</span> All rights reserved 2018. </p>
                                
                <p class="text-center default_text open_sans white_color pp_tc">
                    <a href="#" data-target="#pp_modal" data-toggle="modal">Privacy Policy</a>
                    <span class="period"><i class="fas fa-circle"></i></span>
                    <a href="#" data-target="#tc_modal" data-toggle="modal">Terms & Conditions</a>
                </p>
        
                <p class="text-center default_text open_sans white_color">
                    Made with ❤ in Dubai
                </p>

               
            </div>
        </div>
    </footer>
    <!-- /Footer Section -->














            <div class="to_top sticky_buttons">
                <i class="fas fa-angle-up"></i>
            </div>
            

            

            <div class="share_facebook sticky_buttons">
                <a title="Share in Facebook" href="fb-messenger://share/?link= https%3A%2F%2Fdotcomv.com">
                    <i class="fab fa-facebook-messenger"></i>
                </a>
            </div>


            <div class="share_whatsapp sticky_buttons">
                <a title="Share via Whatsapp"
                    href="https://api.whatsapp.com/send?text= https%3A%2F%2Fdotcomv.com">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </div>






































































































































































            <div class="modal about-modal fade" id="tc_modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">
                                <img src="{{ asset('images/logos/logo-sm.png') }}">
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-info">

                                <div class="document">

                                    <h2>Terms and Condition</h2>

                                    <div class="document_text_content">

                                        <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">
                                            PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY.
                                        </p>
                                        <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">
                                            THESE TERMS AND CONDITIONS SHALL BE BINDING UPON USERS OF WEBSITE WWW.DOTCOMV.COM.
                                        </p>
                                        

                                        <h3>Introduction</h3>
                                        <p>Please carefully read the Terms and Conditions ("Terms") for the website located at www.dotcomv.com (the "Website") including its sub-domains and mobile optimized version, as set out hereinafter. The Website is owned and operated by Dotcom Ventures FZE (hereinafter referred to also as "Company").</p>
                                        <p>The Company is a holding/parent company of its various other subsidiary internet/tech related companies. The Company aims at selling digital products through its subsidiaries as well as operating different digital platforms offering service based on a monthly membership fee.</p>
                                        <p>All our subsidiary companies have separate terms and conditions for their respective services.</p>
                                        <p>Any ancillary terms, guidelines, the privacy policy (the "Policy") and other documents made available by the Website from time to time and as incorporated herein by reference, shall be deemed as an integral part of the Terms. These terms set forth the legally binding agreement between you as the user(s) of the Website (hereinafter referred to as "you", "your" or "User") and the Company. If you are using the Website or its services on behalf of an entity, organization, or company (collectively "Subscribing Organization"), you declare that you are an authorized representative of that Subscribing Organization with the authority to bind such organization to these Terms; and agree to be bound by these Terms on behalf of such Subscribing Organization. In such a case, "you" in these Terms refers to your Subscribing Organization, and any individual authorized to use the Service on behalf of the Subscribing Organization, including you.</p>
                                        <p>By using company provided services, accessing or using the Website in any manner as laid down herein, including, but not limited to, visiting or browsing it, or contributing content or other materials to it, you agree to be bound by these Terms. </p>
                                        <p>These Terms, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by the Website without restriction. Any attempted transfer or assignment in violation hereof shall be null and void. </p>
                                        

                                        <h3>Acceptance of the Terms</h3>
                                        <p>Each time by viewing, using, accessing, browsing, or submitting any content or material on the Website, including the webpages contained or hyperlinked therein and owned or controlled by the Website and its services, whether through the Website itself or through such other media or media channels, devices, software, or technologies as the Website may choose from time to time, you are agreeing to abide by these Terms, as amended from time to time with or without your notice. </p>
                                        <p>The Website reserves the right to modify or discontinue, temporarily or permanently, and at any time, the Website and/or the Website Services (or any part thereof) with or without notice. You agree that the Website shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Website Services.</p>
                                        <p>Website or the Website management may modify these Terms from time to time, and any change to these Terms will be reflected on the Website with the updated version of the Terms and you agree to be bound to any changes to these Terms when you use the Website or the Website Services. The Website may also, in its sole and absolute discretion, choose to alert via email all users with whom it maintains email information of such modifications.</p>
                                        <p>Also, occasionally there may be information on the Website or within the Website Services that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, availability, and various other information, and the Website management reserves the right to correct any errors, inaccuracies or omissions and to change or update the information at any time, without prior notice. </p>
                                        <p>When you register an account on the Website and/or upload, submit, enter any information or material to the Website or use any of the Website Services, you shall be deemed to have agreed to and understand the Terms.</p>
                                        

                                        <h3 style="text-align: center;">IF YOU DO NOT AGREE TO THESE TERMS OF USE, THEN YOU MAY NOT USE THE WEBSITE</h3>


                                        <h3>Service Availability</h3>
                                        <p>The Website shall use commercially reasonable efforts to keep it up and running 24 hours a day, seven days a week; provided, however, that it may carry out scheduled and unscheduled maintenance work as necessary from time to time and such maintenance work may impact the availability of the Website.</p>
                                        

                                        <h3>Your Legal Obligations</h3>
                                        <p>You agree that the use of the Website and/or the Website Services on the Website is subject to all applicable local, state and federal laws and regulations. Unless specifically endorsed or approved by the Website, you also agree:</p>
                                        <ul>
                                            <li>not to use the Website for illegal purposes;</li>
                                            <li>not to commit any acts of infringement on the Website or with respect to content on the Website;</li>
                                            <li>not to copy any content for republication in print or online;</li>
                                            <li>not to create reviews or blog entries for or with any purpose or intent that does not in good faith comport with the purpose or spirit of the Website;</li>
                                            <li>not to attempt to gain unauthorized access to other computer systems from or through the Website;</li>
                                            <li>not to interfere with another person's use and enjoyment of the Website or another entity's use and enjoyment of the Website;</li>
                                            <li>not to upload or transmit viruses or other harmful, disruptive or destructive files; and/or</li>
                                            <li>not to disrupt, interfere with, or otherwise harm or violate the security of the Website, or any services, system restores, accounts, passwords, servers or networks connected to or accessible through the Website or affiliated or linked website.</li>
                                            <li>not to use the Website in any way or take any action that causes, or may cause, damage to the Website or impairment of the performance, availability or accessibility of the Website;</li>
                                            <li>not to use the Website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</li>
                                            <li>not to use the Website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</li>
                                            <li>not to conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to the Website without the express written consent of the Website owner;</li>
                                            <li>not to access or otherwise interact with the Website using any robot, spider or other automated means;</li>
                                            <li>not to use data collected from the website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing);</li>
                                            <li>not to infringe these Terms or allow, encourage or facilitate others to do the same;</li>
                                            <li>not to plagiarize and/or infringe the intellectual property rights or privacy rights of any third party;</li>
                                            <li>not to disturb the normal flow of Services provided within the Website;</li>
                                            <li>not to create a link from the Website to another website or document without Company’s prior written consent;</li>
                                            <li>not to obscure or edit any copyright, trademark or other proprietary rights notice or mark appearing on the Website;</li>
                                            <li>not to create copies or derivate works of the Website or any part thereof;</li>
                                            <li>not to reverse engineer, decompile or extract the Website’s source code;</li>
                                            <li>not to remit or otherwise make or cause to deliver unsolicited advertising, email spam or other chain letters;</li>
                                            <li>not to collect, receive, transfer or disseminate any personally identifiable information of any person without consent from title holder; and/or</li>
                                            <li>not to pretend to be or misrepresent any affiliation with any legal entity or third party.</li>
                                        </ul>
                                        

                                        <p>In addition to the above clause, unless specifically endorsed or approved by the Website, the following uses and activities of and with respect to the Website and the Website Services are prohibited:</p>
                                        <ul>
                                            <li>criminal or tortuous activity, including child pornography, fraud, trafficking in obscene material, drug dealing, gambling, harassment, stalking, spamming, copyright infringement, patent infringement, or theft of trade secrets;</li>
                                            <li>transmitting chain letters or junk email;</li>
                                            <li>interfering with, disrupting, or creating an undue burden on the Website or the Website Services or the networks or services connected or linked thereto;</li>
                                            <li>using any information obtained from the Website or the Website Services in order to harass, abuse, or harm another person;</li>
                                            <li>deciphering, decompiling, disassembling or reverse engineering any of the software comprising or in any way making up a part of the Website or the Website Services;</li>
                                            <li>attempting to bypass any measures of the Website or the Website Services designed to prevent or restrict access to the Website or the Website Services, or any portion of the Website or the Website Services;</li>
                                            <li>harassing, annoying, intimidating or threatening any the Website employees or agents engaged in providing any portion of the Website Services;</li>
                                            <li>using the Website and/or the Website Services in any manner inconsistent with any and all applicable laws and regulations.</li>
                                            <li>Using data collected from the website to contact individuals, companies or other persons or entities.</li>
                                            <li>Supplying false, untrue, expired, incomplete or misleading information through the Website.</li>
                                        </ul>
                                        
                                        <p>You also acknowledge and accept that any violation of the aforementioned provisions may result in the immediate termination of your access to the Website and use of our Services. Access to the Website may be terminated or suspended without prior notice or liability of Company. You represent and warrant to us that you have all right, title, and interest to any and all content you may post, upload or otherwise disseminate through the Website. You hereby agree to provide Company with all necessary information, materials and approval, and render all reasonable assistance and cooperation necessary for our Services.</p>

                                        <h3>Third party websites</h3>
                                        <p>The Website includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations. Goods and services of third parties may be advertised and/or made available on or through this web site. Representations made regarding products and services provided by third parties are governed by the policies and representations made by these third parties. The Website shall not be liable for or responsible in any manner for any of your dealings or interaction with third parties.</p>
                                        <p>The management of the Website has no control over third party websites and their contents, and subject to the Terms it accepts no responsibility for them or for any loss or damage that may arise from your use of them.</p>
                                        <p>The Website may contain links from third party websites. External hyperlinks to or from the site do not constitute the Website’s endorsement of, affiliation with, or recommendation of any third party or its website, products, resources or other information. The Website is not responsible for any software, data or other information available from any third party website. You are solely responsible for complying with the terms and conditions for the third party sites. You acknowledge that Company shall have no liability for any damage or loss arising from your access to any third party website, software, data or other information.</p>
                                        <p>We do not always review the information, pricing, availability or fitness for use of such products and services and they will not necessarily be available or error free or serve your purposes, and any use thereof is at your sole risk.  We do not make any endorsements or warranties, whether express or implied, regarding any third party websites (or their products and services).  Any linked websites are ruled by their privacy policies, terms and conditions and legal disclaimers.  Please read those documents, which will rule any interaction thereof.</p>
                                        <p>The Website may provide tools through the Service that enable you to export information to third party services, including through use of an API or by linking your account on the Website with an account on the third party service, such as Twitter or Facebook. By using these tools, you agree that we may transfer such User Content and information to the applicable third party service. Such third party services are not under our control, and we are not responsible for the contents of the third party service or the use of your User Content or information by the third party service. The Service, including our websites, may also contain links to third-party websites. The linked sites are not under our control, and we are not responsible for the contents of any linked site. We provide these links as a convenience only, and a link does not imply our endorsement of, sponsorship of, or affiliation with the linked site. You should make whatever investigation you feel necessary or appropriate before proceeding with any transaction with any of these third parties services or websites.</p>


                                        
                                        

                                        <h3>Third party rights</h3>
                                        <p>A contract under the Terms is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.</p>
                                        <p>The exercise of the parties' rights under a contract under the Terms is not subject to the consent of any third party.</p>
                                        <p>You agree not to; modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, scrape, gather, market, rent, lease, re-license, reverse engineer, or sell any information published by other users without the original publishers written consent.</p>


                                        
                                        <h3>Ownership</h3>
                                        <p>The trademarks, copyright, service marks, trade names and other intellectual and proprietary notices displayed on the Website are the property of – or otherwise are licensed to – Company or its licensors or affiliates, whether acknowledged (or not), and which are protected under intellectual and proprietary rights throughout the world.  Respective title holders may or may not be affiliated with us or our affiliates, partners and advertisers.</p>
                                        <p>No section hereof shall be construed as intent to grant to you any interest in the Website or our Services, in whole or in part. All content and materials included as part of the Services, such as images, photographs, graphics, texts, forms, lists, charts, guidelines, data, logos, code, icons, videos, audio and other content are the property of, are licensed to or are otherwise duly available to Company, its affiliates, its licensors or to the appertaining third party copyrights holder.</p>
                                        <p>You acknowledge and agree that any and all infringing use or exploitation of copyrighted content in the Website and our Services may cause us, our affiliates, licensors or content providers irreparable injury, which may not be remedied solely at law, and therefore our affiliates, licensors or content providers may seek remedy for breach of these Terms, either in equity or through injunctive or other equitable relief.</p>


                                        


                                        <h3>Term and Termination</h3>
                                        <p>The term hereof shall begin on the date that comes first among: (i) first access to the Website; (ii) your first access or execution of our Services; or (iii) Company begins providing its Services to you.</p>
                                        <p>The term hereof will automatically end on the earlier date of either your: (i) account deactivation, suspension, freezing or deletion; (ii) access termination or access revocation for our Services or the Website; (iii) Company’s termination of these Terms or its Services, at its sole and final discretion; (iv) the termination date indicated by Company to you from time to time; or (v) Company’ decision to make the Website or Services no longer available for use, at its sole and final discretion.</p>
                                        <p>Upon expiration of these Terms or termination of your subscription to our Services, you shall thereafter immediately cease any and all use of our Services, along with any and all information and data collected therefrom.</p>
        
                                        
                                        
                                        <h3>Amendments</h3>
                                        <p>Company hereby reserves the right to update, modify, change, amend, terminate or discontinue the Website, the Terms and/or the Policy, at any time and at its sole and final discretion.  Company may change the Website’s functionalities and (any) applicable fees at any time.  Any changes to these Terms will be displayed in the Website, and we may notify you through the Website or by email. Please, refer to the date shown above for the date where effective changes were last undertook by us. Your use of our Services after the effective date of any update– either by an account registration or simple use – thereby indicates your acceptance thereof.</p>


                                        <h3>No Warranty</h3>
                                        <p>Your use of our Website is at your own risk, and therefore you hereby acknowledge and agree that our Website and Services are provided "as is", "with all faults", and "as available", including all content, guides, checklists, reference guides, sample filing forms, software, materials, services, functions and/or information made available thereby.  It shall be your own responsibility to ensure that the Services or information available through this Website meet your specific requirements.</p>
                                        <p>Neither Company, nor its affiliates, subsidiaries, officers, employees and agents warrantee that the Website will be error-free, uninterrupted, secure, or produce any particular results; or that any listing, purchase, order, amount, information, guide, sheet, checklist and/or content will be current, measured useful and/or valid, or that it will produce any particular results or that the information obtained therefrom will be reliable or accurate. No advice or information given by Company or its employees, affiliates, contractors and/or agents shall create a guarantee. No warranty or representation is made with regard to such services or products of third parties contacted on or through the Website.  In no event shall Company or our affiliates be held liable for any such services.</p>
                                        <p>Neither Company, nor its affiliates, licensors, owners, subsidiaries, brands or advertisers are a professional advisor in any industry.  The results described in the Website are not typical and will vary based on a variety of factors outside the control of Company. Your use of any information and/or materials on this Website is entirely at your own risk, for which we shall not be held liable.</p>

                                            


                                        <h3>Disclaimer of Damages</h3>
                                        <p>Your use of our Website is at your own risk, and therefore you hereby acknowledge and agree that our Website and Services are provided "as is", "with all faults", and "as available", including all content, guides, checklists, reference guides, sample filing forms, software, materials, services, functions and/or information made available thereby.  It shall be your own responsibility to ensure that the Services or information available through this Website meet your specific requirements.</p>
                                        <p>Neither Company, nor its affiliates, subsidiaries, officers, employees and agents warrantee that the Website will be error-free, uninterrupted, secure, or produce any particular results; or that any listing, purchase, order, amount, information, guide, sheet, checklist and/or content will be current, measured useful and/or valid, or that it will produce any particular results or that the information obtained therefrom will be reliable or accurate. No advice or information given by Company or its employees, affiliates, contractors and/or agents shall create a guarantee. No warranty or representation is made with regard to such services or products of third parties contacted on or through the Website.  In no event shall Company or our affiliates be held liable for any such services.</p>
                                        <p>Neither Company, nor its affiliates, licensors, owners, subsidiaries, brands or advertisers are a professional advisor in any industry.  The results described in the Website are not typical and will vary based on a variety of factors outside the control of Company. Your use of any information and/or materials on this Website is entirely at your own risk, for which we shall not be held liable.</p>




                                        <h3>Indemnification</h3>
                                        <p>You agree to indemnify, defend and hold Company and its independent contractors, affiliates, subsidiaries, officers, employees and agents, and their respective employees, agents and representatives, harmless from and against any and all actual or threatened proceedings (at law or in equity), suits, actions, damages, claims, deficiencies, payments, settlements, fines, judgments, costs, liabilities, losses and expenses (including, but not limited to, reasonable expert and attorney fees and disbursements) arising out of, caused or resulting from: (i) your conduct and any user content; (ii) your violation of these Terms or the Policy; and (iii) your violation of the rights of any third-party. </p>
                                        <p>You indemnify the Website and its management for any time that the Website may be unavailable due to routine maintenance, updates or any other technical or non-technical reasons. You agree to indemnify the Website and its management for any error, omission, interruption, deletion, defect, delay in operation or transmission, communication line failure, theft or destruction or unauthorized access to your published content, damages from lost profits, lost data or business interruption.</p>
                                        <p>You hereby indemnify the Website and its management and will not hold them responsible for copyright theft, reverse engineering and use of your content by other users on the website.</p>



                                        <h3>Generals</h3>
                                        <p>Advertisements and Promotions.  From time to time, we may place ads and promotions from third party sources in the Website.  Accordingly, your participation or undertakings in promotions of third parties other than Company, and any terms, conditions, warranties or representations associated with such undertakings, are solely between you and such third party.  Company is not responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of third party advertisers on the Website.</p>
                                        <p>No Assignment. You may not assign or transfer these Terms by operation of law or otherwise without our prior written consent.  Notwithstanding the foregoing, we may assign any rights or obligations hereunder to any current or future affiliated company and to any successor in interest.  Any rights not expressly granted herein are thereby reserved.  These terms will inure to the benefit of any successors of the parties.  We reserve the right, at any time, to transfer some or all of Company’s assets in connection with a merger, acquisition, reorganization or sale of assets or in the event of bankruptcy.</p>
                                        <p>Content Moderation.  Company hereby reserves the right, at its sole and final discretion, to review any and all content delivered into the Website, and use moderators and/or any monitoring technology to flag and remove any user generated content or other content deemed inappropriate.</p>
                                        <p>Force Majeure. Company is no liable for any failure of performance on its obligations as set forth herein, where such failure arises from any cause beyond Company's reasonable control, including but not limiting to, electronic, power, mechanic or Internet failure, from acts of nature, forces or causes beyond our control, including without limitation, Internet failures, computer, telecommunications or any other equipment failures, electrical power failures, strikes, labor disputes, riots, insurrections, civil disturbances, shortages of labor or materials, fires, flood, storms, explosions, acts of God, war, governmental actions, orders of domestic or foreign courts or tribunals or non-performance of third parties. </p>
                                        <p>Headings. The titles of paragraphs in these Terms are shown only for ease of reference and will not affect any interpretation therefrom.</p>
                                        <p>No Waiver. Failure by Company to enforce any rights hereunder shall not be construed as a waiver of any rights with respect to the subject matter hereof. </p>
                                        <p>No Relationship. You and Company are independent contractors, and no agency, partnership, joint venture, employee-employer, or franchiser-franchisee relationship is intended or created by these Terms.</p>
                                        <p>Notices. All legal notices or demands to or upon Company shall be made in writing and sent to Company personally, by courier, certified mail, or facsimile, and shall be delivered to any address the parties may provide.  For communications by e-mail, the date of receipt will be the one in which confirmation receipt notice is obtained.  You agree that all agreements, notices, demands, disclosures and other communications that Company sends to you electronically satisfy the legal requirement that such communication should be in writing.</p>
                                        <p>Severability. If any provision of these Terms is held unenforceable, then such provision will be modified to reflect the parties' intention. All remaining provisions of these Terms will remain in full force and effect. The failure of either party to exercise in any respect any right provided for herein will not be deemed a waiver of any further rights hereunder.</p>



                                        <h3>Contact</h3>
                                        <p>For any inquires or complaints regarding the Service or Website, please contact by email at in<span>fo@</span>d<span>otc</span>omv.c<span>om</span>.</p>
                                                                                
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>





<div id="success_tic" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <a class="close" href="#" data-dismiss="modal">&times;</a>
          <div class="page-body">
                  <div class="head">  
              <h3 style="margin-top:5px;" class="default_section_heading raleway navy_blue">Thank you!</h3>
              <h4 class="default_small_heading raleway blue_color font_200">One of our Member will get in touch with you!</h4>
            </div>

          <h1 style="text-align:center;"><div class="checkmark-circle">
          <div class="background"></div>
          <div class="checkmark draw"></div>
        </div><h1>
      </div>
    </div>
        </div>

  </div>





       <div class="modal about-modal fade" id="sccs_modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">
                                        <img src="{{ asset('images/logo-65.png') }}">
                                    </h4>
                            </div>
                                <div class="modal-body">
                                    <div class="modal-info">
                                            <div class=" success-modal">
                                                    <div id="success-icon">
                                                        <span class="fas-check">
                                                            <i class="fas fa-check"></i>
                                                        </span>
                                                    </div>
                                                    <h1><strong>Thank You!</strong></h1>
                                                    <p class="points">Purchase Completed</p>
                                                    <hr>
                                                    <?php if (isset(($message))): ?>
                                                        <p class="message">{{ $message }}</p>    
                                                    <?php endif ?>
                                                </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                </div>
















































            <div class="modal about-modal fade hasScrollTo" id="pp_modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">
                                <img src="{{ asset('images/logos/logo-sm.png') }}">

                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-info">


                                <div class="document">

                                    <h2>Privacy Policy</h2>

                                    <div class="document_text_content">


                                        {{-- <h3>Introduction</h3> --}}
                                        <p>By accessing or using www.dotcomv.com (the "Website"), a website owned and maintained by Dotcom Ventures FZE ("Company", "we," "us" or "our"), you consent to the information collection, disclosure and use practices described in this Privacy Policy. This Privacy Policy sets out how we may collect, use and disclose information in relation to users of the Website.</p>
                                        <p>The Company is a holding/parent company of its various other subsidiary internet/tech related companies. The Company aims at selling digital products through its subsidiaries as well as operating different digital platforms offering service based on a monthly membership fee.</p>
                                        <p>Use of Website of the Company is subject to separate Terms and Conditions and the following Privacy Policy. </p>
                                        
                                        <h3>Information We Collect</h3>
                                        <p>Your privacy is important to us and we have taken steps to ensure that we do not collect more information from you than what is necessary.</p>
                                        <p>All our subsidiary companies have separate Privacy Policies for their respective services.</p>                                            
                                        <p>We may collect, use and process the following information ("Data") about you:</p>
                                        <ul>
                                            <li>Personal Data, which may include name, email, address, date of birth, gender, age, phone number, and such other data as we may deem necessary, which shall, however, not include be any sensitive data.</li>
                                            <li>Communication Data, which may include any communication that you send to us whether that be through the contact form on our Website, through email, text, social media messaging, social media posting or any other communication that you send us. We process this data for the purposes of communicating with you, for record keeping and for the establishment, pursuance or defence of legal claims. Our lawful ground for this processing is our legitimate interests which in this case are to reply to communications sent to us, to keep records and to establish, pursue or defend legal claims.</li>
                                            <li>Technical Data, which may include data about your use of our Website such as your IP address, your login data, details about your browser, length of visit to pages on our Website, page views and navigation paths, details about the number of times you use our Website, time zone settings and other technology on the devices you use to access our Website.</li>
                                            <li>Marketing Data, which may include data about your preferences in receiving marketing from us and our third parties and your communication preferences.</li>
                                        </ul>


                                        <h3>How We Use and Process the Data</h3>
                                        <p>When you visit our Website you provide us consent to use your Data as per this Privacy Policy. </p>
                                        <p>The Data collected by us from you may be used to better understand your needs, to correspond with you and reply to your questions.</p>
                                        <p>We will not rent or sell your Data to others. If you provide any Data to us, you are deemed to have authorized us to collect, retain and use that data for the following purposes:</p>
                                        <ul>
                                            <li>verifying your identity;</li>
                                            <li>providing you with customer service and responding to your queries, feedback, or disputes;</li>
                                            <li>making such disclosures as may be required for any of the above purposes or as required by law, regulations and guidelines or in respect of any investigations, claims or potential claims brought on or against us; and</li>
                                            <li>maintaining the Website.</li>
                                        </ul>
                                        
                                        <p>We shall ensure that:</p>
                                        <ul>
                                            <li>The Data collected and processed for and on our behalf by any party is collected and processed fairly and lawfully;</li>
                                            <li>You are always made fully aware of the reasons for the collection of Data and are given details of the purpose(s) for which the data will be used;</li>
                                            <li>The Data is only collected to the extent that is necessary to fulfil the purpose(s) for which it is required;</li>
                                            <li>No Data is held for any longer than necessary in light of the purpose(s) for which it is required.</li>
                                            <li>Whenever cookies or similar technologies are used online by us, they shall be used strictly in accordance with the law;</li>
                                            <li>You are informed if any data submitted by you online cannot be fully deleted at your request under normal circumstances and how to request that the we delete any other copies of that data, where it is within your right to do so;</li>
                                            <li>All Data is held in a safe and secure manner taking all appropriate technical and organisational measures to protect the data;</li>
                                            <li>All data is transferred securely, whether it is transmitted electronically or in hard copy.</li>
                                            <li>You can fully exercise your rights with ease and without hindrance.</li>
                                        </ul>
    

                                        <h3>Data Storage and Transfer</h3>
                                        <p>Your Data may be stored and processed at the servers in the United States, Europe, or any other country in which the Website or its subsidiaries, affiliates or service providers maintain facilities.</p>
                                        <p>The Website may transfer Data to affiliated entities, or to other third parties across borders and from your country or jurisdiction to other countries or jurisdictions around the world. Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
                                        <p>We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>
                                        <p>We will only retain your Data preferably for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, see section regarding insurance requirement and reporting requirements. When deciding what the correct time is to keep the Data for we look at its amount, nature and sensitivity, potential risk of harm from unauthorised use or disclosure, the processing purposes, if these can be achieved by other means and legal requirements.</p>



                                        <h3>Protection of your Data</h3>
                                        <p>We store all the Data submitted by you through Website at a secure database.</p>
                                        <p>We are concerned with protecting your privacy and data, but we cannot ensure or warrant the security of any data you transmit to or guarantee that your Data may not be accessed, disclosed, altered or destroyed by breach of any of our industry standard physical, technical or managerial safeguards.</p>
                                        <p>No method of transmission over the Internet or method of electronic is secure. Therefore, we cannot guarantee its absolute security. If you have any questions about security of our Website, you can contact us at info@dotcomv.com.</p>
                                        <p>Any Data supplied by you will be retained by us and will be accessible by our employees, any service providers engaged by us and third parties.</p>
                                        


                                        <h3>Data Protection Officer</h3>
                                        <p>You may also contact Mr. Rene Rowell Dela Rama, our Data Protection Officer, at privacy@dotcomv.com.</p>
                                        
                                        
                                        <h3>The rights of Users </h3>
                                        <p>You may exercise certain rights regarding your Data processed by us. In particular, you have the right to do the following:</p>
                                        <ul>
                                            <li>Withdraw consent at any time. You have the right to withdraw consent where you have previously given your consent to the processing of your Data.</li>
                                            <li>Object to processing of Data. You have the right to object to the processing of your Data if the processing is carried out on a legal basis other than consent. </li>
                                            <li>Access to the Data. You have the right to learn if Data is being processed by us, obtain disclosure regarding certain aspects of the processing and obtain a copy of the Data undergoing processing.</li>
                                            <li>Verify and seek rectification. You have the right to verify the accuracy of your Personal and other Data and ask for it to be updated or corrected.</li>
                                            <li>Restrict the processing of your Data. You have the right, under certain circumstances, to restrict the processing of Data. In this case, we will not process your Data for any purpose other than storing it.</li>
                                            <li>Have the Data deleted or otherwise removed. You have the right, under certain circumstances, to obtain the erasure of the Data from us.</li>
                                        </ul>

                                        
                                        <h3>Compliance with the GDPR</h3>
                                        <p>For users based in the European Union (EU), the Website shall make all reasonable efforts to ensure that it complies with The General Data Protection Regulation (GDPR) (EU) 2016/679 as set forth by the European Union regarding the collection, use, and retention of Data from European Union member countries. Website shall make all reasonable efforts to adhere to the requirements of notice, choice, onward transfer, security, data integrity, access and enforcement.</p>

                                        
                                        <h3 id="cookies">Cookies</h3>
                                        <p>We use technologies, such as cookies, to make better user experience, customise content and advertising, to provide social media features and to analyse traffic to the Website. Where applicable the Website uses a cookie control system allowing the user on their first visit to the Website to allow or disallow the use of cookies on their computer / device.</p>
                                        <p>We also share information about your use of our site with our trusted social media, advertising and analytics partners. Third parties, including Facebook and Google, may use cookies, web beacons, and other storage technologies to collect or receive your information from the Websites and use that information to provide measurement services and target ads.</p>
                                        <p>Cookies are small files saved to the user's computers’ or mobile devices’ hard drive or memory that track, save and store information about the user's interactions and usage of the Website. This allows the Website, through its server to provide the users with a tailored experience within this Website.</p>
                                        <p>Users are advised that if they wish to deny the use and saving of cookies from this Website on to their computers hard drive they should take necessary steps within their web browsers security settings to block all cookies from this Website and its external serving vendors.</p>
                                        <p>We may gather certain information automatically and store it in log files. This information includes Internet protocol (IP) addresses, browser type, Internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and click stream data. We may use this information, which does not identify individual users, to analyze trends, to administer the Website, to track users’ movements around the Website and to gather demographic information about our user base as a whole.</p>
                                        <p>We may track the referring URL (the web page you left before coming to the Website) and the pages, links, and graphics of the Website you visited. We do so because it allows us to evaluate the reputation and responsiveness of specific web pages and any promotional programs we may be running.</p>
                                        <p>Managing Cookies: Many web browsers allow you to manage your preferences. You ca set your browser to refuse cookies or delete certain cookies. You may be able to manage other technologies in the same way that you manage cookies using your browser’s preferences.</p>
                                        <p>Please note that if you choose to block cookies, doing so may impair the Website functionality or prevent certain elements of it from functioning.</p>



                                        
                                        <h3>Third Party Links</h3>
                                        <p>The Website may contain links to third-party websites, plug-ins and applications. Except as otherwise discussed in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you on our Website. Other websites accessible through our site via links or otherwise have their own policies in regard to privacy. We are not responsible for the privacy policies or practices of third parties. When you leave our Website, we encourage you to read the privacy notice of every website you visit.</p>

                                        


                                        <h3>Changes to this Privacy Statement</h3>
                                        <p>We may modify these this Privacy Policy from time to time, and any such change shall be reflected on the Website with the updated version of the Privacy Policy and you agree to be bound to any changes to the updated version of Privacy Policy when you use the Website.</p>
                                        <p>You acknowledge and agree that it is your responsibility to review this Website and this Policy periodically and to be aware of any modifications. Updates to this Policy will be posted on this page.</p>
                                        <p>Also, occasionally there may be information on the Website that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, availability, and various other information, and the Website reserves the right to correct any errors, inaccuracies or omissions and to change or update the information at any time, without prior notice.</p>
                                        


                                        <h3>Contact US</h3>
                                        <p>If you have questions about our Privacy Policy, please contact us via email: <span>info@</span>dot<span>com</span>v.com.</p>
                                        


                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>









































{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
{{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}






</section>
<!-- /Parent Section Ended -->
<!-- jQuery 2.2.0-->
<script src="js/jquery.js"></script>
<!-- Google Map Api -->

{{-- <script src="http://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U" type="text/javascript"></script>
<script src="js/map.js" type="text/javascript"></script> --}}

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Owl Carousel 2 Core JavaScript -->
<script src="js/owl.carousel.js"></script>
<script src="js/owl.animate.js"></script>
<script src="js/owl.autoheight.js"></script>
<script src="js/owl.autoplay.js"></script>
<script src="js/owl.autorefresh.js"></script>
<script src="js/owl.hash.js"></script>
<script src="js/owl.lazyload.js"></script>
<script src="js/owl.navigation.js"></script>
<script src="js/owl.support.js"></script>
<script src="js/owl.video.js"></script>

<!-- Fancy Box Javacript -->
<script src="js/jquery.fancybox.js"></script>
<!-- Wow Js -->
<script src="js/wow.min.js"></script>
<!-- Appear Js-->
<script src="js/jquery.appear.js"></script>
<!-- Countdown Js -->
<script src="js/jquery.countdown.js"></script>

<!-- Particles Core Js -->
<script src="js/particles.js"></script>
<script src="js/particlesJS.min.js"></script>

<!-- Parallax Js -->
<script src="js/parallax.min.js"></script>
<!-- Cube Portfolio Core JavaScript -->
<script src="js/jquery.cubeportfolio.min.js"></script>
<!-- Circliful Core JavaScript -->
<script src="js/jquery.circliful.min.js"></script>
<!-- Swiper Slider Core JavaScript -->
<script src="js/swiper.min.js"></script>
<!-- Custom JavaScript -->
<script src="js/script.js"></script>

<script src="js/master-app.js?v={{ env('APP_ASSET_VER','1.0') }}"></script>





</body>
</html>


































{{-- 





    <!-- Half Section -->
    <section class="half_section_main "  id="skill_section">
        <div class="half_section_picture hidden-sm hidden-xs"></div>
        <div class="container container_big">
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="side_section_text big_padding">
                        
                        <p class="default_small_heading raleway blue_color font_200">About Us</p>
                        <h3 class="default_section_heading raleway navy_blue">About <span class="font_200"> will love it </span></h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">

                        <p class="default_text_light default_text open_sans">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis.
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> <i class="fa fa-comment-o pink_color" aria-hidden="true"></i>24/7 Support</p>
                                    <p class="default_text default_text_light open_sans">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A autem doloremque ea eum eveniet quas.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i> HTML5 Code</p>
                                    <p class="default_text default_text_light open_sans">
                                        Lorem Ipsum is simply text the printing and typesetting standard industry and dor sit imit elit.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- half Section Ended -->











    <!-- Half Section -->
    <section class="half_section_main about_us"  id="about_us_1">
        <div class="container container_big">
            <div class="row">
                <div class="col-md-12">
                    <div class="side_section_text big_padding">                        
                        <h3 class="default_section_heading raleway navy_blue">About <span class="font_200"> Us </span></h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            Dotcom Venture FZE (DV) is a technology company that focuses on developing online platforms, digital products and services across various sectors to provide value for individuals and businesses. Incorporated in the United Arab Emirates in 2018, DV leverages more then a decade experience of the company’s stakeholders in the development of digital platforms and products to form a consortium of online businesses.
                            <br><br>
                            DV believes that the success in online entrepreneurship is driven by the discovery of challenges in various niches and proffering hard to find solutions to the pain points. However, the procedure of discovering the viable market places a demand on data-driven analytics relating the Internet big data which reveals critical patterns and trends that indicate the viability of an online business prospect. This approach helps to channel efforts in the development of products and services for a hungry market rather than depending on instincts to develop products for a non-existing market.
                            <br><br>
                            Besides data-driven market research, DV has the competence, commitment and technical capacity to transform a business idea into reality by harnessing well-established and emerging technologies to deploy solutions. The team of expert programmers and frontend designers of Dotcom Ventures have extensive experience in developing Information Products, Mobile Apps, and Web Applications using various platforms. The goal of the company is to identify hungry markets and create in-demand services that provide immense value. This approach engenders loyalty and goodwill from the users which eventually leads to natural growth. 
                            <br><br>
                            DV believes that progression is driven by curiosity and is unafraid to venture into new sectors to embrace emerging technologies. Blockchain technology and the entire cryptocurrency ecosphere is proving to be a viable technology that proffers solutions which transcends the financial industry. DV looks forward to developing blockchain-derived products among other digital products and services.                            
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- half Section Ended





    <!-- Half Section -->
    <section class="half_section_main about_us"  id="about_us_2">        
        <div class="container container_big">
            <div class="row display_flex">
                
                <div class="col-md-6 valign_center">
                    <div class="image" style="background-image: url({{ asset('images/home/about-us_01.jpg') }});"></div>
                </div>

                <div class="col-md-6">
                    <div class="side_section_text big_padding">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        <i class="fa fa-comment-o pink_color" aria-hidden="true"></i> New Frontier
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        The advent and the proliferation of the digital economy have opened a new frontier in the technology industry. This concept has led to the popularity of virtual products and services that has a high potential for viral growth. Typical examples are the startups (Google, Amazon, Facebook, Netflix a.o.) that became global companies in just a couple of years. This sector is still in development and has a massive potential for creating more success stories. 
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="icons_div_small">
                                    <p class="small_heading_navy raleway font_400"> 
                                        <i class="fa fa-file-code-o pink_color" aria-hidden="true"></i> Innovation
                                    </p>
                                    <p class="default_text default_text_light open_sans">
                                        The digital economy plays a critical role locally and on a global scale; it fosters innovation, supports industries and creates employment. However, due to the increase in competition in this sector, there is a need for extensive market research and elements of innovation to have an edge over competition. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- half Section Ended -->









    <!-- Circle Js Skill Section -->
    <!-- Skill Heading Section -->
    <section class="combined_skills_section big_padding" >

        <section class="skill_section" >
            <div class="container">
                <div class="skill_section_inner">
                    <p class="default_text raleway text-center default_small_heading blue_color font_200">What we do</p>
                    <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                        Awesome
                        <span class="font_200">
                                Services
                            </span>
                    </h2>
                    <hr class="default_divider default_divider_blue default_divider_big">
                </div>
            </div>
        </section>

        <!-- /Skill Section -->
        <section class="circle_pie_section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="expertise_section_inner">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="expertise_block text-center margin_after_tab wow slideInLeft">
                                        <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span class="font_600 pink_color">01.</span> Web Design</h3>
                                        <p class="default_text text-center open_sans default_text_light">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="expertise_block text-center margin_after_tab wow fadeInUp">
                                        <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span class="font_600 pink_color">02.</span> Photography</h3>
                                        <p class="default_text text-center open_sans default_text_light">
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dosit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="expertise_block text-center wow slideInRight">
                                        <h3 class="small_heading_navy text-center raleway h3_pink font_400"><span class="font_600 pink_color">03.</span> Audio Creation</h3>
                                        <p class="default_text text-center open_sans default_text_light">
                                            Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!-- /Circel Js Skill Section -->
    <!-- Stats Section -->
    <section class="stats_section big_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-check white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 2000 </h3>
                        <p class="small_heading_navy white_color open_sans font_200">Completed Projects</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-flag white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 125 </h3>
                        <p class="small_heading_navy white_color open_sans font_200">Total Countries</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-globe white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 5000</h3>
                        <p class="small_heading_navy white_color open_sans font_200">Users Worldwide</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 margin_after_tab text-center">
                    <div class="stats_section_inner">
                        <i class="fa fa-gift white_color stats_section_icon text-center" aria-hidden="true"></i>
                        <h3 class="default_section_heading white_color raleway number-scroll"> 9 </h3>
                        <p class="small_heading_navy white_color open_sans font_200">Award Winners</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Stats Section -->
    <!-- Portfolio Section -->
    <section class="portfolio_section big_padding" style="padding-bottom: 0;" id="work">
        <div class="container">
            <p class="default_small_heading raleway text-center blue_color font_200">Creative Portfolio</p>
            <h2 class="default_section_heading text-center">
                <span class="font_200"> Our </span>
                Awesome
                <span class="font_200">Work</span>
            </h2>
            <hr class="default_divider default_divider_blue default_divider_big">
        </div>
        <div  class="cube_fullwidth_style_portfolio">
            <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter open_sans">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item open_sans button bg_before_pink">
                    All
                    <div class="cbp-filter-counter"></div>
                </div>
                <div data-filter=".branding" class="cbp-filter-item open_sans button bg_before_pink">
                    Branding
                    <div class="cbp-filter-counter"></div>
                </div>
                <div data-filter=".web-design" class="cbp-filter-item open_sans button bg_before_pink">
                    Web Design
                    <div class="cbp-filter-counter"></div>
                </div>
                <div data-filter=".photography" class="cbp-filter-item open_sans button bg_before_pink">
                    Photography
                    <div class="cbp-filter-counter"></div>
                </div>
            </div>
            <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">
                <div class="cbp-item web-design photography">
                    <a href="images/sample-bgs/projects-1.jpg" class="cbp-caption cbp-lightbox" data-title="Bolt UI<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/sample-bgs/projects-1.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">Bolt UI</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item branding photography">
                    <a href="images/sample-bgs/projects-2.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock<br>by Paul Flavius Nechita">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/sample-bgs/projects-2.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">World Clock</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item branding photography">
                    <a href="images/sample-bgs/projects-3.jpg" class="cbp-caption cbp-lightbox" data-title="WhereTO App<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/sample-bgs/projects-3.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">WhereTO App</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item branding graphic">
                    <a href="images/sample-bgs/projects-4.jpg" class="cbp-caption cbp-lightbox" data-title="Digital Menu<br>by Cosmin Capitanu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/sample-bgs/projects-4.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">Digital Menu</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="cbp-item web-design branding">
                    <a href="images/sample-bgs/projects-6.jpg" class="cbp-caption cbp-lightbox" data-title="iDevices<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/sample-bgs/projects-6.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">iDevices</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="cbp-item photography">
                    <a href="images/sample-bgs/projects-5.jpg" class="cbp-caption cbp-lightbox" data-title="Seemple* Music<br>by Tiberiu Neamu">
                        <div class="cbp-caption-defaultWrap">
                            <img src="images/sample-bgs/projects-5.jpg" alt="">
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <div class="cbp-l-caption-title">Seemple* Music</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- /Portfolio Section -->
    <section class="team_section big_padding text-center " id="team">
        <div class="container">
            <p class="raleway default_small_heading blue_color font_200">Let's Meet</p>
            <div class="col-xs-12">
                <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                    Creative
                    <span class="font_200">
                                Team
                            </span>
                </h2>
                <hr class="default_divider default_divider_blue default_divider_big">
                <div class="swiper-container team_slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="team_detail wow slideInLeft">
                                <div class="team_detail_inner">
                                    <img src="images/sample-bgs/team-1.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Chris Martin</h3>
                                <p class="default_text text-center open_sans">CEO - Company</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_detail wow fadeInUpBig">
                                <div class="team_detail_inner">
                                    <img src="images/sample-bgs/team-2.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Ellyse Perry</h3>
                                <p class="default_text text-center open_sans">Java Developer</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_detail wow fadeInDownBig">
                                <div class="team_detail_inner">
                                    <img src="images/sample-bgs/team-3.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Danny M</h3>
                                <p class="default_text text-center open_sans">Web Designer</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_detail wow slideInRight">
                                <div class="team_detail_inner">
                                    <img src="images/sample-bgs/team-4.jpg" alt="team_img">
                                </div>
                                <h3 class="small_heading_navy default_small_heading navy_blue text-center raleway font_400">Sarah Taylor</h3>
                                <p class="default_text text-center open_sans">Graphic Designer</p>
                                <ul class="team_members_list">
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style google_team">
                                            <i class="fa fa-linkedin team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style facebook_team">
                                            <i class="fa fa-facebook team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#." class="team_members_icons anchor_none_style twiiter_team">
                                            <i class="fa fa-twitter team_members_icon_inner" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Half Section -->
    <section class="half_section_main half_section_right bg_grey">
        <div class="half_section_picture hidden-sm hidden-xs"></div>
        <div class="container container_big">
            <div class="row">
                <div class="col-md-6">
                    <div class="side_section_text big_padding">
                        <p class="default_small_heading raleway blue_color font_200">Professional Expertise</p>
                        <h3 class="default_section_heading raleway navy_blue"><span class="font_200"> Our </span> Awesome <span class="font_200"> Skills </span></h3>
                        <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                        <p class="default_text_light default_text open_sans">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis.
                        </p>
                        <div class="progress_bar_outer_div">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy raleway default_small_heading navy_blue h3_pink font_400">Writing</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 84%;" role="progressbar" aria-valuenow="84" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">84%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy default_small_heading navy_blue raleway font_400">Development</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 98%;" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">98%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy default_small_heading navy_blue raleway font_400">Design</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 63%;" role="progressbar" aria-valuenow="63" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">63%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="progress_outer">
                                        <h3 class="small_heading_navy default_small_heading navy_blue raleway font_400">Video Editing</h3>
                                        <div class="progress progress_bar bg_white">
                                            <div class="progress-bar bg_pink" style="width: 78%;" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100">
                                                <span class="bg_navy">78%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- half Section Ended -->
    <!-- Pricing Table -->
    <section class="pricing_section big_padding bg_grey" id="pricing_table">
        <div class="container">
            <div class="pricing_table_section">
                <p class="default_text raleway text-center default_small_heading blue_color font_200">New Plans</p>
                <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                    Pricing
                    <span class="font_200">
                                Table
                            </span>
                </h2>
                <hr class="default_divider default_divider_blue default_divider_big">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="pricing_table_column wow slideInLeft">
                            <h3 class="small_heading_navy raleway">Basic</h3>
                            <div class="price">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2 class="open_sans font_400 pink_color"><span class="dollar raleway">$</span>19<br><span class="month raleway">Month</span></h2>
                                    </div>
                                    <div class="col-xs-8">
                                        <p>It has survived not only five centuries, but also the leap into electronic.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="packages">
                                <li><i class="fa fa-check" aria-hidden="true"></i>Full access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Bandwidth</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Powerful Admin Panel</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Email Accounts</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>8 Free Forks Every Months</li>
                            </ul>
                            <div class="pricing_button">
                                <a href="#." class="bg_pink button button_default_style open_sans bg_before_navy">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pricing_table_column blue_price wow fadeInUpBig">
                            <h3 class="small_heading_navy raleway white_color">Advance</h3>
                            <div class="price">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2 class="open_sans font_400 white_color"><span class="dollar raleway">$</span>49<br><span class="month raleway">Month</span></h2>
                                    </div>
                                    <div class="col-xs-8">
                                        <p>It has survived not only five centuries, but also the leap into electronic.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="packages">
                                <li><i class="fa fa-check" aria-hidden="true"></i>Full access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Bandwidth</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Powerful Admin Panel</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Email Accounts</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>8 Free Forks Every Months</li>
                            </ul>
                            <div class="pricing_button">
                                <a href="#." class="bg_white button button_default_style open_sans bg_before_pink navy_blue">Buy Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pricing_table_column wow slideInRight">
                            <h3 class="small_heading_navy raleway">Premium</h3>
                            <div class="price">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h2 class="open_sans font_400 pink_color"><span class="dollar raleway">$</span>99<br><span class="month raleway">Month</span></h2>
                                    </div>
                                    <div class="col-xs-8">
                                        <p>It has survived not only five centuries, but also the leap into electronic.</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="packages">
                                <li><i class="fa fa-check" aria-hidden="true"></i>Full access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Unlimited Bandwidth</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Powerful Admin Panel</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>Email Accounts</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i>8 Free Forks Every Months</li>
                            </ul>
                            <div class="pricing_button">
                                <a href="#." class="bg_pink button button_default_style open_sans bg_before_navy">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Pricing Table -->
    <!-- Customer Review Slider -->
    <section class="customer_feedback_section big_padding text-center bg_dblue">
        <div class="container">
            <div class="feedaback_inner">
                <img src="images/cutomer_quote.png" alt="cutomer_quote" class="quote_pic">
                <h2 class="default_section_heading text-center white_color">
                            <span class="font_200">
                                What
                            </span>
                    People
                    <span class="font_200">
                                Say ?
                            </span>
                </h2>
                <hr class="default_divider default_divider_white default_divider_big">
                <div class="swiper-container customer_feedback_slider white_pagination">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <p class="customer_feedback_text white_color open_sans default_text font_200">

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere
                                convallis.
                            </p>
                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">John Doe - Google CEO</p>
                        </div>
                        <div class="swiper-slide">
                            <p class="customer_feedback_text white_color open_sans default_text font_200">

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere
                                convallis.
                            </p>
                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">John Doe - Google CEO</p>
                        </div>
                        <div class="swiper-slide">
                            <p class="customer_feedback_text white_color open_sans default_text font_200">

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere
                                convallis.
                            </p>
                            <p class="customer_feedback_name white_color open_sans default_small_heading font_600">John Doe - Google CEO</p>
                        </div>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next fa fa-angle-right hidden-sm hidden-xs"></div>
                    <div class="swiper-button-prev fa fa-angle-left hidden-sm hidden-xs"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Customer Review Slider -->
    <!-- Blog Section -->
    <section class="blog_section big_padding" id="blog">
        <div class="container">
            <p class="default_text raleway text-center default_small_heading blue_color font_200">Read the News</p>
            <h2 class="default_section_heading text-center">
                            <span class="font_200">
                                Our
                            </span>
                Latest
                <span class="font_200">
                                Blogs
                            </span>
            </h2>
            <hr class="default_divider default_divider_blue default_divider_big">
            <div class="blog_slider_main swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="blog_slide wow slideInLeft">
                            <a href="blog.html" class="anchor_style_default">
                                <h3 class="small_heading_navy raleway center_on_mobile font_400">Pellentesque euismod</h3>
                            </a>
                            <p class="default_text default_text_light open_sans center_on_mobile">June 18, 2017</p>
                            <div class="blog_img">
                                <img src="images/sample-bgs/blog_post_1.jpg" alt="blog_img">
                            </div>
                            <p class="default_text open_sans center_on_mobile">
                                Aenean faucibus nulla ex, in rutrum dui blandit at. Pellentesque bibendum odio vitae gravida consequat aliquam sem morbi dor sit imet sen
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="blog_slide wow fadeInDownBig">
                            <a href="blog.html" class="anchor_style_default">
                                <h3 class="small_heading_navy raleway center_on_mobile font_400">Proin condimentum</h3>
                            </a>
                            <p class="default_text default_text_light open_sans center_on_mobile">June 18, 2017</p>
                            <div class="blog_img">
                                <img src="images/sample-bgs/blog_post_2.jpg" alt="blog_img">
                            </div>
                            <p class="default_text open_sans center_on_mobile">
                                Cras eget ligula dolor aenean ac velit ultricies, i pretium tincidunt bibendum turpis maximus posuere get
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="blog_slide wow slideInRight">
                            <a href="blog.html" class="anchor_style_default">
                                <h3 class="small_heading_navy raleway center_on_mobile font_400">Donec dapibus</h3>
                            </a>
                            <p class="default_text default_text_light open_sans center_on_mobile">June 18, 2017</p>
                            <div class="blog_img">
                                <img src="images/sample-bgs/blog_post_3.jpg" alt="blog_img">
                            </div>
                            <p class="default_text open_sans center_on_mobile">
                                Cras eget ligula dolor aenean ac velit ultricies, accumsan nisl cursus, aliquam sem morbi pretium tincidunt bibendum turpis maximus posuere get
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
    <!-- /Blog Section -->
    <!-- Google Map Section -->
    <section id="map"></section>
    <!-- /Google Map Section -->
    <!-- Contact Form Section -->
    <section class="contact_form_section" id="contact-form">
        <div class="container">
            <div class="row">
                <div class="contact_form_inner big_padding clearfix">
                    <div class="col-md-6 wow slideInLeft">
                        <div class="contact_form_detail  center_on_mobile">
                            <p class="default_small_heading raleway blue_color font_200">Locate Us</p>
                            <h3 class="default_section_heading raleway navy_blue"> <span class="font_200">Let's Get In</span> Touch</h3>
                            <hr class="default_divider default_divider_blue " style="margin-left: 0;">
                            <p class="default_text_light default_text open_sans">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisi enim, vulputate at justo tristique, tempor sagittis dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec sit amet ligula consequat urna posuere convallis.
                            </p>
                            <div class="row">
                                <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-map-marker blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <p class="default_text form_text open_sans default_text_light">
                                            58 alley, Mertle St, Rd 12, Apt 2C
                                        </p>
                                        <p class="default_text form_text open_sans default_text_light">
                                            Seattle, Washington
                                        </p>
                                    </div>
                                </div>
                                <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-phone blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <p class="default_text form_text open_sans default_text_light">
                                            +92 1440 59 58
                                        </p>
                                        <p class="default_text form_text open_sans default_text_light">
                                            +92 1440 59 58
                                        </p>
                                    </div>
                                </div>
                                <div class="contact_form_extra_inner clearfix center_on_mobile">
                                    <div class="col-md-1 col-sm-12 form_padding_left_0">
                                        <i class="fa fa-globe blue_color text-center form_icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-11 col-sm-12">
                                        <a href="#." class="anchor_style_default">
                                            <p class="default_text form_text open_sans default_text_light">
                                                email@website.com
                                            </p>
                                        </a>
                                        <a href="#." class="anchor_style_default">
                                            <p class="default_text form_text open_sans default_text_light">
                                                www.website.com
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 wow slideInRight">
                        <form onsubmit="return false" class="form_class">
                            <div class="row">
                                <div class="mew_form clearfix">
                                    <div class="col-sm-12" id="result"></div>
                                    <div class="col-sm-6">
                                        <input placeholder="Your Name" class="form_inputs" id="name" name="name" required="required">
                                    </div>
                                    <div class="col-sm-6">
                                        <input placeholder="Email" class="form_inputs" id="email" name="email" required="required">
                                    </div>
                                    <div class="col-sm-12">
                                        <textarea placeholder="Your Message" class="form_inputs form_textarea" id="message" name="message" required="required"></textarea>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="button_div  center_on_mobile">
                                            <input id="submit_handle" type="submit" style="display: none" />
                                            <a href="javascript:" id="submit_btn" class="bg_pink button button_default_style open_sans bg_before_navy"> <i class="fa fa-envelope" aria-hidden="true"></i> Send Message</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Contact Form  Section -->
    <!-- Footer Section -->
    <footer class="footer_section big_padding bg_navy">
        <div class="container">
            <div class="footer_detail">
                <ul class="footer_links text-center">
                    <li>
                        <a class="anchor_style_default facebook wow zoomInDown" href="#.">
                            <i class="text-center fa fa-facebook "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default twitter wow zoomInUp" href="#.">
                            <i class="text-center fa fa-twitter "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default g_plus wow zoomInDown" href="#.">
                            <i class="text-center fa fa-google-plus "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default linkedin wow zoomInUp" href="#.">
                            <i class="text-center fa fa-linkedin "></i>
                        </a>
                    </li>
                    <li>
                        <a class="anchor_style_default  pinterest wow zoomInDown" href="#.">
                            <i class="text-center fa fa-pinterest"></i>
                        </a>
                    </li>
                </ul>
                <p class="text-center default_text open_sans white_color">&copy; 2018 Themes Industry,  All rights reserved. </p>
            </div>
        </div>
    </footer>
    <!-- /Footer Section -->
</section>    

 --}}