<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags For Seo + Page Optimization -->
    <meta charset="utf-8">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    
    <!-- Insert Favicon Here -->
    <link rel="icon" type="image/png" href="images/logos/logo-icon-sm.png?v=1.0"/>
    <!-- Page Title(Name)-->
    <title>Dotcom Ventures FZE</title>


    <meta property="og:type" content="article">
    <meta property="og:url" content="https://dotcomv.com">
    <meta property="og:image" content="{{ asset('images/logos/logo-icon.png') }}">
    <meta property="og:title" content="Dotcom Ventures FZE">
    <meta property="og:description" content="WE DREAM IT. WE BUILD IT.">

    <meta name="description" content="WE DREAM IT. WE BUILD IT.">    
    <meta name="author" content="Dotcom Ventures FZE">    




    <script src="https://www.google.com/recaptcha/api.js?render=6LcJZdEUAAAAAL-5ZAg1UmrvAhtf9fR4KPPZ0kdh"></script>
    <script>

    </script>    




    
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Stylesheets -->
    <link href="{{ Util::assetUrl('css/preloader.css') }}" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    {{-- aos --}}
    {{-- <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">    
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> --}}
    <link href="{{ Util::assetUrl('css/aos.css') }}" rel="stylesheet">
    <script src="{{ Util::assetUrl('js/aos.js') }}"></script>


    {{-- <link href="plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
    <link href="plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
    <link href="plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES --> --}}

    <link href="{{ Util::assetUrl('css/style.css') }}" rel="stylesheet">
    
    {{-- old css for menu--}}
    <link href="{{ Util::assetUrl('css/style-old.css') }}" rel="stylesheet">

    <link href="{{ Util::assetUrl('css/responsive.css') }}" rel="stylesheet">
    
    {{-- <link href="css/master-app.css" rel="stylesheet"> --}}
    <link href="{{ Util::assetUrl('css/master-app.css') }}" rel="stylesheet">


    {{-- <link href="css/wave.css" rel="stylesheet"> --}}
    {{-- <link href="css/clouds.css" rel="stylesheet"> --}}
    {{-- <link href="css/multi-bg.css" rel="stylesheet"> --}}




</head>

<script type="text/javascript">
    @if (session('error') !== null)
       var success = 'true';
    @else
        var success = 'false';
    @endif
</script>

    


<body data-spy="scroll" data-target=".navbar" data-offset="83" id="body" class="particles_special_id">

    <div class="page-wrapper">
         
        <!-- Preloader -->
        <div class="preloader">
            <img src="{{ asset('images/logos/logo-sm.png') }}" alt="logo"> 
            {{-- <br> --}}
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
        </div>
         




    <?php /*
    <!-- Main Header / Style Two-->
    <header class="main-header header-style-two">
        
        <!--Header-Upper-->
        <div class="header-upper">
            <div class="outer-box clearfix">
                
                <div class="logo-outer">
                    <div class="logo"><a href="index.html"><img src="images/logos/logo-sm.png" alt="" title=""></a></div>
                </div>
                
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li><span class="scrollTo" data-scrollToSelector="#home">Home</span></li>
                                <li><span class="scrollTo" data-scrollToSelector="#ethos-values">Ethos & Values</span></li>
                                <li><span class="scrollTo" data-scrollToSelector="#key-business-areas">Key Business Areas</span></li>
                                <li><span class="scrollTo" data-scrollToSelector="#projects">Projects</span></li>
                                <li><span class="scrollTo" data-scrollToSelector="#team">Our Team</span></li>
                                <li><span class="scrollTo" data-scrollToSelector="#contact-us">Contact</span></li>
                            </ul>
                        </div>
                    </nav>
                    
                    <!--Nav Toggler-->
                    {{-- <div class="nav-toggler"><span class="flaticon-menu-1"></span></div> --}}
                    
                </div>
                
            </div>
        </div>
        <!--End Header Upper-->
    
    </header>
    <!--End Main Header -->
    */ ?>





    <!-- Navbar Section -->
    <nav class="navbar navbar-fixed-top ">


        {{-- <div class="cookie_warning">
            <p>
                To continue, you must agree to the <b>www.dotcomv.com<span class="symbol">&copy;</span></b>
                <a data-target="#tc_modal" data-toggle="modal" class="pp_tc_link">Terms and Conditions</a>
                and <a data-target="#pp_modal" data-toggle="modal" class="pp_tc_link">Privacy Policy</a>.
                This Website uses cookies in order to offer you the most relevant information.
                Please read our Cookies Policy for more information in the Privacy Policy <a data-target="#pp_modal"
                    data-toggle="modal" data-anchor="#cookies" class="pp_tc_link scroll_toanchor">Here</a>.
                If you want to proceed to the Website, please accept the Website policies by clicking on
                "Yes, I accept" button below.
                If you do not agree, then you may leave by closing this webpage.
            </p>
            <button class="btn btn-default i_accept">Yes, I accept</button>
        </div> --}}



        <div class="container-fluid">
            <!--second nav button -->
            <div id="menu_bars" class="right menu_bars">
                <span class="t1"></span>
                <span class="t2"></span>
                <span class="t3"></span>
            </div>
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ asset('') }}"><img src="{{ asset('images/logos/logo-sm.png') }}" alt="logo"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse  ">
                    <ul class="nav navbar-nav navbar-right">
                        {{-- <li class="active"><a href="#home" class="scroll">Home</a></li> --}}
                        {{-- <li><a href="#about_us" class="scroll">About Us</a></li> --}}

                        <li><a href="#home" class="scroll">Home</a></li>
                        <li><a href="#ethos-values" class="scroll">Ethos & Values</a></li>
                        {{-- <li><a href="#key-business-areas" class="scroll">Key Business Areas</a></li> --}}
                        <li><a href="#projects" class="scroll">Projects</a></li>
                        <li><a href="#team" class="scroll">Our Team</a></li>
                        <li><a href="#investment-opportunity" class="scroll">Investment Opportunity</a></li>
                        <li><a href="#contact-us" class="scroll">Contact</a></li>
                        
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <div class="sidebar_menu">
                <nav class="pushmenu pushmenu-right">
                    <a class="push-logo"><img src="images/logos/logo-sm.png" alt="logo"></a>
                    <ul class="push_nav centered">
                        <li class="clearfix"><a href="#home" class="scroll"><span>0.1</span>Home</a></li>
                        <li class="clearfix"><a href="#ethos-values" class="scroll"><span>0.2</span>Ethos & Values</a></li>
                        {{-- <li class="clearfix"><a href="#key-business-areas" class="scroll"><span>0.3</span>Key Business Areas</a></li> --}}
                        <li class="clearfix"><a href="#projects" class="scroll"><span>0.4</span>Projects</a></li>
                        <li class="clearfix"><a href="#team" class="scroll"><span>0.5</span>Our Team</a></li>
                        <li class="clearfix"><a href="#investment-opportunity" class="scroll"><span>0.6</span>Investment Opportunity</a></li>
                        <li class="clearfix"><a href="#contact-us" class="scroll"><span>0.7</span>Contact</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    {{-- <ul class="social_icon black top25 bottom20 list-inline">
                        <li><a href="#" class="navy_blue facebook"><i class="fa fa-fw fa-facebook"></i></a></li>
                        <li><a href="#" class="navy_blue twitter"><i class="fa fa-fw fa-twitter"></i></a></li>
                        <li><a href="#" class="navy_blue pinterest"><i class="fa fa-fw fa fa-pinterest"></i></a></li>
                        <li><a href="#" class="navy_blue linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul> --}}
                </nav>
            </div>
        </div>
    </nav>
    <!-- /Navbar Section -->





































































































    <?php /*
    <!--Main Slider-->
    <section class="main-slider" id="home">
    	
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_two_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_two" data-version="5.4.1">
                <ul>

                    <?php
                        $slides = [
                            ['img'=>'images/home/main-images/img11.jpg'],
                            // ['img'=>'images/home/main-images/img13.jpg'],
                            // ['img'=>'images/home/main-images/img5.jpg'],
                        ]
                    ?>
                    
                    @foreach($slides as $slideKey => $slide)
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" 
                    data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-{{ $slideKey }}" data-saveperformance="off" 
                    data-slotamount="default" data-thumb="{{ Util::assetUrl($slide['img']) }}" data-title="Slide Title" data-transition="parallaxvertical">

                        <div class="slide-backdrop"></div>

                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" 
                        data-no-retina="" 
                        src="{{ Util::assetUrl($slide['img']) }}"> 
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['680','680','600','320']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['-40','-70','-110','-80']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1000,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        style="z-index: 7; white-space: nowrap;text-transform:left;">
                                <div class="subtitle">Dotcom Ventures</div>
                                {{-- <div class="subtitle">&nbsp;</div> --}}
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['680','680','600','320']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['50','20','-20','10']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        style="z-index: 7; white-space: nowrap;text-transform:left;">
                            <div class="big-title">We dream it<br>we build it</div>
                        </div>
                        
                        <div class="tp-caption tp-resizeme" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['680','680','600','320']"
                        data-whitespace="normal"
                        data-hoffset="['15','15','15','15']"
                        data-voffset="['160','130','70','100']"
                        data-x="['left','left','left','left']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1000,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        style="z-index: 7; white-space: nowrap;text-transform:left;">
                            <div class="link-box">
                                finance, evaluate, develop, and invest in modern-age emerging technologies, products, services and businesses.                            
                            </div>
                        </div>
                        
                    </li>
                    @endforeach
                    
                    
                </ul>
            </div>
        </div>

    </section>
    <!--End Main Slider-->
    */ ?>



    

    <section class="home" id="home">


        {{-- <div class="bg-image" style="background-image: url({{ Util::assetUrl('images/home/main-images/bulb.png') }});"></div> --}}
        <div class="bg-image1 rotate" 
        style="background-image: url({{ Util::assetUrl('images/home/main-images/bg-img6.png') }});background-size: contain;"></div>

        <div class="bg-image1" style="background-image: url({{ Util::assetUrl('images/home/main-images/bg-img1.png') }});"></div>

        <div class="backdrop-image"></div>

        {{-- <canvas id="home_particles_bg" class="particles_bg" style="position: absolute; width: 100%; height: 100%;"></canvas> --}}
        
        <div class="text">

            <div class="outerCircle"></div>
            {{-- <div class="innerCircle"></div> --}}
            <div class="text-content wow zoomIn">
                <h1 style="margin-bottom:10px;">Dotcom Ventures</h1>
                <h3>WE DREAM IT. WE BUILD IT.</h3>
                <div class="social-media-links">
                    <?php
                    $sdvs = [
                        [ 'name'=>'facebook', 'icon'=>"fa fa-facebook", 'url'=>'https://www.facebook.com/DotcomV/',],
                        [ 'name'=>'instagram', 'icon'=>"fab fa-instagram", 'url'=>'https://www.instagram.com/dotcomv_/',],
                        [ 'name'=>'linkedin', 'icon'=>"fa fa-linkedin", 'url'=>'https://www.linkedin.com/company/dotcomv',],
                        [ 'name'=>'twitter', 'icon'=>"fa fa-twitter", 'url'=>'https://twitter.com/DotcomV_',],
                    ];
                    ?>
                    @foreach ($sdvs as $sdv)                                        
                        <a target="_blank" href=" {{ $sdv['url'] }} ">
                            <i class=" {{ $sdv['icon'] }} "></i>
                        </a> &ensp; 
                    @endforeach      
                </div>                
            </div>            
            {{-- <h5>Dotcom Ventures FZE finance, evaluate, develop, and invest in modern-age emerging technologies, products, services and businesses.</h5> --}}

                        
        </div>
            



    </section>



    <!--About Section-->
    <section class="about-section" id="about-us" style="background-image:url({{ Util::assetUrl('images/home/about-us/bg-img2.png') }})">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column col-md-5 col-sm-12 col-xs-12" data-aos="fade-up">
                    <div class="inner">
                        <div class="title-box">
                            {{-- <h3 class="subtitle">the axeon</h3> --}}
                            <h2>Our Goals are Clear <br>and Focused.</h2>
                            {{-- <h4>Dolore magna aliq edamco laboris nisut aliquip.</h4> --}}
                        </div>
                        <div class="text-content">
                            Headquartered in Dubai, United Arab Emirates, Dotcom Ventures FZE is focused on the ideation, planning, 
                            creation and growth of businesses in all tech sectors, including but not limited to electrical engineering and lighting, 
                            information technology, fintech, blockchain and tokenization, real estate, hospitality, ecommerce and advertising.                             
                            <br><br>                            
                            Our unique combination of deep industry knowledge, practical experience, intellectual property, regulatory compliance, 
                            manufacturing operations, and finance make our business stand out from the rest.
                        </div>
                        
                        <!--Facts-->
                        <div class="facts">
                            {{-- <ul class="facts-list clearfix">
                                <li><div class="count-box count"><span class="count-text" data-speed="1000" data-stop="8">0</span>+</div><div class="fact-title">Our Team</div></li>
                                <li><div class="count-box count"><span class="count-text" data-speed="1000" data-stop="5">0</span>+</div><div class="fact-title">Projects</div></li>
                            </ul> --}}
                            
                            <div class="border-themed" style="border: 1px solid #000; width: 100px;"></div>

                        </div>
                    </div>
                </div>
                
                <!--Images Column-->
                <div class="images-column col-md-7 col-sm-12 col-xs-12" data-aos="fade-up">
                    <div class="inner">
                        <div class="row clearfix">
                            <!--Image Col-->
                            <div class="image-col col-md-12 col-sm-12 col-xs-12 wow_zoomIn">
                                <div class="bg-image" 
                                style="background-image:url({{ Util::assetUrl('images/home/about-us/img7.png') }})"></div>
                            </div>
                            <!--Image Col-->
                            {{-- <div class="image-col col-md-6 col-sm-6 col-xs-12 wow fadeInLeft">
                                <div class="bg-image" 
                                style="background-image:url({{ Util::assetUrl('images/home/about-us/img6.png') }})"></div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="row clearfix">
            	<!--Text Column-->
                <div class="text-column col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                    <a href="#team" class="scroll our-team-text">
                    <div class="inner-box">
                        <h3 class="text-right" style="margin-bottom:10px;">
                            <span>Our Team</span>
                        </h3>
                        <div class="text text-right">
                            Dotcom Ventures is led by a diverse group of professionals with decades of experience in finance, IT, electronics, science, industry, and intellectual property.                            
                        </div>
                    </div>
                    </a>
                </div>
                <!--Text Column-->
                <div class="text-column col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                    <a href="#projects" class="scroll out-portfolio-text">
                    <div class="inner-box">
                        <h3 style="margin-bottom:10px;">                            
                            <span>Our Portfolio</span>
                        </h3>
                        <div class="text text-left">
                            We seek to build partnerships with a wide variety of businesses and venture capital firms to create value growth for our companies.
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            
            <!--Social Links-->
            {{-- <div class="social-links">
            	<ul class="clearfix">                	
                    <li><a href="#">Instagram</a></li>
                    <li><a href="#">Facebook</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Linkedin</a></li>
                </ul>
            </div> --}}
                        
        </div>
    </section>
    








    
    
    <!--key business areas-->
    <section class="featured-content-section key-business-areas" id="key-business-areas">
    	<!--Title Box-->
        <div class="title-box">
            <div class="auto-container">
                <div class="inner-box">
                    <h3 class="title">Key Business Areas</h3>
                </div>
            </div>
        </div>

        <?php 
            $kbas = [
                [
                    'title'=>'Electrical Manufacturing and <br> Energy Efficient Lighting', 
                    'img'=>'images/home/key-business-areas/img10.png', 
                    'class'=>'wow_zoomIn', 
                    'style'=>'', 
                    'text'=>'We offer energy efficient lighting products and solutions, helping the world to conserve energy by reducing power consumption and lowering electricity bills.',
                ],

                [
                    'title'=>'Fintech', 
                    'img'=>'images/home/key-business-areas/img15.png', 
                    'class'=>'wow_zoomIn', 
                    'style'=>'', 
                    'text'=>'We deal in cutting edge fintech businesses and solutions, including but not limited to fintech, decentralized finance (DEFI), cryptocurrencies, and tokenized assets.',
                ],

                [
                    'title'=>'Emerging Technologies', 
                    'img'=>'images/home/key-business-areas/img17.png', 
                    'class'=>'wow_zoomIn', 
                    'style'=>'background-size: cover;', 
                    'text'=>'We provide end-to-end services for the discovery, computing, deployment, and administering of Big Data and AI based solutions for the benefit of businesses and corporate clients.',
                ],

                [
                    'title'=>'SaaS', 
                    'img'=>'images/home/key-business-areas/img14.png', 
                    'class'=>'wow_zoomIn', 
                    // 'style'=>'background-size: cover;', 
                    'style'=>'', 
                    'text'=>'We offer in-demand Software as a Service (SaaS) for carefully selected niches after comprehensive market research.',
                ],

                [
                    'title'=>'E-Learning', 
                    'img'=>'images/home/key-business-areas/img13.png', 
                    'class'=>'wow_zoomIn', 
                    'style'=>'', 
                    'text'=>'Dotcom Ventures will provide e-learning in the form of electronic books and a multimedia training system to serve visual learners.',
                ],

            ];
        ?>

        @foreach ($kbas as $kbasKey => $kba )
        <?php 
            $fadeIn = ($kbasKey%2)?'fadeInLeft':'fadeInRight';
            $pull = ($kbasKey%2)?'pull-left':'pull-right';
            $pullImg = ($kbasKey%2)?'pull-right':'pull-left';

            if($kbasKey%2) {
                $aos_a = 'data-aos="fade-right"';
                $aos_b = 'data-aos="fade-left"';
            } else  {
                $aos_a = 'data-aos="fade-left"';
                $aos_b = 'data-aos="fade-right"';
            }
        ?>
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Content Column-->
                <div class="content-column {{ $pull }} col-md-6 col-sm-12 col-xs-12" {!! $aos_a !!}>
                	<div class="inner">
                        <h3>{!! $kba['title'] !!}</h3>
                        <div class="text"> {{ $kba['text'] }} </div>
                    </div>
                </div>
                <!--Image Column-->
                <div class="image-column {{ $pullImg }} col-md-6 col-sm-12 col-xs-12 {{ $kba['class'] }}" {!! $aos_b !!}>
                <div class="image-bg" style="background-image: url( {{  Util::assetUrl($kba['img'])  }} ); {{ $kba['style'] }}"></div>
                </div>
            </div>
        </div>
        @endforeach                
    </section>
    
    


        

    
    
    
    
        
    <!--Services Section-->
    <section class="services-section ethos-values" id="ethos-values">
    	<div class="auto-container">
        	<!--Heading-->
        	<div class="title-box" data-aos="fade-up">
                {{-- <h2 class="subtitle"></h2> --}}
                <h2>Our Ethos and Values</h2>
                {{-- <h4>Dotcom Ventures has established value propositions that serve as the corporate philosophy of the company, fostering the internal operations, product and service development, as well as present and future corporate alliances.</h4> --}}
                <div class="text">
                    Dotcom Ventures has strong value propositions that serve as the corporate philosophy of our company, 
                    fueling our internal operations, product and service development, and present and future corporate alliances.
                </div>
            </div>
            
            <div class="row clearfix">
                <?php
                    $ethosValues = [
                        ['title'=>'Excellence', 'icon'=>'fas fa-award', 
                            'text'=>'At Dotcom Ventures, we pay attention to every detail, ensuring that a quality final product is delivered that far surpasses the status quo.', ],
                        ['title'=>'Research & Development', 'icon'=>'fas fa-book', 
                            'text'=>'We don’t just think and create. We do thorough research, and analysis, plus industry and competitor overviews, to ensure that our projects are self-sustainable and have maximum growth potential.', ],
                        ['title'=>'Partnerships', 'icon'=>'far fa-handshake', 
                            'text'=>'As a business partner, we go the extra mile for you. We always put your best interests first—regardless of project size or budget—so that we can help you succeed.', ],
                        ['title'=>'Teamwork', 'icon'=>'fas fa-users', 
                            'text'=>'We think and create together, combining our strengths to give our customers a superior, more versatile solution. Our commitment to collaboration means that each team member’s voice is heard, valued, and respected.', ],
                        ['title'=>'Integrity & Professionalism', 'icon'=>'fas fa-user-tie', 
                            'text'=>'We demonstrate our integrity by keeping our promises to you. We maintain a spirit of transparent honesty and professionalism in everything we do.', ],
                        ['title'=>'Creativity & Innovation', 'icon'=>'far fa-lightbulb', 
                            'text'=>'To us, creativity means problem solving, and we believe in solving the impossible. We never settle for the first, easiest, or quickest solution. We carefully analyze, research, and create a custom solution that not only solves your immediate problem, but also helps you to stand out among your peers.', ],
                    ];    
                ?>
                
                @foreach ($ethosValues as $ev)                    
                <!--Service Style One-->
                <div class="service-style-one col-md-4 col-sm-6 col-xs-12"  data-aos="fade-up">
                	<div class="inner-box wow_zoomIn">
                    	<div class="icon-box">
                            <i class="{{$ev['icon']}}"></i>
                        </div>
                        {{-- <h3><a href="services.html">Web design</a></h3> --}}
                        <h3>{{$ev['title']}}</h3>
                        <div class="text">{{$ev['text']}}</div>
                        {{-- <div class="more-link"><a href="services.html">read more</a></div> --}}
                    </div>
                </div>
                @endforeach


            </div>
            
        </div>
    </section>
    
    













    

    <!--Featured Content Section Two-->
    <section class="featured-content-section-two hot-developments" id="projects">
    	<div class="outer-container clearfix">
            <!--Content Column-->
            <div class="content-column">
                <div class="inner" data-aos="fade-up">
                    <h2>Hot Developments</h2>
                    {{-- <h3>Tangible value in the form of digital products</h3> --}}
                    <div class="text">We have successfully created several companies from scratch to the startup stage across wide-ranging industries.</div>
                    
                    <!--Facts-->
                    <div class="facts">
                        <ul class="facts-list clearfix">
                            <li><div class="fact-inner"><div class="icon"><span class="flaticon-sticker"></span></div><div class="count-box count"><span class="count-text" data-speed="1500" data-stop="9">0</span>+</div><div class="fact-title">experience years</div></div></li>
                            <li><div class="fact-inner"><div class="icon"><span class="flaticon-earth"></span></div><div class="count-box count"><span class="count-text" data-speed="1500" data-stop="5">0</span>+</div><div class="fact-title">projects</div></div></li>
                            {{-- <li><div class="fact-inner"><div class="icon"><span class="flaticon-handshake-1"></span></div><div class="count-box count"><span class="count-text" data-speed="1500" data-stop="38">0</span>+</div><div class="fact-title">satisfied clients</div></div></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <!--Image Column-->
            <div class="image-column" style="background-image:url({{ Util::assetUrl('images/home/main-images/img9.jpg') }});">
                <figure class="image-box"><img src=" {{ Util::assetUrl('images/home/main-images/img9.jpg') }} " alt=""></figure>
            </div>
        </div>
    </section>
    
    
    <!--Sponsors Section-->
    <section class="sponsors-section hot-developments">
    	<!--Title Box-->
        <div class="title-box">
            <div class="auto-container">
                <div class="inner-box">
                    {{-- <h3 class="title">some of our potential projects ...</h3> --}}
                </div>
            </div>
        </div>
        
    	<div class="auto-container">
            <div class="row projects">            
                <?php
                $socialBal = [
                    [ 'type'=>'instagram', 'icon'=>'instagram', 'url'=>'https://www.instagram.com/buyanylight/'],
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/BuyAnyLight/'],
                    [ 'type'=>'linkedin', 'icon'=>'linkedin', 'url'=>'https://www.linkedin.com/company/buyanylight/'],
                    [ 'type'=>'twitter', 'icon'=>'twitter', 'url'=>'https://twitter.com/BuyAnyLight'],
                    [ 'type'=>'youtube', 'icon'=>'youtube', 'url'=>'https://www.youtube.com/c/BuyAnyLight'],
                    [ 'type'=>'telegram', 'icon'=>'telegram', 'url'=>'https://t.me/buyanylight'],
                ];

                $socialBillboardly = [
                    [ 'type'=>'instagram', 'icon'=>'instagram', 'url'=>'https://www.instagram.com/billboard.ly/'],
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/billboard.ly/'],
                    [ 'type'=>'linkedin', 'icon'=>'linkedin', 'url'=>'https://www.linkedin.com/company/billboard-ly/'],
                    [ 'type'=>'twitter', 'icon'=>'twitter', 'url'=>'https://twitter.com/billboard_ly'],
                ];
    

                $dps = [
                    ['title'=>'BuyAnyLight',            'img_class'=>'buyanylight',  'url'=>'www.buyanylight.com',         'image'=>'images/hot-developments/bal.png', 'style'=>"", 'social'=>$socialBal],
                    ['title'=>'Billboardly',              'img_class'=>'billboardly',  'url'=>'www.billboard.ly',            'image'=>'images/hot-developments/billboardly.png', 'style'=>"", 'social'=>$socialBillboardly],
                ];
                ?>
                @foreach($dps as $dp)
                <div class="col-sm-6 wow_zoomIn" data-aos="fade-up">
                    <div class="project">
                        <a href="http://{{ $dp['url'] }}" class="anchor_style_default"  target="_blank">
                            <h3 class="small_heading_navy raleway center_on_mobile font_400">{{ $dp['title'] }}</h3>
                        </a>
                        <p class="default_text default_text_light open_sans center_on_mobile">
                            <a href="http://{{ $dp['url'] }}" target="_blank">{{ $dp['url'] }}</a>
                        </p>
                        <a href="http://{{ $dp['url'] }}" target="_blank" class="img_a">
                            <div class="blog_img">
                                <img style="{{ $dp['style'] }}" src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                            </div>
                        </a>
                    </div>
                    <div class="social">
                        <ul class="footer_links text-center">
                            @foreach ($dp['social'] as $soc)
                            <li>
                                <a class="anchor_style_default {{ $soc['type'] }} wow_zoomInDown" href="{{ $soc['url'] }}" target="_blank">
                                    <i style=""; class="text-center fab fa-{{ $soc['icon'] }}"></i>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>



            <div class="row">
                <div class="content-column">
                    <div class="inner" data-aos="fade-up">
                        <h2>Successful Spin-Offs</h2>
                        {{-- <h3>Tangible value in the form of digital products</h3> --}}
                        <div class="text">
                            Here’s a look at what we have developed with passion and joy to date. 
                            At present our amazing teams are continuing their work on a variety of ventures, 
                            adding further value to the market and resulting in outstanding products and better lives for all!                            
                        </div>
                    </div>
                </div>                
            </div>

            <div class="row projects">
                <?php
                $socialAlmani = [
                    [ 'type'=>'instagram', 'icon'=>'instagram', 'url'=>'https://www.instagram.com/almani_lighting'],
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/almani.lighting'],
                    [ 'type'=>'linkedin', 'icon'=>'linkedin', 'url'=>'https://www.linkedin.com/company/almani-lighting'],
                    [ 'type'=>'twitter', 'icon'=>'twitter', 'url'=>'https://twitter.com/Almani_Lighting'],
                ];

                $socialZoomDNA = [
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/zoomdna'],
                ];

                $socialDubaiConsult = [
                    [ 'type'=>'facebook', 'icon'=>'facebook', 'url'=>'https://www.facebook.com/DubaiConsult'],
                ];

                $dps = [
                    ['title'=>'Almani Lighting',  'img_class'=>'',  'url'=>'www.almani.ae',         'image'=>'images/hot-developments/almani.png',    'social'=> $socialAlmani],
                    ['title'=>'ZoomDNA',          'img_class'=>'',  'url'=>'www.zoomdna.com',       'image'=>'images/more-projects/zoomdna.png',      'social'=> $socialZoomDNA],
                    ['title'=>'DubaiConsult',     'img_class'=>'',  'url'=>'www.dubaiconsult.com',  'image'=>'images/more-projects/dubaiconsult.png', 'social'=> $socialDubaiConsult],
                
                ];
                ?>
                @foreach($dps as $dp)
                <div class="col-sm-4 swiper-slide" data-aos="fade-up">
                    <div class="project">
                        <a href="http://{{ $dp['url'] }}" class="anchor_style_default"  target="_blank">
                            <h3 class="small_heading_navy raleway center_on_mobile font_400">{{ $dp['title'] }}</h3>
                        </a>
                        <p class="default_text default_text_light open_sans center_on_mobile">
                            <a href="http://{{ $dp['url'] }}" target="_blank">{{ $dp['url'] }}</a>
                        </p>
                        <a href="http://{{ $dp['url'] }}" target="_blank" class="img_a">
                            <div class="blog_img">
                                    {{-- <img src="images/sample-bgs/blog_post_1.jpg" alt="blog_img"> --}}
                                    <img src="{{ asset($dp['image']) }}?v={{ env('APP_ASSET_VER','1.0') }}" alt="{{ $dp['title'] }} Logo">
                            </div>
                        </a>
                        {{-- <p class="default_text open_sans center_on_mobile">
                            Aenean faucibus nulla ex, in rutrum dui blandit at. Pellentesque bibendum odio vitae gravida consequat aliquam sem morbi dor sit imet sen
                        </p> --}}
                    </div>
                    <div class="social">
                        <ul class="footer_links text-center">
                            @foreach ($dp['social'] as $soc)
                            <li>
                                <a class="anchor_style_default {{ $soc['type'] }} wow_zoomInDown" href="{{ $soc['url'] }}" target="_blank">
                                    <i style=""; class="text-center fab fa-{{ $soc['icon'] }}"></i>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>                                    
                </div>
                @endforeach
            </div>




            
        </div>
        
    </section>














    
    <!--Team Section-->
    <section class="team-section" id="team">
        <div class="auto-container">
 
        	<!--Heading-->
        	<div class="title-box" data-aos="fade-up">
                {{-- <h2 class="subtitle"></h2> --}}
                <h2>Our Team</h2>
                {{-- <h4>Dotcom Ventures has established value propositions that serve as the corporate philosophy of the company, fostering the internal operations, product and service development, as well as present and future corporate alliances.</h4> --}}
                <div class="text">
                    Dotcom Ventures is driven by a dedicated team of young, vibrant, and committed specialists who are experts in a variety of core competencies. 
                    <br>
                    These teams work together to identify online opportunities, develop novel solutions for various market sectors, 
                    and attract targeted leads to our businesses.                    
                </div>
            </div>
                        
        	<div class="row justify-content-center clearfix team-members">
                
                <?php

                $teamMembers = [];
                $teamMembers[] = [	'name'=>'Johannes Eidens',        'title'=>'CEO & Founder',             'linkedin'=>'https://www.linkedin.com/in/dotcomv',                         'image'=>'images/home/team/joh.jpg',            'text'=>'', ];
                $teamMembers[] = [	'name'=>'Martin Heyen',           'title'=>'Co-Founder & CFO',          'linkedin'=>'',                                                            'image'=>'images/home/team/martin.jpg',         'text'=>'', ];
                $teamMembers[] = [	'name'=>'Pascal Papara',        'title'=>'Blockchain Project Manager',   'linkedin'=>'https://www.linkedin.com/in/pascal-papara/',             'image'=>'images/home/team/pascal.jpg',   'text'=>'', ];
                $teamMembers[] = [	'name'=>'Muhammad Younas',        'title'=>'Chief Operating Officer',   'linkedin'=>'https://www.linkedin.com/in/muhammad-younas2023',             'image'=>'images/home/team/muhammad.jpg',   'text'=>'', ];
                $teamMembers[] = [	'name'=>'Leo Vicente',            'title'=>'Chief Technical Officer',   'linkedin'=>'https://www.linkedin.com/in/ljvicente',                       'image'=>'images/home/team/leo.jpg',            'text'=>'', ];
                $teamMembers[] = [	'name'=>'Rene dela Rama',       'title'=>'Chief Information Officer',   'linkedin'=>'https://www.linkedin.com/in/rene-rowell-dela-rama-3ab529148', 'image'=>'images/home/team/rene.jpg', 'text'=>'', ];
                $teamMembers[] = [	'name'=>'Rizvi Iqbal',            'title'=>'Chief Strategy Officer',    'linkedin'=>'https://www.linkedin.com/in/rizviqbal',                       'image'=>'images/home/team/rizvi.jpg', 'text'=>'', ];
                $teamMembers[] = [	'name'=>'Ryan Quines',            'title'=>'Design Lead',               'linkedin'=>'https://www.linkedin.com/in/ryan-matthew-quines-551a85152',   'image'=>'images/home/team/ryan.jpg', 'text'=>'', ];
                $teamMembers[] = [	'name'=>'Juliane Schreilechner', 'title'=>'Training Consultant',        'linkedin'=>'https://www.linkedin.com/in/juliane-schreilechner-142108174/', 'image'=>'images/home/team/juliane.jpg', 'text'=>'', ];
                $teamMembers[] = [	'name'=>'Maria Carron Igloso',    'title'=>'Chief Relations Officer',   'linkedin'=>'https://www.linkedin.com/in/maria-carron-igloso-1324b5105',   'image'=>'images/home/team/maria.jpg',          'text'=>'', ];

                // add passcal
                ?>
                
                <!--Team Member-->
                @foreach ($teamMembers as $tm)                    
                <div class="team-member col-md-3 col-sm-6 col-xs-12 wow_zoomIn" data-aos="fade-up">
                	<div class="inner-box">
                    	{{-- <figure class="image-box wow fadeInDown" data-wow-duration="1500ms" data-wow-delay="0ms"><a href="#"><img src="images/resource/team-image-1.jpg" alt=""></a></figure> --}}
                    	
                        <div class="bg-image">
                            <div class="bg-tm-image" style="background-image:url({{ Util::assetUrl($tm['image']) }})"></div>
                            @if($tm['linkedin'])
                            <a href="{{ $tm['linkedin'] }}" class="text-white linkedin" style="" target="_blank">
								<i class="fab fa-linkedin"></i>
                            </a>
                            @endif
                        </div>
                        <div class="lower-content">
                            <div class="designation">{{ $tm['title'] }}</div>
                            <h3>{{ $tm['name'] }}</h3>
                        </div>
                    </div>
                </div>
                @endforeach
                
            </div>
               
        </div>
    </section>
    


    <!--Featured Content Section-->
    <section class="featured-content-section investment-opportunity" 
    style="background-image:url({{ Util::assetUrl('images/home/img36.jpg') }}) "
    id="investment-opportunity">
    <div class="bg-backdrop"></div>
    	<div class="auto-container">
        	<div class="row clearfix">
            	<!--Content Column-->
                <div class="content-column pull-left col-md-6 col-sm-12 col-xs-12">
                	<div class="inner" data-aos="fade-right">
                    	<h2>Investment Opportunity</h2>
                        <h3>lucrative gains for your investment!</h3>
                        <div class="text">
                            <p>Are you looking for your next hot investment opportunity?</p>
                            <p>Are you looking for a partner to start the serial productive of your disruptive business idea?</p>
                            <p>If you are looking to invest and reap lucrative returns on your investments:</p>                            
                            <h3><span class="scrollTo" data-scrollToSelector="#contact-us">APPLY NOW!</span></h3>
                        </div>
                    </div>
                </div>
                <div class="image-column pull-right col-md-6 col-sm-12 col-xs-12 wow_zoomIn" data-aos="fade-left">
                    <div class="image-bg" style="background-image:url({{ Util::assetUrl('images/home/investment-opportunity/img1.png') }}) "></div>
                </div> 
                <!--Image Column-->
                {{-- <div class="image-column pull-left col-md-6 col-sm-12 col-xs-12">
                    <div class="image-bg" style="background-image:url({{ Util::assetUrl('images/home/img35.jpg') }}) "></div>
                </div> --}}
            </div>
        </div>
    </section>
    



    <!--Contact Info Section-->
    <section class="contact-info-section" id="contact-us">
    	<div class="auto-container">
        	<!--Heading-->
        	<div class="title-box" data-aos="fade-up">
                {{-- <h3 class="subtitle">Dotcom Ventures</h3> --}}
                <h2>Contact Us</h2>
                <h5>To get in touch with Dotcom Ventures, please fill in the form below.</h5>
            </div>
            
            <div class="info-container">
                <div class="row clearfix">
                    

                    
                    <!--Left Column-->
                    <div class="left-column col-md-6 col-sm-6 col-xs-12 " data-aos="fade-right">
                        <div class="inner-box complete-address">
                            <h5>
                                <i class="fas fa-map-marker-alt"></i>
                                Dotcom Ventures FZE, <br>
                                Business Center, <br>
                                Al Shmookh Building, <br>
                                Free Trade Zone <br>
                                Umm Al Quwain, UAE
                            </h5>
                            <h5>
                                <i class="fas fa-phone"></i>
                                <a href="tel:+97148873265" style="font-weight: bold;">(+971) 4 887 3265</a>
                            </h5>
                            <h5>
                                <i class="fas fa-at"></i>
                                i<span></span>nfo<span></span>@dot<span>com</span>v<span></span>.com
                            </h5>

                        </div>                   
                    </div>

                    <!--Right Column-->
                    <div class="right-column col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                        <div class="contact-us-form">
                            <form method="post" action="{{ route('contact.inquiry') }}" name="inquiry" id="inquiry" id="contact-form">
                                {{ csrf_field() }}
                                <input type="hidden" name="csrf_token" id="csrf_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input id="name" type="text" name="username" value="" placeholder="Your Name">
                                </div>
                                <div class="form-group">
                                    <input id="email" type="email" name="email" value="" placeholder="Email Address">
                                </div>
                                <div class="form-group">
                                    <textarea id="message" name="message" placeholder="Message"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" name="check_privacypolicy" class="" id="check_privacypolicy" value="1" required="required">
                                    <span class="default_small_heading raleway blue_color font_200t" for="check_privacypolicy">I agree the processing of my personal data. Click <a href="#" data-target="#pp_modal" data-toggle="modal">Privacy Policy</a> for more info.</span>
                                </div>
                                <div class="form-group">
                                    <button id="submit_btn" type="button" 
                                    class="theme-btn btn-style-three send-message-btn"
                                    onclick="checkForm()">
                                    send message
                                    </button>
                                </div>
                                <div class="form-group" style="font-size: 10px; line-height: 15px; font-size: 10px; line-height: 15px; font-style: italic;">
                                    This site is protected by reCAPTCHA and the Google
                                    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                                    <a href="https://policies.google.com/terms">Terms of Service</a> apply.                                    
                                </div>

                            </form>

                            {{-- recaptcha v3 --}}
                            <input type="hidden" name="verifyrecpatcha_url" id="verifyrecpatcha_url" value="{{ route('verifyrecpatcha') }}">
                            <input type="hidden" name="recaptcha_token" id="recaptcha_token" value="">

                        </div>
                    </div>
                    

                </div>
        	</div>
                
        </div>
    </section>
    
    
    



    
    <!--Main Footer-->
    <footer class="main-footer">
              
        <!--Footer Bottom-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="footer-social">
                    <?php
                    $sdvs = [
                        [ 'name'=>'facebook', 'icon'=>"fa fa-facebook", 'url'=>'https://www.facebook.com/DotcomV/',],
                        [ 'name'=>'instagram', 'icon'=>"fab fa-instagram", 'url'=>'https://www.instagram.com/dotcomv_/',],
                        [ 'name'=>'linkedin', 'icon'=>"fa fa-linkedin", 'url'=>'https://www.linkedin.com/company/dotcomv',],
                        [ 'name'=>'twitter', 'icon'=>"fa fa-twitter", 'url'=>'https://twitter.com/DotcomV_',],
                    ];
                ?>
                @foreach ($sdvs as $sdv)                                        
                    <a target="_blank" href=" {{ $sdv['url'] }} ">
                        <i class=" {{ $sdv['icon'] }} "></i>
                    </a> &ensp; 
                @endforeach                    
                </div>                
                <div class="copyright-text">
                    Copyright &copy; 2018. All Rights Reserved By 
                    <strong>Dotcom Ventures FZE</strong>
                </div>
                <div class="copyright-text">                    
                    <a href="#" data-target="#pp_modal" data-toggle="modal">Privacy Policy</a>
                    <span class="period"><i class="fas fa-circle"></i></span>
                    <a href="#" data-target="#tc_modal" data-toggle="modal">Terms & Conditions</a>
                </div>
            </div>
        </div>
    </footer>
    



    </div>
    <!--End pagewrapper-->
    









    {{-- modals --}}
    {{-- /////////////////////////////////////////////////////////////////////////// --}}








    <div class="modal about-modal fade" id="tc_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <img src="{{ asset('images/logos/logo-sm.png') }}">
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="modal-info">

                        <div class="document">

                            <h2>Terms and Condition</h2>

                            <div class="document_text_content">

                                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">
                                    PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY.
                                </p>
                                <p style="text-align: center; font-weight: bold; margin-bottom: 5px;">
                                    THESE TERMS AND CONDITIONS SHALL BE BINDING UPON USERS OF WEBSITE WWW.DOTCOMV.COM.
                                </p>
                                

                                <h3>Introduction</h3>
                                <p>Please carefully read the Terms and Conditions ("Terms") for the website located at www.dotcomv.com (the "Website") including its sub-domains and mobile optimized version, as set out hereinafter. The Website is owned and operated by Dotcom Ventures FZE (hereinafter referred to also as "Company").</p>
                                <p>The Company is a holding/parent company of its various other subsidiary internet/tech related companies. The Company aims at selling digital products through its subsidiaries as well as operating different digital platforms offering service based on a monthly membership fee.</p>
                                <p>All our subsidiary companies have separate terms and conditions for their respective services.</p>
                                <p>Any ancillary terms, guidelines, the privacy policy (the "Policy") and other documents made available by the Website from time to time and as incorporated herein by reference, shall be deemed as an integral part of the Terms. These terms set forth the legally binding agreement between you as the user(s) of the Website (hereinafter referred to as "you", "your" or "User") and the Company. If you are using the Website or its services on behalf of an entity, organization, or company (collectively "Subscribing Organization"), you declare that you are an authorized representative of that Subscribing Organization with the authority to bind such organization to these Terms; and agree to be bound by these Terms on behalf of such Subscribing Organization. In such a case, "you" in these Terms refers to your Subscribing Organization, and any individual authorized to use the Service on behalf of the Subscribing Organization, including you.</p>
                                <p>By using company provided services, accessing or using the Website in any manner as laid down herein, including, but not limited to, visiting or browsing it, or contributing content or other materials to it, you agree to be bound by these Terms. </p>
                                <p>These Terms, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by the Website without restriction. Any attempted transfer or assignment in violation hereof shall be null and void. </p>
                                

                                <h3>Acceptance of the Terms</h3>
                                <p>Each time by viewing, using, accessing, browsing, or submitting any content or material on the Website, including the webpages contained or hyperlinked therein and owned or controlled by the Website and its services, whether through the Website itself or through such other media or media channels, devices, software, or technologies as the Website may choose from time to time, you are agreeing to abide by these Terms, as amended from time to time with or without your notice. </p>
                                <p>The Website reserves the right to modify or discontinue, temporarily or permanently, and at any time, the Website and/or the Website Services (or any part thereof) with or without notice. You agree that the Website shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Website Services.</p>
                                <p>Website or the Website management may modify these Terms from time to time, and any change to these Terms will be reflected on the Website with the updated version of the Terms and you agree to be bound to any changes to these Terms when you use the Website or the Website Services. The Website may also, in its sole and absolute discretion, choose to alert via email all users with whom it maintains email information of such modifications.</p>
                                <p>Also, occasionally there may be information on the Website or within the Website Services that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, availability, and various other information, and the Website management reserves the right to correct any errors, inaccuracies or omissions and to change or update the information at any time, without prior notice. </p>
                                <p>When you register an account on the Website and/or upload, submit, enter any information or material to the Website or use any of the Website Services, you shall be deemed to have agreed to and understand the Terms.</p>
                                

                                <h3 style="text-align: center;">IF YOU DO NOT AGREE TO THESE TERMS OF USE, THEN YOU MAY NOT USE THE WEBSITE</h3>


                                <h3>Service Availability</h3>
                                <p>The Website shall use commercially reasonable efforts to keep it up and running 24 hours a day, seven days a week; provided, however, that it may carry out scheduled and unscheduled maintenance work as necessary from time to time and such maintenance work may impact the availability of the Website.</p>
                                

                                <h3>Your Legal Obligations</h3>
                                <p>You agree that the use of the Website and/or the Website Services on the Website is subject to all applicable local, state and federal laws and regulations. Unless specifically endorsed or approved by the Website, you also agree:</p>
                                <ul>
                                    <li>not to use the Website for illegal purposes;</li>
                                    <li>not to commit any acts of infringement on the Website or with respect to content on the Website;</li>
                                    <li>not to copy any content for republication in print or online;</li>
                                    <li>not to create reviews or blog entries for or with any purpose or intent that does not in good faith comport with the purpose or spirit of the Website;</li>
                                    <li>not to attempt to gain unauthorized access to other computer systems from or through the Website;</li>
                                    <li>not to interfere with another person's use and enjoyment of the Website or another entity's use and enjoyment of the Website;</li>
                                    <li>not to upload or transmit viruses or other harmful, disruptive or destructive files; and/or</li>
                                    <li>not to disrupt, interfere with, or otherwise harm or violate the security of the Website, or any services, system restores, accounts, passwords, servers or networks connected to or accessible through the Website or affiliated or linked website.</li>
                                    <li>not to use the Website in any way or take any action that causes, or may cause, damage to the Website or impairment of the performance, availability or accessibility of the Website;</li>
                                    <li>not to use the Website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</li>
                                    <li>not to use the Website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</li>
                                    <li>not to conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to the Website without the express written consent of the Website owner;</li>
                                    <li>not to access or otherwise interact with the Website using any robot, spider or other automated means;</li>
                                    <li>not to use data collected from the website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing);</li>
                                    <li>not to infringe these Terms or allow, encourage or facilitate others to do the same;</li>
                                    <li>not to plagiarize and/or infringe the intellectual property rights or privacy rights of any third party;</li>
                                    <li>not to disturb the normal flow of Services provided within the Website;</li>
                                    <li>not to create a link from the Website to another website or document without Company’s prior written consent;</li>
                                    <li>not to obscure or edit any copyright, trademark or other proprietary rights notice or mark appearing on the Website;</li>
                                    <li>not to create copies or derivate works of the Website or any part thereof;</li>
                                    <li>not to reverse engineer, decompile or extract the Website’s source code;</li>
                                    <li>not to remit or otherwise make or cause to deliver unsolicited advertising, email spam or other chain letters;</li>
                                    <li>not to collect, receive, transfer or disseminate any personally identifiable information of any person without consent from title holder; and/or</li>
                                    <li>not to pretend to be or misrepresent any affiliation with any legal entity or third party.</li>
                                </ul>
                                

                                <p>In addition to the above clause, unless specifically endorsed or approved by the Website, the following uses and activities of and with respect to the Website and the Website Services are prohibited:</p>
                                <ul>
                                    <li>criminal or tortuous activity, including child pornography, fraud, trafficking in obscene material, drug dealing, gambling, harassment, stalking, spamming, copyright infringement, patent infringement, or theft of trade secrets;</li>
                                    <li>transmitting chain letters or junk email;</li>
                                    <li>interfering with, disrupting, or creating an undue burden on the Website or the Website Services or the networks or services connected or linked thereto;</li>
                                    <li>using any information obtained from the Website or the Website Services in order to harass, abuse, or harm another person;</li>
                                    <li>deciphering, decompiling, disassembling or reverse engineering any of the software comprising or in any way making up a part of the Website or the Website Services;</li>
                                    <li>attempting to bypass any measures of the Website or the Website Services designed to prevent or restrict access to the Website or the Website Services, or any portion of the Website or the Website Services;</li>
                                    <li>harassing, annoying, intimidating or threatening any the Website employees or agents engaged in providing any portion of the Website Services;</li>
                                    <li>using the Website and/or the Website Services in any manner inconsistent with any and all applicable laws and regulations.</li>
                                    <li>Using data collected from the website to contact individuals, companies or other persons or entities.</li>
                                    <li>Supplying false, untrue, expired, incomplete or misleading information through the Website.</li>
                                </ul>
                                
                                <p>You also acknowledge and accept that any violation of the aforementioned provisions may result in the immediate termination of your access to the Website and use of our Services. Access to the Website may be terminated or suspended without prior notice or liability of Company. You represent and warrant to us that you have all right, title, and interest to any and all content you may post, upload or otherwise disseminate through the Website. You hereby agree to provide Company with all necessary information, materials and approval, and render all reasonable assistance and cooperation necessary for our Services.</p>

                                <h3>Third party websites</h3>
                                <p>The Website includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations. Goods and services of third parties may be advertised and/or made available on or through this web site. Representations made regarding products and services provided by third parties are governed by the policies and representations made by these third parties. The Website shall not be liable for or responsible in any manner for any of your dealings or interaction with third parties.</p>
                                <p>The management of the Website has no control over third party websites and their contents, and subject to the Terms it accepts no responsibility for them or for any loss or damage that may arise from your use of them.</p>
                                <p>The Website may contain links from third party websites. External hyperlinks to or from the site do not constitute the Website’s endorsement of, affiliation with, or recommendation of any third party or its website, products, resources or other information. The Website is not responsible for any software, data or other information available from any third party website. You are solely responsible for complying with the terms and conditions for the third party sites. You acknowledge that Company shall have no liability for any damage or loss arising from your access to any third party website, software, data or other information.</p>
                                <p>We do not always review the information, pricing, availability or fitness for use of such products and services and they will not necessarily be available or error free or serve your purposes, and any use thereof is at your sole risk.  We do not make any endorsements or warranties, whether express or implied, regarding any third party websites (or their products and services).  Any linked websites are ruled by their privacy policies, terms and conditions and legal disclaimers.  Please read those documents, which will rule any interaction thereof.</p>
                                <p>The Website may provide tools through the Service that enable you to export information to third party services, including through use of an API or by linking your account on the Website with an account on the third party service, such as Twitter or Facebook. By using these tools, you agree that we may transfer such User Content and information to the applicable third party service. Such third party services are not under our control, and we are not responsible for the contents of the third party service or the use of your User Content or information by the third party service. The Service, including our websites, may also contain links to third-party websites. The linked sites are not under our control, and we are not responsible for the contents of any linked site. We provide these links as a convenience only, and a link does not imply our endorsement of, sponsorship of, or affiliation with the linked site. You should make whatever investigation you feel necessary or appropriate before proceeding with any transaction with any of these third parties services or websites.</p>


                                
                                

                                <h3>Third party rights</h3>
                                <p>A contract under the Terms is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.</p>
                                <p>The exercise of the parties' rights under a contract under the Terms is not subject to the consent of any third party.</p>
                                <p>You agree not to; modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, scrape, gather, market, rent, lease, re-license, reverse engineer, or sell any information published by other users without the original publishers written consent.</p>


                                
                                <h3>Ownership</h3>
                                <p>The trademarks, copyright, service marks, trade names and other intellectual and proprietary notices displayed on the Website are the property of – or otherwise are licensed to – Company or its licensors or affiliates, whether acknowledged (or not), and which are protected under intellectual and proprietary rights throughout the world.  Respective title holders may or may not be affiliated with us or our affiliates, partners and advertisers.</p>
                                <p>No section hereof shall be construed as intent to grant to you any interest in the Website or our Services, in whole or in part. All content and materials included as part of the Services, such as images, photographs, graphics, texts, forms, lists, charts, guidelines, data, logos, code, icons, videos, audio and other content are the property of, are licensed to or are otherwise duly available to Company, its affiliates, its licensors or to the appertaining third party copyrights holder.</p>
                                <p>You acknowledge and agree that any and all infringing use or exploitation of copyrighted content in the Website and our Services may cause us, our affiliates, licensors or content providers irreparable injury, which may not be remedied solely at law, and therefore our affiliates, licensors or content providers may seek remedy for breach of these Terms, either in equity or through injunctive or other equitable relief.</p>


                                


                                <h3>Term and Termination</h3>
                                <p>The term hereof shall begin on the date that comes first among: (i) first access to the Website; (ii) your first access or execution of our Services; or (iii) Company begins providing its Services to you.</p>
                                <p>The term hereof will automatically end on the earlier date of either your: (i) account deactivation, suspension, freezing or deletion; (ii) access termination or access revocation for our Services or the Website; (iii) Company’s termination of these Terms or its Services, at its sole and final discretion; (iv) the termination date indicated by Company to you from time to time; or (v) Company’ decision to make the Website or Services no longer available for use, at its sole and final discretion.</p>
                                <p>Upon expiration of these Terms or termination of your subscription to our Services, you shall thereafter immediately cease any and all use of our Services, along with any and all information and data collected therefrom.</p>

                                
                                
                                <h3>Amendments</h3>
                                <p>Company hereby reserves the right to update, modify, change, amend, terminate or discontinue the Website, the Terms and/or the Policy, at any time and at its sole and final discretion.  Company may change the Website’s functionalities and (any) applicable fees at any time.  Any changes to these Terms will be displayed in the Website, and we may notify you through the Website or by email. Please, refer to the date shown above for the date where effective changes were last undertook by us. Your use of our Services after the effective date of any update– either by an account registration or simple use – thereby indicates your acceptance thereof.</p>


                                <h3>No Warranty</h3>
                                <p>Your use of our Website is at your own risk, and therefore you hereby acknowledge and agree that our Website and Services are provided "as is", "with all faults", and "as available", including all content, guides, checklists, reference guides, sample filing forms, software, materials, services, functions and/or information made available thereby.  It shall be your own responsibility to ensure that the Services or information available through this Website meet your specific requirements.</p>
                                <p>Neither Company, nor its affiliates, subsidiaries, officers, employees and agents warrantee that the Website will be error-free, uninterrupted, secure, or produce any particular results; or that any listing, purchase, order, amount, information, guide, sheet, checklist and/or content will be current, measured useful and/or valid, or that it will produce any particular results or that the information obtained therefrom will be reliable or accurate. No advice or information given by Company or its employees, affiliates, contractors and/or agents shall create a guarantee. No warranty or representation is made with regard to such services or products of third parties contacted on or through the Website.  In no event shall Company or our affiliates be held liable for any such services.</p>
                                <p>Neither Company, nor its affiliates, licensors, owners, subsidiaries, brands or advertisers are a professional advisor in any industry.  The results described in the Website are not typical and will vary based on a variety of factors outside the control of Company. Your use of any information and/or materials on this Website is entirely at your own risk, for which we shall not be held liable.</p>

                                    


                                <h3>Disclaimer of Damages</h3>
                                <p>Your use of our Website is at your own risk, and therefore you hereby acknowledge and agree that our Website and Services are provided "as is", "with all faults", and "as available", including all content, guides, checklists, reference guides, sample filing forms, software, materials, services, functions and/or information made available thereby.  It shall be your own responsibility to ensure that the Services or information available through this Website meet your specific requirements.</p>
                                <p>Neither Company, nor its affiliates, subsidiaries, officers, employees and agents warrantee that the Website will be error-free, uninterrupted, secure, or produce any particular results; or that any listing, purchase, order, amount, information, guide, sheet, checklist and/or content will be current, measured useful and/or valid, or that it will produce any particular results or that the information obtained therefrom will be reliable or accurate. No advice or information given by Company or its employees, affiliates, contractors and/or agents shall create a guarantee. No warranty or representation is made with regard to such services or products of third parties contacted on or through the Website.  In no event shall Company or our affiliates be held liable for any such services.</p>
                                <p>Neither Company, nor its affiliates, licensors, owners, subsidiaries, brands or advertisers are a professional advisor in any industry.  The results described in the Website are not typical and will vary based on a variety of factors outside the control of Company. Your use of any information and/or materials on this Website is entirely at your own risk, for which we shall not be held liable.</p>




                                <h3>Indemnification</h3>
                                <p>You agree to indemnify, defend and hold Company and its independent contractors, affiliates, subsidiaries, officers, employees and agents, and their respective employees, agents and representatives, harmless from and against any and all actual or threatened proceedings (at law or in equity), suits, actions, damages, claims, deficiencies, payments, settlements, fines, judgments, costs, liabilities, losses and expenses (including, but not limited to, reasonable expert and attorney fees and disbursements) arising out of, caused or resulting from: (i) your conduct and any user content; (ii) your violation of these Terms or the Policy; and (iii) your violation of the rights of any third-party. </p>
                                <p>You indemnify the Website and its management for any time that the Website may be unavailable due to routine maintenance, updates or any other technical or non-technical reasons. You agree to indemnify the Website and its management for any error, omission, interruption, deletion, defect, delay in operation or transmission, communication line failure, theft or destruction or unauthorized access to your published content, damages from lost profits, lost data or business interruption.</p>
                                <p>You hereby indemnify the Website and its management and will not hold them responsible for copyright theft, reverse engineering and use of your content by other users on the website.</p>



                                <h3>Generals</h3>
                                <p>Advertisements and Promotions.  From time to time, we may place ads and promotions from third party sources in the Website.  Accordingly, your participation or undertakings in promotions of third parties other than Company, and any terms, conditions, warranties or representations associated with such undertakings, are solely between you and such third party.  Company is not responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of third party advertisers on the Website.</p>
                                <p>No Assignment. You may not assign or transfer these Terms by operation of law or otherwise without our prior written consent.  Notwithstanding the foregoing, we may assign any rights or obligations hereunder to any current or future affiliated company and to any successor in interest.  Any rights not expressly granted herein are thereby reserved.  These terms will inure to the benefit of any successors of the parties.  We reserve the right, at any time, to transfer some or all of Company’s assets in connection with a merger, acquisition, reorganization or sale of assets or in the event of bankruptcy.</p>
                                <p>Content Moderation.  Company hereby reserves the right, at its sole and final discretion, to review any and all content delivered into the Website, and use moderators and/or any monitoring technology to flag and remove any user generated content or other content deemed inappropriate.</p>
                                <p>Force Majeure. Company is no liable for any failure of performance on its obligations as set forth herein, where such failure arises from any cause beyond Company's reasonable control, including but not limiting to, electronic, power, mechanic or Internet failure, from acts of nature, forces or causes beyond our control, including without limitation, Internet failures, computer, telecommunications or any other equipment failures, electrical power failures, strikes, labor disputes, riots, insurrections, civil disturbances, shortages of labor or materials, fires, flood, storms, explosions, acts of God, war, governmental actions, orders of domestic or foreign courts or tribunals or non-performance of third parties. </p>
                                <p>Headings. The titles of paragraphs in these Terms are shown only for ease of reference and will not affect any interpretation therefrom.</p>
                                <p>No Waiver. Failure by Company to enforce any rights hereunder shall not be construed as a waiver of any rights with respect to the subject matter hereof. </p>
                                <p>No Relationship. You and Company are independent contractors, and no agency, partnership, joint venture, employee-employer, or franchiser-franchisee relationship is intended or created by these Terms.</p>
                                <p>Notices. All legal notices or demands to or upon Company shall be made in writing and sent to Company personally, by courier, certified mail, or facsimile, and shall be delivered to any address the parties may provide.  For communications by e-mail, the date of receipt will be the one in which confirmation receipt notice is obtained.  You agree that all agreements, notices, demands, disclosures and other communications that Company sends to you electronically satisfy the legal requirement that such communication should be in writing.</p>
                                <p>Severability. If any provision of these Terms is held unenforceable, then such provision will be modified to reflect the parties' intention. All remaining provisions of these Terms will remain in full force and effect. The failure of either party to exercise in any respect any right provided for herein will not be deemed a waiver of any further rights hereunder.</p>



                                <h3>Contact</h3>
                                <p>For any inquires or complaints regarding the Service or Website, please contact by email at in<span>fo@</span>d<span>otc</span>omv.c<span>om</span>.</p>
                                                                        
                            </div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>




    <div id="success_tic" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h3 style="margin-top:5px;" class="default_section_heading raleway navy_blue">Thank you!</h3>
                        <h4 class="default_small_heading raleway blue_color font_200">One of our Member will get in touch with you!</h4>
                    </div>
    
                    <h1 style="text-align:center;">
                        <div class="checkmark-circle">
                            <div class="background"></div>
                            <div class="checkmark draw"></div>
                        </div>
                    </h1>
                </div>
            </div>
        </div>
    </div>


    <div id="error_tic" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <a class="close" href="#" data-dismiss="modal">&times;</a>
                <div class="page-body">
                    <div class="head">
                        <h3 style="">Error</h3>
                        <h4 class="error_text default_small_heading raleway blue_color font_200">Please check the form!</h4>
                    </div>
    
                    {{-- <h1 style="text-align:center;">
                        <div class="checkmark-circle">
                            <div class="background"></div>
                            <div class="checkmark draw"></div>
                        </div>
                    </h1> --}}
                </div>
            </div>
        </div>
    </div>





    <div class="modal about-modal fade hasScrollTo" id="pp_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <img src="{{ asset('images/logos/logo-sm.png') }}">

                    </h4>
                </div>
                <div class="modal-body">
                    <div class="modal-info">


                        <div class="document">

                            <h2>Privacy Policy</h2>

                            <div class="document_text_content">


                                {{-- <h3>Introduction</h3> --}}
                                <p>By accessing or using www.dotcomv.com (the "Website"), a website owned and maintained by Dotcom Ventures FZE ("Company", "we," "us" or "our"), you consent to the information collection, disclosure and use practices described in this Privacy Policy. This Privacy Policy sets out how we may collect, use and disclose information in relation to users of the Website.</p>
                                <p>The Company is a holding/parent company of its various other subsidiary internet/tech related companies. The Company aims at selling digital products through its subsidiaries as well as operating different digital platforms offering service based on a monthly membership fee.</p>
                                <p>Use of Website of the Company is subject to separate Terms and Conditions and the following Privacy Policy. </p>
                                
                                <h3>Information We Collect</h3>
                                <p>Your privacy is important to us and we have taken steps to ensure that we do not collect more information from you than what is necessary.</p>
                                <p>All our subsidiary companies have separate Privacy Policies for their respective services.</p>                                            
                                <p>We may collect, use and process the following information ("Data") about you:</p>
                                <ul>
                                    <li>Personal Data, which may include name, email, address, date of birth, gender, age, phone number, and such other data as we may deem necessary, which shall, however, not include be any sensitive data.</li>
                                    <li>Communication Data, which may include any communication that you send to us whether that be through the contact form on our Website, through email, text, social media messaging, social media posting or any other communication that you send us. We process this data for the purposes of communicating with you, for record keeping and for the establishment, pursuance or defence of legal claims. Our lawful ground for this processing is our legitimate interests which in this case are to reply to communications sent to us, to keep records and to establish, pursue or defend legal claims.</li>
                                    <li>Technical Data, which may include data about your use of our Website such as your IP address, your login data, details about your browser, length of visit to pages on our Website, page views and navigation paths, details about the number of times you use our Website, time zone settings and other technology on the devices you use to access our Website.</li>
                                    <li>Marketing Data, which may include data about your preferences in receiving marketing from us and our third parties and your communication preferences.</li>
                                </ul>


                                <h3>How We Use and Process the Data</h3>
                                <p>When you visit our Website you provide us consent to use your Data as per this Privacy Policy. </p>
                                <p>The Data collected by us from you may be used to better understand your needs, to correspond with you and reply to your questions.</p>
                                <p>We will not rent or sell your Data to others. If you provide any Data to us, you are deemed to have authorized us to collect, retain and use that data for the following purposes:</p>
                                <ul>
                                    <li>verifying your identity;</li>
                                    <li>providing you with customer service and responding to your queries, feedback, or disputes;</li>
                                    <li>making such disclosures as may be required for any of the above purposes or as required by law, regulations and guidelines or in respect of any investigations, claims or potential claims brought on or against us; and</li>
                                    <li>maintaining the Website.</li>
                                </ul>
                                
                                <p>We shall ensure that:</p>
                                <ul>
                                    <li>The Data collected and processed for and on our behalf by any party is collected and processed fairly and lawfully;</li>
                                    <li>You are always made fully aware of the reasons for the collection of Data and are given details of the purpose(s) for which the data will be used;</li>
                                    <li>The Data is only collected to the extent that is necessary to fulfil the purpose(s) for which it is required;</li>
                                    <li>No Data is held for any longer than necessary in light of the purpose(s) for which it is required.</li>
                                    <li>Whenever cookies or similar technologies are used online by us, they shall be used strictly in accordance with the law;</li>
                                    <li>You are informed if any data submitted by you online cannot be fully deleted at your request under normal circumstances and how to request that the we delete any other copies of that data, where it is within your right to do so;</li>
                                    <li>All Data is held in a safe and secure manner taking all appropriate technical and organisational measures to protect the data;</li>
                                    <li>All data is transferred securely, whether it is transmitted electronically or in hard copy.</li>
                                    <li>You can fully exercise your rights with ease and without hindrance.</li>
                                </ul>


                                <h3>Data Storage and Transfer</h3>
                                <p>Your Data may be stored and processed at the servers in the United States, Europe, or any other country in which the Website or its subsidiaries, affiliates or service providers maintain facilities.</p>
                                <p>The Website may transfer Data to affiliated entities, or to other third parties across borders and from your country or jurisdiction to other countries or jurisdictions around the world. Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
                                <p>We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>
                                <p>We will only retain your Data preferably for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, see section regarding insurance requirement and reporting requirements. When deciding what the correct time is to keep the Data for we look at its amount, nature and sensitivity, potential risk of harm from unauthorised use or disclosure, the processing purposes, if these can be achieved by other means and legal requirements.</p>



                                <h3>Protection of your Data</h3>
                                <p>We store all the Data submitted by you through Website at a secure database.</p>
                                <p>We are concerned with protecting your privacy and data, but we cannot ensure or warrant the security of any data you transmit to or guarantee that your Data may not be accessed, disclosed, altered or destroyed by breach of any of our industry standard physical, technical or managerial safeguards.</p>
                                <p>No method of transmission over the Internet or method of electronic is secure. Therefore, we cannot guarantee its absolute security. If you have any questions about security of our Website, you can contact us at info@dotcomv.com.</p>
                                <p>Any Data supplied by you will be retained by us and will be accessible by our employees, any service providers engaged by us and third parties.</p>
                                


                                <h3>Data Protection Officer</h3>
                                <p>You may also contact Mr. Rene Rowell Dela Rama, our Data Protection Officer, at privacy@dotcomv.com.</p>
                                
                                
                                <h3>The rights of Users </h3>
                                <p>You may exercise certain rights regarding your Data processed by us. In particular, you have the right to do the following:</p>
                                <ul>
                                    <li>Withdraw consent at any time. You have the right to withdraw consent where you have previously given your consent to the processing of your Data.</li>
                                    <li>Object to processing of Data. You have the right to object to the processing of your Data if the processing is carried out on a legal basis other than consent. </li>
                                    <li>Access to the Data. You have the right to learn if Data is being processed by us, obtain disclosure regarding certain aspects of the processing and obtain a copy of the Data undergoing processing.</li>
                                    <li>Verify and seek rectification. You have the right to verify the accuracy of your Personal and other Data and ask for it to be updated or corrected.</li>
                                    <li>Restrict the processing of your Data. You have the right, under certain circumstances, to restrict the processing of Data. In this case, we will not process your Data for any purpose other than storing it.</li>
                                    <li>Have the Data deleted or otherwise removed. You have the right, under certain circumstances, to obtain the erasure of the Data from us.</li>
                                </ul>

                                
                                <h3>Compliance with the GDPR</h3>
                                <p>For users based in the European Union (EU), the Website shall make all reasonable efforts to ensure that it complies with The General Data Protection Regulation (GDPR) (EU) 2016/679 as set forth by the European Union regarding the collection, use, and retention of Data from European Union member countries. Website shall make all reasonable efforts to adhere to the requirements of notice, choice, onward transfer, security, data integrity, access and enforcement.</p>

                                
                                <h3 id="cookies">Cookies</h3>
                                <p>We use technologies, such as cookies, to make better user experience, customise content and advertising, to provide social media features and to analyse traffic to the Website. Where applicable the Website uses a cookie control system allowing the user on their first visit to the Website to allow or disallow the use of cookies on their computer / device.</p>
                                <p>We also share information about your use of our site with our trusted social media, advertising and analytics partners. Third parties, including Facebook and Google, may use cookies, web beacons, and other storage technologies to collect or receive your information from the Websites and use that information to provide measurement services and target ads.</p>
                                <p>Cookies are small files saved to the user's computers’ or mobile devices’ hard drive or memory that track, save and store information about the user's interactions and usage of the Website. This allows the Website, through its server to provide the users with a tailored experience within this Website.</p>
                                <p>Users are advised that if they wish to deny the use and saving of cookies from this Website on to their computers hard drive they should take necessary steps within their web browsers security settings to block all cookies from this Website and its external serving vendors.</p>
                                <p>We may gather certain information automatically and store it in log files. This information includes Internet protocol (IP) addresses, browser type, Internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and click stream data. We may use this information, which does not identify individual users, to analyze trends, to administer the Website, to track users’ movements around the Website and to gather demographic information about our user base as a whole.</p>
                                <p>We may track the referring URL (the web page you left before coming to the Website) and the pages, links, and graphics of the Website you visited. We do so because it allows us to evaluate the reputation and responsiveness of specific web pages and any promotional programs we may be running.</p>
                                <p>Managing Cookies: Many web browsers allow you to manage your preferences. You ca set your browser to refuse cookies or delete certain cookies. You may be able to manage other technologies in the same way that you manage cookies using your browser’s preferences.</p>
                                <p>Please note that if you choose to block cookies, doing so may impair the Website functionality or prevent certain elements of it from functioning.</p>



                                
                                <h3>Third Party Links</h3>
                                <p>The Website may contain links to third-party websites, plug-ins and applications. Except as otherwise discussed in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you on our Website. Other websites accessible through our site via links or otherwise have their own policies in regard to privacy. We are not responsible for the privacy policies or practices of third parties. When you leave our Website, we encourage you to read the privacy notice of every website you visit.</p>

                                


                                <h3>Changes to this Privacy Statement</h3>
                                <p>We may modify these this Privacy Policy from time to time, and any such change shall be reflected on the Website with the updated version of the Privacy Policy and you agree to be bound to any changes to the updated version of Privacy Policy when you use the Website.</p>
                                <p>You acknowledge and agree that it is your responsibility to review this Website and this Policy periodically and to be aware of any modifications. Updates to this Policy will be posted on this page.</p>
                                <p>Also, occasionally there may be information on the Website that contains typographical errors, inaccuracies or omissions that may relate to service descriptions, pricing, availability, and various other information, and the Website reserves the right to correct any errors, inaccuracies or omissions and to change or update the information at any time, without prior notice.</p>
                                


                                <h3>Contact US</h3>
                                <p>If you have questions about our Privacy Policy, please contact us via email: <span>info@</span>dot<span>com</span>v.com.</p>
                                


                            </div>

                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
















































    {{-- /////////////////////////////////////////////////////////////////////////// --}}
    {{-- modals --}}


















    <script>
        AOS.init({
            duration: 600,            
            anchorPlacement: 'center-bottom', 
        });
    </script>    

    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-arrow-up"></span></div>
    

    
    <script src="js/jquery.js"></script>
    
    <!--Revolution Slider-->
    {{-- <script src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="js/main-slider-script.js"></script>
    <!--End Revolution Slider Script-->
     --}}
    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/jquery.fancybox-media.js"></script>
    <script src="js/mixitup.js"></script>
    <script src="js/owl.js"></script>
    <script src="js/appear.js"></script>
    <script src="js/wow.js"></script>


    <!-- Particles Core Js -->
    {{-- <script src="js/particles.js"></script>
    <script src="js/particlesJS.min.js"></script> --}}


    <script src="js/script.js"></script>
    
    {{-- old javascript - for menu --}}
    <script src="js/script-old.js"></script>

    <script src="js/master-app.js?v={{ env('APP_ASSET_VER','1.0') }}"></script>
    
    </body>
    </html>
    