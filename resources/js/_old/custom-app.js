$(document).ready(function(){

	$('.open_read_more').click(function(){
		$(this).slideUp().closest('div').find('.read_more').slideDown();
	});


	$('.submit_btn').click(function() {
	});

	$( "form" ).submit(function() {
	   $('.submit_btn').attr('disabled','disabled').html('<i class="fa fa-envelope" aria-hidden="true"></i> Please Wait..');

	});


	if(success === 'true') {
		$('#success_tic').modal('show');
	}

	// cookie
	//////////////////////////////////////////
	if(getCookie('dotcomv_cookie')=='1') {
		$('.cookie_warning').css('display','none');
	}


	$('.i_accept').click(function(){
		$('.cookie_warning').slideUp();
		setCookie('dotcomv_cookie','1', '180')
	});
	//////////////////////////////////////////
	// cookie 

	var swiper = new Swiper('.present_products_slider_main', {
	    slidesPerView: 2,
	    spaceBetween: 30,
	    autoplay : {
	    	 delay: 7000,
	   	},
		navigation: {
			nextEl: $('#present_products .swiper-button-next'),
			prevEl: $('#present_products .swiper-button-prev'),
		},	    
	    paginationClickable: true,
	    loop: false,
	    breakpoints: {
	        950: {
	            slidesPerView: 2,
	            spaceBetween: 10,
	        },
	        550: {
	            slidesPerView: 1,
	            spaceBetween: 10,
	        }
	    }
	});

	var swiper = new Swiper('.successful_spinoffs_slider_main', {
	    slidesPerView: 3,
	    spaceBetween: 10,	    
	    autoplay : {
	    	 delay: 5000,
	   	},
		navigation: {
			nextEl: $('#successful_spinoffs .swiper-button-next'),
			prevEl: $('#successful_spinoffs .swiper-button-prev'),
		},	    
	    paginationClickable: true,
	    loop: true,
	    breakpoints: {
	        950: {
	            slidesPerView: 3,
	            spaceBetween: 10,
	        },
	        550: {
	            slidesPerView: 1,
	            spaceBetween: 10,
	        }
	    }
	});

	var swiper = new Swiper('.more_projects_slider_main', {
	    slidesPerView: 3,
	    spaceBetween: 8,	    
	    autoplay : {
	    	 delay: 5000,
	   	},
		navigation: {
			nextEl: $('#more_projects .swiper-button-next'),
			prevEl: $('#more_projects .swiper-button-prev'),
		},	    
	    paginationClickable: true,
	    loop: false,
	    breakpoints: {
	        950: {
	            slidesPerView: 2,
	            spaceBetween: 10,
	        },
	        550: {
	            slidesPerView: 1,
	            spaceBetween: 10,
	        }
	    }
	});



    // Back to top button
    /////////////////////////////////////////////////////////////////////////
    $('.to_top').stop().fadeOut();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.to_top').stop().fadeIn('slow');
        } else {
            $('.to_top').stop().fadeOut('slow');
        }
    });


	$('.to_top').click(function(){

	    $('html,body').animate({
	        scrollTop: 0,
	    }, 1000);	
	});
    /////////////////////////////////////////////////////////////////////////
    // Back to top button




	$('.modal.hasScrollTo').on('shown.bs.modal', function(event) {
		// get the section using data
		var section = $(event.relatedTarget).attr('data-anchor');

		// get the top of the section
		var sectionOffset = $(this).find(section).position();

		//scroll the container
		$(this).find('.document_text_content').animate({
			scrollTop: sectionOffset.top - 35
		}, "slow");
	});
	$('.modal').on('hide.bs.modal', function(event) {
		// reset the scroll to top
		$(this).find('.document_text_content').scrollTop(0);	
	});




});
















 


function setCookie(cname, cvalue, exdays) {

    // if(!exdays)
    if(typeof exdays === 'undefined')        
    exdays = 5;

    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

 
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) { 
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


