const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


var path_public_assets = "public";
var path_resources_assets = "resources";


// mix.sass(path_resources_assets+'/sass/loader.scss', path_public_assets+'/css/master-loader.css');
mix.sass(path_resources_assets+'/sass/custom-app.scss', path_public_assets+'/css/master-app.css');

mix.scripts([
	path_resources_assets+'/js/custom-app.js',
], 	path_public_assets+'/js/master-app.js');








mix.js('resources/js/app.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css');













mix.sourceMaps(); // Enable sourcemaps
mix.version(); // Enable versioning.


mix.options({
  // extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
  processCssUrls: false, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
  // purifyCss: false, // Remove unused CSS selectors.
  // uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
  // postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
});



