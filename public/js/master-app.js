$(document).ready(function () {

    $(".scrollTo").click(function () {
        var offset = (getBrowserWidth() === 'xs') ? 0 : 100;

        $('html, body').animate({
            scrollTop: $($(this).attr('data-scrollToSelector')).offset().top - offset
        }, 1500);
    });









});




var getBrowserWidth = function () {
    if (window.innerWidth < 768) {
        // Extra Small Device
        return "xs";
    } else if (window.innerWidth < 991) {
        // Small Device
        return "sm"
    } else if (window.innerWidth < 1199) {
        // Medium Device
        return "md"
    } else {
        // Large Device
        return "lg"
    }
};


function checkForm() {
    var emailFilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if ($('#name').val() === '') {
        showAlert('Please Enter your Name');
    } else if ($('#email').val() === '') {
        showAlert('Please Enter your Email');
    } else if (!emailFilter.test($('#email').val())) {
        showAlert('Please Enter a Valid Email');
    } else if ($('#message').val() === '') {
        showAlert('Please Enter your Message');
    } else if (!$('#check_privacypolicy').is(':checked')) {
        showAlert('Please agree to Privacy Policy');
    } else {
        // let recaptcha_token = $('#recaptcha_token').val();
        $('#submit_btn').html('Please Wait ..').attr('disabled', 'disabled');

        getRecaptchaToken()
        .then((rspns)=>{
            let recaptcha_token = rspns;            
            verifyRecaptcha(recaptcha_token)
            .then((rspns) => {
                console.log('rspns',rspns);
                if(rspns=='success') {
                    sendInquiry();
                } else {
                    showAlert('Recaptcha Error. Please refresh and try again.');
                    $('#submit_btn').html('send message').removeAttr('disabled');
                }
            })
            .catch((error) => {
                showAlert('Recaptcha Error. Please refresh and try again.');
                $('#submit_btn').html('send message').removeAttr('disabled');
            });
        })
        .catch((error) => {
            showAlert('Recaptcha Error. Please refresh and try again.');
            $('#submit_btn').html('send message').removeAttr('disabled');
        });

    }
}

function showAlert(text) {
    $('#error_tic').find('.error_text').html(text); 
    $('#error_tic').modal('show');
}

function sendInquiry() {

    let sendInquiry_url = $('#inquiry').attr('action');
    let csrf_token = $('#csrf_token').val();

    let name = $('#inquiry #name').val();
    let email = $('#inquiry #email').val();
    let message = $('#inquiry #message').val();

    let post_data = {
        'name': name,
        'email': email,
        'message': message,
    };

    //Ajax post data to server            
    $.ajax({
        type: 'POST',
        url: sendInquiry_url,
        data: post_data,
        headers: {
            'X-CSRF-TOKEN': csrf_token
        },
        dataType: 'JSON',
        success: function (rspns) {
            console.log('success',rspns);
            if(rspns.error) {
                showAlert(rspns.message);
                $('#submit_btn').html('send message').removeAttr('disabled');
            } else {
                $('#submit_btn').html('send message').removeAttr('disabled');
                $('#success_tic').modal('show');                
            }

        },
        fail: function (error) {
            console.log('fail',error);
        },
    });

        
}




function verifyRecaptcha(recaptcha_token) {
    return new Promise((resolve, reject) => {
        
        // recaptcha v3
        ////////////////////////////////////////////////////////////////////////////
        if (recaptcha_token) {

            let verifyrecpatcha_url = $('#verifyrecpatcha_url').val();
            let csrf_token = $('#csrf_token').val();

            let post_data = {
                'recaptcha_token': recaptcha_token,
            };

            //Ajax post data to server            
            $.ajax({
                type: 'POST',
                url: verifyrecpatcha_url,
                data: post_data,
                headers: {
                    'X-CSRF-TOKEN': csrf_token
                },
                dataType: 'JSON',
                success: function (rspns) {
                    console.log('success');
                    resolve(rspns);
                },
                fail: function (error) {
                    console.log('fail');
                    reject(error);
                },
            });

        } else {
            reject();
        }
        ////////////////////////////////////////////////////////////////////////////
        // recaptcha v3
    });

}



function getRecaptchaToken(){
    return new Promise((resolve, reject)=>{
        grecaptcha.ready(function () {
            console.log('grecaptcha.ready');
            grecaptcha.execute('6LcJZdEUAAAAAL-5ZAg1UmrvAhtf9fR4KPPZ0kdh', {
                    action: 'homepage'
                })
                .then(function (rspns) {
                    resolve(rspns)
                });
        });
    });
}
