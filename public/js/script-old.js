"use strict";


/* page loader*/
$(window).on('load', function () {
    // Animate loader off screen
    $(".loader").fadeOut("slow");
});



$(window).on('scroll', function () {
    if ($(window).scrollTop() > 70) { // Set position from top to add class
        $('.navbar').addClass("shrink");
        $('.navbar .navbar-brand> img').attr('src', 'images/logos/logo-sm.png');
        $('.active-navbar .navbar .navbar-brand> img').attr('src', 'images/logos/logo-sm.png');

        $('.index-only-side-nav .navbar  .navbar-brand').addClass("display_none");
        $('.index-only-side-nav .navbar').removeClass("shrink");

    }
    else {
        $('.navbar').removeClass("shrink");
        $('.navbar .navbar-brand> img').attr('src', 'images/logos/logo-sm.png');
        $('.active-navbar .navbar .navbar-brand> img').attr('src', 'images/logos/logo-sm.png');
        $('.active-navbar .navbar').addClass("shrink");

        $('.index-only-side-nav .navbar  .navbar-brand').removeClass("display_none");
        $('.index-only-side-nav .navbar').removeClass("shrink");
    }
});

//scroll sections on clicking Links
///////////////////////////////////////////////////////////////////////////////
$(".scroll").on('click', function (event) {
    event.preventDefault();
    var offset = 80;
    
    // if mobile
    if ($(window).width() < 780)
    offset = 0;


    $('html,body').animate({scrollTop: $(this.hash).offset().top - offset}, 1000);
});

// $(".scroll").on('click', function(event) {
//     event.preventDefault();
//     $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 750);
// });
///////////////////////////////////////////////////////////////////////////////
//scroll sections on clicking Links

//scroll nav
$(window).on('scroll',function () {
    if($(this).scrollTop()  > 200){
        $('.special-nav').addClass("fixing_navbar_after_scrolling");
        $('.special-nav').removeClass("special_nav_styling");
    }
    else {
        $('.special-nav').removeClass("fixing_navbar_after_scrolling");
        $('.special-nav').addClass("special_nav_styling");
    }
})

/* Countdown Timer */


// fixing nav on top on scrolliing
var $fixednav = $(".bottom_navbar .navbar");
$(window).on("scroll",function () {
    var $heightcalc = $(window).height() - $fixednav.height();
    if($(this).scrollTop() > $heightcalc){
        $fixednav.addClass("navbar-fixed-top");
        $fixednav.addClass("bottom_auto");
    }else {
        $fixednav.removeClass("navbar-fixed-top");
        $fixednav.removeClass("bottom_auto");
    }
});

// Push Menus
var $menuLeft = $(".pushmenu-left");
var $menuRight = $(".pushmenu-right");
var $toggleleft = $(".menu_bars.left");
var $toggleright = $(".menu_bars.right");
$toggleright.on("click", function() {
    $('.menu_bars').toggleClass("active");
    $menuRight.toggleClass("pushmenu-open");
    $("body").toggleClass("pushmenu-push-toLeft");
    $(".navbar").toggleClass("navbar-right_open");
    return false;
});

$('.push_nav li a').on('click', function(){
    $toggleright.toggleClass("active");
    $menuRight.toggleClass("pushmenu-open");
    $("body").toggleClass("pushmenu-push-toLeft");
    $(".navbar").toggleClass("navbar-right_open");
    return true;
});


